# Trier des données

- 1.3.1 Introduction sur les données en table
- 1.3.2 Recherches dans une table
- 1.3.3 **Trier des données**
- 1.3.4 Fusion de tables
- 1.3.5 Sauvegarde des données
- 1.3.6 Jointure

####

- Sébastien Hoarau.  Manipuler des données en table : faire des recherches.

[![Vidéo 1 B1-M3-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M3-S3.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M3-S3.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M3/B1-M3-S3.srt" target="_blank">Sous-titre de la vidéo</a> 

## Documents  complémentaires

<a href="https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/Le_Mooc/bloc_1_repr_donnees_information/1-3_donnees_en_tables/1-3-3_Trier_des_donn%C3%A9es/1-3-3-1_csv-pandas.html" target="_blank">Texte : CSV et Pandas</a>