## Les p-uplets

- le tableau,
- **le p-uplet**,
- le dictionnaire

####

- Sébastien Hoarau. Les p-uplets

[![Vidéo 1 B1-M2-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M2-S2.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M2-S2.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M2/B1-M2-S2.srt" target="_blank">Sous-titre de la vidéo</a> 

## Texte complémentaire 

<a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/bloc_1_repr_donnees_information/1-2_types_construits/1-2-2_Les_p-uplets/1-2-2-1_Fiche-Puplets.md" target="_blank">Texte complément à la video</a>

