# AIDE EN CAS DE BESOIN

#### Conseils

Un problème d'accès ? : les conseils donnés à l'exercice précédent peuvent vous aider

Par ailleurs La [La partie sur UpyLaB de la Foire aux questions du MOOC "Apprendre à coder avec Python"](https://upylab.ulb.ac.be/pub/FAQ_mooc/Modules/intro.html#upylab) parle des erreurs classique dont le message "« EOF » when reading a line".

#### COMMENT RÉSOUDRE LES EXERCICES UPYLAB

Un point essentiel à fixer pour bien apprendre lors de ce cours est
la façon de réaliser les exercices UpyLaB.  Pour chaque exercice
UpyLaB, l'énoncé est fourni.
  
UpyLaB n'est pas un environnement de développement.  Il permet juste
de tester si votre code lui semble correct.

Ainsi, après avoir bien compris l’énoncé de l’exercice, il est
important de développer une solution en utilisant votre IDE pour voir de façon
détaillée comment votre code s'exécute. Ce n’est que quand votre solution
sera complète et que vous l’aurez validée en testant le code que vous
pourrez copier et coller ce code dans la fenêtre UpyLaB de l’exercice
pour lancer la validation. Tester son code signifie exécuter le
programme plusieurs fois et, si possible, sur des exemples différents,
pour être convaincu qu’il donne toujours une réponse correcte.

Si UpyLaB ne le valide pas - j’ai par exemple mis l’instruction  :

``print("Bonsoir UpyLaB !")``

ou même

``print("Bonjour UpyLaB ! ")``

avec **une espace en trop après le point d'exclamation**,

à la place de :

``print("Bonjour UpyLaB !")``

vous devez le corriger avant de soumettre une solution modifiée.

> Attention :  Le nombre de vérifications que vous pouvez faire avec UpyLaB n'est
> pas limité. Malgré tout, il est préférable de réussir l’exercice avec un
> nombre de clics "Valider" le plus petit possible.
  
