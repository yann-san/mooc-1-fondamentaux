# AIDE EN CAS DE BESOIN

#### Conseils

* Vous obtenez une erreur de syntaxe ? :
  Vérifiez que vous avez bien copié l’instruction demandée, sans
  ajouter d’espace devant, et sans oublier parenthèses ou guillemets
  fermants.

* Pas d’erreur de syntaxe, mais UpyLaB refuse de valider votre code ?  :
  L’affichage doit être exactement identique à celui attendu.  Veillez
  en particulier à respecter majuscules / minuscules, à ne pas ajouter
  ou ôter des espaces (en particulier à la fin de la phrase), à ne pas
  oublier la ponctuation.

* Un conseil pour avoir un message identique est de le copier depuis
  l'énoncé pour le coller dans votre code.  Ici, vous pouvez même
  copier toute l'instruction.  :

  ```python
       print("Bonjour UpyLaB !")
  ```
  
  avant de la coller dans la fenêtre UpyLaB de l'exercice.
 

#### La fenêtre UpyLaB reste bloquée

Pour certains navigateurs Web et certaines configurations, des
problèmes d'accès à notre outil UpyLaB peuvent survenir.  Typiquement,
la fenêtre reste bloquée.

Si cela se produit, cliquez simplement sur le bouton  ``Rafraîchir la page si demandé par UpyLaB avant de vérifier à nouveau`` situé en dessous de l'exercice (même si ce n'est pas demandé explicitement :-) ). 

Si le souci perdure, une des causes les plus courantes est l'utilisation de code anti
"pop-up" ou anti-mouchard (en particulier avec Firefox).  Si c'est le
cas, il est possible que vous deviez autoriser l'accès à
https://upylab.ulb.ac.be (l'adresse d'accès à notre outil UpyLaB).

Donc si une telle erreur se produit, allez voir dans la FAQ (Foire Aux
Questions du cours "Apprendre à coder avec Python" https://upylab.ulb.ac.be/pub/FAQ_mooc/Modules/intro.html#probleme-d-acces-a-upylab )  où un
résumé des soucis constatés et des solutions précises proposées y a
été déposé.

  
