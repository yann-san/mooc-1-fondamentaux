# Exercice UpyLaB 2

## Enoncé

Écrivez en Python, une fonction `representation_fractionnaire(val, base=2, longueur=128)`

qui renvoie la représentation en base `base` de la valeur fractionnaire `val`.

### Hypothèses 

- `0.0 <= val < 1.0`
- `1 < base < 37`

### Résultat

Renvoie une chaîne de caractères `"0. ..."` qui représente :

- soit '0.0' si `val` vaut 0.0
- soit la valeur exacte dans la base `base`
- soit la représentation d'une valeur approchée avec une partie décimale de `longueur` chiffres et dans ce cas, la représentation est terminée par un caractère '+'
- soit la valeur avec la représentation notée '0.prefixe (periodique)$\; \omega$' et représentant la représentation

'0. prefixe periodique periodique ...'

(la partie periodique étant répétée une infinité de fois).

Les chiffres seront notés (dans l'ordre des valeurs) : `'0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'`

### Paramètres
- `val` : `float` (`0.0 <= val < 1.0`) valeur fractionnaire à représenter
- `base`: `int` (`1 < base < 37`) base

### Contraintes

Votre code soumis à UpyLaB :

- **ne peut effectuer aucun affichage (aucun `print` n'est autorisé par UpyLaB)**
- ne peut effectuer aucun `input`
- ne doit pas faire  appel à la fonction
- peut comporter des définitions de constantes ainsi que  d'autres fonctions qui vous semblent utiles.


### Conseils

- $\omega$ est donné en Python par chr(969) 

- **Attention aux erreurs d'arrondi :** Notons que les valeurs soumises à traduction sont en fait des valeurs stockées dans des variables float Python ; plus précisément, la représentation par l'implémentation CPython de la valeur.  Il s'agit donc le la représentation sur 64 bits utilisant la norme IEEE754 double précision.  Certaines valeurs sont stockées précisément, d'autres avec une arreur d'arrondi.  Ainsi  la valeur `0.1` est stockée en mémoire avec une erreur d'arrondi. Si on demande  l'affichage de la valeur `0.1` avec 55 chiffres décimaux  (`f"{0.1:.55f}"`), cela donne :

  `'0.1000000000000000055511151231257827021181583404541015625'`

  Du coup en fonction de la base choisie, la réprésentation binaire de `0.1`, dû à cette erreur d'arrondi, est tantôt périodique (cf avec la base 5) tantôt non ou périodique mais pas comme attendu (avec la base 2 par exemple) même si avec une représentation parfaite, les 2 sont périodiques !


### Résultat

Renvoie, suivant les paramètres une chaîne de caractères parmi les formats :

- '0.0' 
- '0. prefixe' 
- '0. prefixe +'
- '0. prefixe (periodique) $\; \omega$'

(voir plus haut).


### Exemples :

- representation_fractionnaire(0.0, 20) : '0.0'
- representation_fractionnaire(0.1, 5) : '0.0(2)$\omega$' # représente 0.02222222...
- representation_fractionnaire(0.1, 20) : '0.2' # en base 20
- representation_fractionnaire(1/3, 10, 20) : '0.33333333333333330372+' # non terminé
(dû aux erreurs dans la représentation de 1/3)
