# TP installation d'un SGBDR 

Plutôt que des exercices, ce chapitre peut facilement donner lieu à une
mise en pratique basée sur l'installation d'un serveur et d'un
client, et de quelques investigations sur leur configuration et leur
fonctionnement. Cet atelier n'est bien entendu faisable que si vous
disposez d'une ordinateur et d'un minimum de pratique. informatique.

Plusieurs SGBD relationnels sont disponibles en *open source*. Pour
chacun, vous pouvez installer le serveur et une ou plusieurs
applications clientes.

### MySQL

MySQL est un des SGBDR les plus utilisés au monde. Il est maintenant
distribué par Oracle Corp, mais on peut l'installer (version MySQL
Commiunity server) et l'utiliser gratuitement.

Il est assez pratique d'installer MySQL avec un environnement Web
comprenant Apache, le langage PHP et le client phpMyAdmin. Cet
environnement est connu sous l'acronyme AMP (Apache - PHP - MySQL).

> -   Se référer au site <http://www.mysql.com> pour des informations
>     générales.
>
> -   
>
>     Choisir une distribution adaptée à votre environnement,
>
>         -   EasyPHP, <http://www.easyphp.org/>, pour Windows
>         -   MAMP pour Mac OS X
>         -   D'innombrables outils pour Linux.
>
> -   Suivez les instructions pour installer le serveur et le démarrer.
>
> -   Trouvez le fichier de configuration et consultez-le. La
>     configuration d'un serveur comprend typiquement: la mémoire
>     allouée et l'emplacement des fichiers.
>
> -   Cherchez où se trouvent les fichiers de base et consultez-les.
>     Peut-on les éditer ou ces fichiers sont-ils en format binaire,
>     lisibles seulement par le serveur?
>
> -   Installez une application cliente. Dans un environnement AMP, vous
>     avez en principe d'office l'application Web phpMyAdmin qui est
>     installée. Essayez de comprendre l'architecture: qui est le
>     serveur, qui est le client, quel est le rôle de Apache, quel est
>     le rôle de votre navigateur web.
>
> -   Installez un client autre que phpMyAdmin, par exemple MySQL
>     Workbench (disponible sur le site d'Oracle). Quels sont les
>     paramètres à donner pour connecter ce client au serveur?
>
> -   Executez les scripts SQL de création et d'alimentation de la base
>     de voyageurs.

Quand vous aurez clarifié tout cela pour devriez être en situation
confortable pour passer à la suite du cours.

### Autres

Si le cœur vous en dit, vous pouvez essayer d'autres systèmes
relationnels libres: Postgres, Firebird, Berkeley DB, autres? À vous de
voir.

