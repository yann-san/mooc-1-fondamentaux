## Le modèle relationnel. Atelier : une étude de cas 1/3

Dans l'ensemble des exercices qui suivent, on cherche à spécifier le
système d'information d'un zoo, et on suppose que l'on se trouve dans
la situation suivante : une personne peu avertie (elle n'a pas suivi les
enseignements du Cnam !) a créé en tout et pour tout une seule relation
dans laquelle on trouve toutes les informations. Voici le schéma de
cette table.

``` 
Zoo (animal, nom, année_naissance, espèce, gardien,
       prénom, salaire, classe, origine, emplacement, surface)
```

Chaque ligne corrrespond à un animal auquel on attribue un nom propre,
une année de naissance et une espèce (Ours, Lion, Boa, etc.). Cet animal
est pris en charge par un gardien (avec prénom et salaire) et occupe un
emplacement dans le zoo dont on connaît la surface. Enfin chaque espèce
appartient à une classe (les mammifères, poissons, reptiles, batraciens
ou oiseaux) et on considère pour simplifier qu'elle provient d'une
origine unique (Afrique, Europe, etc.).

Tout cela est évidemment très approximatif. Essayons d'y mettre de
l'ordre. Voici les dépendances fonctionnelles :

> -   animal $\to$ nom, année\_naissance, espèce, emplacement.
> -   nom, espèce $\to$ animal.
> -   espèce $\to$ origine, classe.
> -   gardien $\to$ prénom, salaire.
> -   emplacement $\to$ surface, gardien.

Le but pour l'instant est d'identifier les anomalies et de trouver les
clés.

### Interprétation des dépendances


Supposons que le contenu de la table `Zoo` respecte les dépendances
fonctionnelles ci-dessus. Répondez aux questions suivantes :

> 1. Deux animaux peuvent-ils avoir le même nom ?
> 2. Le nom d'un animal suffit-il pour l'identifier ?
> 3. Peut-on avoir deux animaux avec le même nom sur le même emplacement ?
> 4. Connaissant un animal, est-ce que je connais son origine ?
> 5. Connaissant un animal, est-ce que je sais quel est son gardien ?
> 6. Un gardien peut-il s'occuper de plusieurs emplacements ?
> 7. Un emplacement peut-il être pris en charge par plusieurs gardiens ?
> 8. Deux gardiens peuvent-ils avoir le même salaire ?
>
