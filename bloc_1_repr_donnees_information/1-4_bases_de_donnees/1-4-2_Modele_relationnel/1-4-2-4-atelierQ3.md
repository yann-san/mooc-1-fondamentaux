## Le modèle relationnel. Atelier : une étude de cas 3/3

### Calcul des clés

1. Montrer que `animal` et `nom, Espèce` sont des clés de la relation Zoo. 
2. Montrer que ce sont les seules clés. 
3. Montrer que la table n’est pas en troisième forme normale. 

