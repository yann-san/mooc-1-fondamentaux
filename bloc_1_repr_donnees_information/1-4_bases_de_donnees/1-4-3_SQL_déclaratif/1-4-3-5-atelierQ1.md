## Q1. Requêtes SQL sur la base Voyageurs 


Sur notre base des voyageurs en ligne (ou sur la vôtre, après
installation d'un SGBD et chargement de nos scripts), vous devez
exprimer les requêtes suivantes :

> -   a) Nom des villes
> -   b) Nom des logements en Bretagne
> -   c) Nom des logements dont la capacité est inférieure à 20
> -   d) Description des activités de plongée
> -   e) Nom des logements avec piscine
> -   f) Nom des voyageurs qui sont allés en Corse
> -   g) Les voyageurs qui sont allés ailleurs qu'en Corse
> -   h) Nom des logements visités par un auvergnat
> -   i) Nom des logements et des voyageurs situés dans la même région
> -   j) Les paires de voyageurs (donner les noms) qui ont séjourné dans le
>     même logement
> -   k) Les voyageurs qui sont allés (au moins) deux fois dans le même
>     logement
> -   l) Les logements qui ont reçu (au moins) deux voyageurs différents
> -   ...

**Contrainte** : n'utilisez pas l'imbrication, pour aucune requête (et
forcez-vous à utiliser la forme déclarative, même si vous connaissez
d'autres options que nous étudierons dans le prochain chapitre).

