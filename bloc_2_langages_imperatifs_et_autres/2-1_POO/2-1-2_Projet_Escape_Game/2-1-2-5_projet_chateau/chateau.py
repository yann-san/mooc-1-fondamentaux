
import argparse
import turtle

from constantes import *

# ----- UTILITAIRES 
# -----


def aller(tortue, x, y):
    tortue.up()
    tortue.goto(x, y)
    tortue.down()

def mark(tortue, x, y, couleur, size):
    """Dessine un carré dont le sommet inférieur gauche est en x, y, de couleur intérieur couleur, de couleur extérieure la couleur constante définie par COULEUR_EXTERIEURE et de dimension echelle"""
    aller(tortue, x, y)
    tortue.color(COULEUR_EXTERIEUR, couleur)
    tortue.begin_fill()
    for _ in range(4):
        tortue.fd(size)
        tortue.left(90)
    tortue.end_fill()

def translate(x, y, ech):
    """Passe de coordonnées entières aux coordonnées de la fenêtre graphique"""
    return X_MIN + x * ech, Y_MAX - y * ech



# ----- LE CHATEAU et SES CELLULES
# -----

class Cell:
    """Une  cellule générique du château, avec une coordonnée
    cela peut être l'ENTREE, la SORTIE ou juste une cellule VIDE
    """

    def __init__(self, type_cell, x, y, chateau):
        self.x = x
        self.y = y
        self.type = type_cell
        self.chateau = chateau

    def sortie(self):
        return self.type == SORTIE

    def accessible(self):
        return self.type != MUR

    def seen(self):
        self.change_type(SEEN)

    def change_type(self, type_cell):
        """Change la nature d'une cellule et informe le chateau qu'il
        faut mettre à jour la vue"""
        self.type = type_cell
        self.chateau.update_view(self)

    def interaction(self):
        pass

    def setup(self, *args):
        pass


class Porte(Cell):

    def __init__(self, *args):
        self.question = str()
        self.solution = str()
        super(Porte, self).__init__(PORTE, *args)

    def setup(self, *args):
        self.question, self.solution = args

    def verrouillee(self):
        return self.type == PORTE

    def accessible(self):
        return not self.verrouillee()

    def deverrouille(self):
        self.change_type(VIDE)

    def bonne_reponse(self, reponse):
        return reponse.lower() == self.solution.lower()

    # Le contrôleur
    #
    def interaction(self):
        """Contrôleur de la porte
        Si la porte est verrouillée, la porte demande au chateau de poser la question au joueur. 
        La porte recupere la reponse fournie et si c'est une bonne réponse, se déverrouille sinon reste verrouillée. A chaque étape, le chateau demande au mdj de faire des annonces appropriées au joueur
        """ 
        if self.verrouillee():
            self.chateau.annonce(MSG_PORTE_VERROUILLEE)
            reponse = self.chateau.ask(self.question)
            if self.bonne_reponse(reponse):
                self.deverrouille()
                self.chateau.annonce(MSG_PORTE_DEVERROUILLEE)
            else:
                self.chateau.annonce(MSG_MAUVAISE_REPONSE)


class Objet(Cell):

    def __init__(self, *args):
        self.nom = str()
        super(Objet, self).__init__(OBJET, *args)

    def setup(self, *args):
        self.nom = args[0]

    def accessible(self):
        return self.type != OBJET

    def interaction(self):
        self.chateau.donner(self.nom)
        self.change_type(VIDE)    


# ----- 
# ----- LE CHATEAU

class Chateau:

    def __init__(self, mdj):
        self.map = list() # matrice de Cell
        self.w = 0
        self.h = 0
        self.mdj = mdj
        
        self.ech = 0
        self.view = turtle.Turtle()    # pour la visualisation du chateau

    def setup(self, f_plan, f_obj, f_portes):
        entree = self.initialiser_plan(f_plan)
        self.init_portes_et_objets(f_portes)
        self.init_portes_et_objets(f_obj)
        self.ech = min(WIDTH // self.w, HEIGHT // self.h)

        # Réglages de la tortue
        self.view.ht()
        self.view.screen.tracer(0)
        return entree

    def initialiser_plan(self, fichier, delimiter=DELIMITER):
        with open(fichier, 'r') as f_in:
            for y, ligne in enumerate(f_in):
                self.map.append([])
                for x, str_value in enumerate(ligne.strip().split(delimiter)):
                    type_cell = int(str_value)
                    if type_cell in (VIDE, SORTIE, ENTREE, MUR):
                        if type_cell == ENTREE:
                            entree = x, y
                        self.map[-1].append(Cell(type_cell, x, y, self))
                    elif type_cell == PORTE:
                        self.map[-1].append(Porte(x, y, self))
                    elif type_cell == OBJET:
                        self.map[-1].append(Objet(x, y, self))
        self.w, self.h = len(self.map[0]), len(self.map)
        return entree

    def init_portes_et_objets(self, fichier, delimiter=DELIMITER):
        with open(fichier, 'r', encoding='utf-8') as f_in:
            for ligne in f_in:
                x, y, *args = ligne.strip().split(delimiter)
                self.cell(int(x), int(y)).setup(*args)

    def inside(self, x, y):
        return 0 <= x < self.w and 0 <= y < self.h

    def view(self):
        return self.view

    def cell(self, x, y):
        print(x, y)
        return self.map[y][x]

    def init_view(self):
        """Parcourt les cellule de la map et met à jour la vue"""
        for ligne in self.map:
            for cell in ligne:
                self.update_view(cell)
        self.mdj.update()

    def update_view(self, cell):
        """Pour mettre à jour la vue, d'une cellule"""
        xv, yv = translate(cell.x, cell.y, self.ech)
        mark(self.view, xv, yv, COULEURS[cell.type], self.ech)
    
    def inside(self, x, y):
        return 0 <= x < self.w and 0 <= y < self.h

    def annonce(self, msg):
        self.mdj.annonce(msg)
    
    def donner(self, nom):
        self.mdj.annonce(MSG_INDICE_TROUVE)
        self.mdj.note_inventaire(nom)

    def ask(self, question):
        return self.mdj.ask(question)

    def active_ecoute(self):
        self.mdj.active_ecoute()



# ----- 
# ----- LE HEROS

class Heros:

    def __init__(self, chateau, mdj):
        self.x = None
        self.y = None
        self.ech = 0
        self.chateau = chateau
        self.mdj = mdj
        self.view = turtle.Turtle()

    def __str__(self):
        return f'heros en {self.x},{self.y}'

    # -- Le modèle

    def setup(self, entree, echelle):
        self.x, self.y = entree
        self.ech = echelle
        
        # Réglages de la tortue
        self.view.color(COULEUR_PERSONNAGE)
        self.view.shape('circle')
        self.view.shapesize(self.ech / RATIO_PERSONNAGE)
        self.view.up()

    def goto(self, dx, dy):
        self.x += dx
        self.y += dy

    def found_exit(self):
        return self.chateau.cell(self.x, self.y).sortie()

    # -- La vue du joueur
    
    def update_view(self):
        xv, yv = translate(self.x, self.y, self.ech)
        aller(self.view, xv + self.ech // 2, yv + self.ech // 2)
        self.mdj.update()


    # -- Le contrôleur du joueur
    
    def move_up(self):
        self.avancer(UP)

    def move_down(self):
        self.avancer(DOWN)

    def move_left(self):
        self.avancer(LEFT)

    def move_right(self):
        self.avancer(RIGHT)

    def avancer(self, direction):
        dx, dy = direction
        origine = self.chateau.cell(self.x, self.y)
        if self.chateau.inside(self.x + dx, self.y + dy):
            destination = self.chateau.cell(self.x + dx, self.y + dy)
            if destination.accessible():
                origine.seen()
                self.goto(dx, dy)
                self.update_view()
                if self.found_exit():
                    self.mdj.end()
            else:
                destination.interaction()




# ----- 
# ----- LE MAITRE DE JEU

class MaitreDeJeu:

    def __init__(self):
        self.chateau = Chateau(self)
        self.heros = Heros(self.chateau, self)
        self.controleur = turtle.Screen()   # gère les évévements
        self.msg_view = turtle.Turtle()     # les messages à l'utilisateur
        self.sac_view = turtle.Turtle()     # la visualisation du contenu du sac


    def setup(self):
        # Traitement des options
        #
        parser = argparse.ArgumentParser()
        parser.add_argument('-s', '--set', help='numéro du set de fichiers', type=int)

        args = parser.parse_args()
        set_fichier = int(args.set) if args.set else 1
        fichier_plan = f'fichiers/plan_chateau_{set_fichier}.txt'
        fichier_objets = f'fichiers/dico_objets_{set_fichier}.txt'
        fichier_portes = f'fichiers/dico_portes_{set_fichier}.txt'
        
        # Réglages des tortues messagères
        self.msg_view.ht()
        aller(self.msg_view, *POINT_AFFICHAGE_ANNONCES)
        self.sac_view.ht()
        self.sac_view.up()
        self.sac_view.right(90)
        aller(self.sac_view, *POINT_AFFICHAGE_INVENTAIRE)
        self.note_inventaire('INVENTAIRE')

        # Setup du chateau et du heros
        entree = self.chateau.setup(fichier_plan, fichier_objets, fichier_portes)
        self.heros.setup(entree, self.chateau.ech)


    def active_ecoute(self):
        self.controleur.listen()

    def ask(self, question):
        reponse = self.controleur.textinput('Question', question)
        self.active_ecoute()
        return reponse

    def annonce(self, msg):
        self.msg_view.clear()
        self.msg_view.write(msg, font=FONT)        
    
    def note_inventaire(self, msg):
        self.sac_view.write(msg, font=FONT)
        self.sac_view.up()
        self.sac_view.fd(HAUTEUR_TEXTE)
    
    def unbind(self):
        for key in (KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT):
            self.controleur.onkey(None, key)

    def bind(self):
        self.controleur.onkey(self.heros.move_up, KEY_UP)
        self.controleur.onkey(self.heros.move_down, KEY_DOWN)
        self.controleur.onkey(self.heros.move_left, KEY_LEFT)
        self.controleur.onkey(self.heros.move_right, KEY_RIGHT)

    def update(self):
        self.controleur.update()

    def end(self):
        self.annonce(MSG_VICTOIRE)
        self.unbind()
        
    def start(self):
        self.annonce(MSG_DEBUT)
        self.chateau.init_view()
        self.heros.update_view()
        self.bind()
        self.controleur.listen()
        self.controleur.mainloop()


# ----- 
# ----- PROGRAMME PRINCIPAL

jeu = MaitreDeJeu()
jeu.setup()
jeu.start()


