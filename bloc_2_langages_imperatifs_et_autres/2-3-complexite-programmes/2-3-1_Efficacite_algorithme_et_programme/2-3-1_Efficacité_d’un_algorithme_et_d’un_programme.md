# Efficacité d’un algorithme et d’un programme

1. **Efficacité d'un algorithme et d'un programme**
2. Approche pragmatique
3. Approche analytique
4. La notion de grand O
5. Calcul du grand O
6. Applications des règles de calcul du grand O
7. Complexité des tris simples
8. Complexité des manipulations des séquences de données python

####

- **Thierry Massart**. Comment évaluer l'efficacité d'un algorithme 

[![Vidéo 1 B2-M3-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B2-M3-S1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B2-M3-S1.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B2-M3/B2-M3-S1.srt" target="_blank">Sous-titre de la vidéo</a> -  <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B2-M3/B2-M3-S1-script.md" target="_blank">Transcription de la vidéo </a>

## Supports de présentation (diapos)

<a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B2-M3/Slides/B2-M3-diapos-video1.pdf" target="_blank">Supports de présentation</a>


