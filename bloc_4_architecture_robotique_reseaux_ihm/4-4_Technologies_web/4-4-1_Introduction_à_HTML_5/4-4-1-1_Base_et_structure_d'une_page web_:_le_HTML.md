# Introduction à HTML 5 1/3: Base et structure d'une page web : le HTML

1. **Base et structure d'une page web : le HTML**
2. HTML : éléments de base du HTML
3. HTML : mise en forme et structures avancées

####

* **Rodrigue Chakode**.  Introduction à Html5 et au web statique

[![Vidéo 1 B4-M4-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M4-S1-video2.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M4-S1-video2.mp4	)

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M4/NSI-B4-M4-S1-diapos.pdf" target="_blank">Supports de présentation des vidéos de la section 1</a>

## Exercice d'application 

<a href="https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/Le_Mooc/bloc_4_architecture_robotique_reseaux_ihm/4-4_Technologies_web/4-4-5-TP/TP1_Creation_page_web_statique_minimale.html "> TP 1 : Création d'une première page web minimaliste (30 mn) </a>

