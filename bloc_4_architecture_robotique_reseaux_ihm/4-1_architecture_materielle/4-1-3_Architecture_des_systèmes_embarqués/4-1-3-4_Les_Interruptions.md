## Architecture des systèmes embarqués: Les interruptions

L'objectif de ce premier chapitre ou séquence est de faire une présentation générale du rôle et des fonctionnalités principales d'un système d'exploitation.

1. Introduction. Applications et problématiques des systèmes embarqués
2. Architecture d’un microcontrôleur, d’un SoC (2 vidéos)
3. Les appels de fonctions et la pile
4. **Les interruptions**

[![Vidéo 4 B4-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S3-V4.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S3-V4.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M1/NSI-B4-M1-S3-script-SystemesEmbarques.md" target="_blank">Script videos</a>


## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M1/NSI-B4-M1-S3-diapos.pdf" target="_blank">Supports de présentation des vidéos</a>
