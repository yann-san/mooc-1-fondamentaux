## Architecture des ordinateurs: La CPU, calculer plus vite

L'objectif de ce premier chapitre ou séquence est de faire une présentation générale du rôle et des fonctionnalités principales d'un système d'exploitation.

1. Introduction
2. La mémoire : plus et plus vite (4 videos)
    - La hierarchie des mémoires, Les technologies SRAM et DRAM, les registres
    - La mémoire centrale
    - La mémoire cache & la mémoire virtuelle
    - Les mémoires de stockage
3. **La CPU : calculer plus vite (3 vidéos)**
    - **Instructions SIMD**
    - **Pipelines** 
    - multithreading
4. Le chipset et les bus : communiquer (1 vidéos)

[![Vidéo 5 B4-M1-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S4-V5.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S4-V5-v2.mp4)

...

[![Vidéo 6 part2 B4-M1-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S4-V6.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S4-V6-v2.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M1/NSI-B4-M1-S4-script2-CPU_Bus.md" target="_blank">Script videos</a>

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M1/NSI-B4-M1-S4-diapos.pdf" target="_blank">Supports de présentation des vidéos</a>

