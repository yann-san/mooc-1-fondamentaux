# Sommaire B4-M1 Architecture Matérielle des Systèmes informatiques


## 4-1-0 Présentation du Module

## 4-1-1 Introduction à l'architecture des systèmes informatiques  
- 4-1-1-1 De la machine de Turing au Pentium, un demi-siècle d'évolution sans limite    
- 4-1-1-2 Croître malgré l'échauffement, les évolutions récentes de l'informatique  

## 4-1-2 Architecture minimale d'un système informatique        
- 4-1-2-1 Du transistor au processeur   
- 4-1-2-2 Architectures Von Neumann / Harvard   
- 4-1-2-3 Le langage de la CPU, l’assembleur    
- 4-1-2-4 Du langage haut niveau à l’assembleur     
    
## 4-1-3 Architecture des systèmes embarqués        
- 4-1-3-1 Applications et problématiques des systèmes embarqués     
- 4-1-3-2 Architecture d’un microcontrôleur, d’un SoC   
- 4-1-3-3 Les appels de fonctions et la pile    
- 4-1-3-4 Les Interruptions     
    
## 4-1-4 Architecture des ordinateurs       
- 4-1-4-1 La mémoire : plus et plus vite    
  - La hiérarchie des mémoires
  - Les technologies SRAM et DRAM
  - Les registres
  - La mémoire centrale
  - La mémoire cache
  - La mémoire virtuelle 
  - Les mémoires de stockage
- 4-1-4-2 La CPU : calculer plus vite 
  - Jeu d'instructions avancées
  - Processeurs superscalaires
  - Pipelines
  - Exécution dans le désordre, exécution spéculative
  - Hyperthreading
  - Multicoeur
  - Refroidissement
- 4-1-4-3 Le chipset et les bus : communiquer   
  - Les bus du microprocesseur
  - L'implantation matériel
