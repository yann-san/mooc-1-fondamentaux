## Introduction à l'architecture des systèmes informatique. De la machine de Turing au Pentium, un demi-siècle d'évolution sans limite

Ce premier chapitre, à travers une rapide histoire de l'architecture des ordinateurs, vise à sensibiliser l'auditeur comment les algortihmes ont sucité les développements matériels et comment les innovations matérielles ont permis et influencé certaines évolutions logicielles pour l'émergence de l'informatique puis du traitement massif des données et de l'intelligence artificielle.
Il présentera également les limites technologiques pour comprendre les freins au développement et les options de contournement choisies.

1. **Introduction**
2. **De la machine de Turing au Pentium, un demi-siècle d'évolution sans limite**
3. Croître malgré l'échauffement, les évolutions récentes de l'informatique (2 vidéos)

[![Vidéo 1 B4-M1-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S1-V1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S1-V1.mp4)


#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M1/NSI-B4-M1-S1-script-videos1-2.md" target="_blank">Script de la vidéo </a>


## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M1/NSI-B4-M1-S1-diapos.pdf" target="_blank">Supports de présentation des vidéos</a>
