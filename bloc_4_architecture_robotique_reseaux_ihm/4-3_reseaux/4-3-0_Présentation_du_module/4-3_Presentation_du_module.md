# 4.3 Réseaux  

## Objectifs du module

 - Connaître et comprendre les protocoles permettant le dialogue des équipements sur Internet,
 - Savoir mettre en œuvre un réseau local ouvert vers et depuis l’extérieur,
 - Simuler un réseau de plus grande taille.

[![Vidéo présentation module 3  B4-M3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S0.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-presentation.mp4)


## Sommaire 

- 1. Introduction aux réseaux
- 2. Couches physiques, Ethernet
- 3. Réseau IP
- 4. Routage
- 5. Transport TCP/UDP
- 6. Application/ Service
- 7. Sécurité

##  Prérequis

Électronique numérique (représentation hexadécimale, fonctions logiques) 

## Temps d'apprentissage

30 heures

## Présentation enseignants

- **Anthony Juton**
    - Anthony Juton est professeur agrégé à l'ENS Paris Saclay au département E2A où il enseigne notamment les réseaux , tout comme à l'Université Paris Saclay.
- **Anne Josiane Kouam**
    -  Anne Josiane Kouam est doctorante dans l'équipe de recherche Inria TRiBE qui s'intéresse aux réseaux, notamment aux réseaux sans fils et travaille à rendre les réseaux plus sécurisés et adaptés au nombre de plus en plus croissant et à l'hétérogénéité des appareils interconnectés. Elle intervient dans l'enseignement des réseaux à l'Université Paris-Saclay.
- **Geoffrey Vaquette**
    - Geoffrey Vaquette est  Ingénieur-chercheur en informatique chez CEA Liste.


## Sites et ouvrages de références

- Réseaux, Andrew Tanenbaum, PEARSON  5e édition (25 août 2011)
- Les réseaux, Guy Pujolle, Eyrolles, Édition : 9 (5 juillet 2018)
- Cisco Networking Academy www.netacad.com
- Wikipédia, très complète sur le sujet
- Documentation du logiciel analyseur de réseaux IP https://www.wireshark.org/

