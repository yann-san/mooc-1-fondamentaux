# Transport TCP/UDP 4/4: La gestion des congestions via TCP

1. Présentation de la couche transport
2. Le protocole UDP
3. Le protocole TCP
4. **La gestion des congestions via TCP**

####

- Dans cette vidéo, **Anthony Juton** explique comment TCP propose des mécanismes de régulation du trafic pour limiter les congestions.

[![Vidéo 4 B4-M3-S5 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S5_video4_Congestion.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S5_video4_Congestion.mp4)

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S5_3_congestions_TCP.pdf" target="_blank">Supports de présentation vidéo 4</a>
