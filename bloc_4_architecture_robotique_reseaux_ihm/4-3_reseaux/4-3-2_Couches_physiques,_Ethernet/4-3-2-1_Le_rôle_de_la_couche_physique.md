# Couches Physiques, Ethernet 1/3: le rôle de la couche physique

1. **La couche physique**
2. Le protocole Ethernet
3. Relier des hôtes : le hub et le switch

####

- Dans cette video, **Geoffrey Vaquette**  nous parle de la couche physique et des éléments qui la composent, essentiellement des couches physique et liaison du modèle OSI correspondant aux protocoles Ethernet.  

[![Vidéo 1 B4-M3-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S2_video1_CouchePhy.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S2_video1_CouchePhy.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S2-script-video1.md" target="_blank">Transcription vidéo 1</a>

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S2-video1-diapos.pdf" target="_blank">Supports de présentation de la vidéo 1</a>


