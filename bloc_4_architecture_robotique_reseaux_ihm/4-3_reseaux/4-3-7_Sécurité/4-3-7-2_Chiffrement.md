# Sécurité 2/5: Chiffrement

1. Firewall / Proxy / DMZ
2. **Chiffrement**
3. Les protocoles SSH/SSL
4. VPN
5. VLAN

####

- **Anne Josiane  Kouam**. Nous allons voir dans cette vidéo la notion de chiffrement et les principes utilisés pour assurer la sécurité des réseaux. 

[![Vidéo 2part1 B4-M3-S7 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S7_video2p1-Chiffrement.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S7_video2p1-Chiffrement.mp4)

 #### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S7-script-video2_part1.md" target="_blank">Transcription de la vidéo</a> 

----

[![Vidéo 2part2 B4-M3-S7 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S7_video2p2-Chiffrement.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S7_video2p2-Chiffrement.mp4)

 #### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S7-script-video2_part2.md" target="_blank">Transcription de la vidéo</a> 

 
## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S7-video2-diapos.pdf" target="_blank">Supports de présentation de la vidéo</a>
