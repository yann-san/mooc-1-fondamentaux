# Exercice Pratique : DNS

1. Expliquer en 3 lignes à quoi sert le protocole DNS.

La commande `ipconfig /all` sous windows ou le fichier /etc/resolv.conf permettent de connaître le serveur DHCP utilisé. 

2. Lancer Wireshark, faire une requête vers un site improbable (www.nigeriacurling.com par exemple), pour être sûr que son adresse IP ne soit pas déjà stockée dans la mémoire cache du PC.

    2.1 Comment le PC obtient-il l’adresse IP du serveur ? Montrer pour répondre à ces questions les requêtes et réponses via Wireshark.  
    2.2 Quel est le temps de réponse ? Quels sont les ports client et serveur ?  
    2.3 Que contient le champ "data" de la réponse du serveur ?  

    Si possible, faire une requête vers le même site depuis un autre PC, équipé de wireshark, quel est le temps de réponse ? 

3. Quel est le serveur DNS du PC ? Quel est le serveur DNS de la box ?

4. Quelle machine avait la réponse à la requête la première fois ? La seconde fois ? 
