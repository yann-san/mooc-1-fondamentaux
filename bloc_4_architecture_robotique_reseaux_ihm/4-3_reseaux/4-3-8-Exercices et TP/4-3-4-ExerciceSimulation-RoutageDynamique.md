# Simulation de routage dynamique sur Cisco Packet Tracer

Reprendre le schéma PacketTracer de l'exercice sur le routage statique.
On augmente le nombre de routeur au niveau de l'école.

![image-31.png](./image-31.png)

La mise en place des règles de routage de manière statique est fastidieuse et cela ne s’adapte pas à d’éventuels défaut interne. On utilise ici le routage dynamique pour configurer les routeurs.
Pour chaque protocole, tester l’accès aux serveurs depuis le PC et expliquer les échanges permettant d’aboutir aux tables de routage.

## Routage dynamique OSPF entre les routeurs de l'école

Avant de commencer cette partie, il faut désactiver RIP sur tous les routeurs :
`Router(config)#no router rip`
Dans cette partie on met en place un routage dynamique OSPF. L'objectif est d'observer le fonctionnement du protocole. Il est possible d’activer le debug OSPF :

* `# debug ip ospf events`

Pour commencer, on met toutes les interfaces impliquées dans OSPF de tous les routeurs ENS dans la même aire, 0 par exemple. Seule l'interface du routeur ENS vers l'extérieur n'est pas concernée.
Sur chaque routeur :

* `Router#conf term`
* `Router(config)#router ospf 1`
* `Router(config-­router)#network ___.___.___.___ 0.0.0.255 area 0`

___.___.___.___ doit être remplacé par l’adresse réseau auquel le routeur est directement connecté. Le masque réseau est inversé comme on peut le voir dans l’exemple ci-dessus (/24).

Une fois cette configuration effectuée, on peut voir que les routeurs ont partagé leur configuration :

* `# show ip route`

Pour que le routeur ENS partage sa connexion par défaut, on ajoute, en mode configuration OSPF, pour ce dernier : 

* `Router(config-­router­)#network 193.55.20.0 0.0.0.255 area 1`
* `Router(config­-­router)#default-information originate`

Vérifier le bon routage en envoyant des messages Simple PDU du PC domicile à chaque serveur ENS et des serveurs ENS au serveur Orange.

## Observation des échanges OSPF

Passer en Simulation et à l'aide du bouton _Power Cycle Devices_ en bas à gauche, faire un redémarrage des équipements. 

Observer les types de messages échangés et leur contenu. Quelques pistes de réflexions :
* Combien de type de paquets voit-on ? Décrire ces messages.
* Quels sont les identifiants des routeurs ?
* Quelle est la fréquence d’émission des paquets Hello ?

Supprimer un lien entre 2 routeurs. Observer, via des passages de messages Simple PDU ou des ping la cicatrisation du réseau.

* Combien de temps s’écoulerait-il avant que la route se mette à jour ?
* Quels messages ont permis la cicatrisation du réseau ?

Recréer le lien entre les 2 routeurs. Observer, via des passages de messages Simple PDU ou des ping la cicatrisation du réseau.

* Combien de temps s’écoule-t-il avant que la route se mette à jour ?
* Quels messages ont permis la cicatrisation du réseau ?

