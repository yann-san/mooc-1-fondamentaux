1
00:00:00,750 --> 00:00:02,149
Dans les vidéos précédentes,

2
00:00:02,249 --> 00:00:03,952
nous avons couvert plusieurs techniques

3
00:00:04,052 --> 00:00:06,105
pour représenter des nombres de toute nature.

4
00:00:06,432 --> 00:00:08,003
Intéressons-nous maintenant

5
00:00:08,103 --> 00:00:09,559
à la représentation des textes.

6
00:00:09,659 --> 00:00:10,951
L'idée de base est simple,

7
00:00:11,051 --> 00:00:13,271
elle consiste à associer un nombre binaire unique

8
00:00:13,371 --> 00:00:14,253
à chaque symbole.

9
00:00:14,597 --> 00:00:16,287
On entend par symboles des lettres,

10
00:00:16,387 --> 00:00:17,503
ou de la ponctuation,

11
00:00:17,603 --> 00:00:18,583
ou même les espaces.

12
00:00:19,130 --> 00:00:20,777
Ensuite, on représente un texte

13
00:00:20,877 --> 00:00:22,548
par la séquence des nombres binaires

14
00:00:22,648 --> 00:00:24,285
qui représentent chacun de ses symboles.

15
00:00:25,775 --> 00:00:27,437
Cette idée n'est pas neuve.

16
00:00:27,812 --> 00:00:29,563
Au XIXe siècle déjà,

17
00:00:29,663 --> 00:00:32,390
l'ingénieur français Jean Maurice Émile Baudot

18
00:00:32,490 --> 00:00:34,246
invente le premier code pratique

19
00:00:34,346 --> 00:00:36,250
qui permet de représenter tout l'alphabet latin

20
00:00:36,350 --> 00:00:38,320
ainsi que des symboles de ponctuation usuels

21
00:00:38,420 --> 00:00:39,298
sur cinq bits.

22
00:00:40,431 --> 00:00:41,699
Vous voyez à l'écran

23
00:00:41,799 --> 00:00:43,181
le brevet qu'il a déposé.

24
00:00:44,093 --> 00:00:45,699
Ce code était utilisé

25
00:00:45,799 --> 00:00:47,107
pour transmettre de l'information

26
00:00:47,207 --> 00:00:49,966
entre des sortes de grosses machines à écrire électromécaniques

27
00:00:50,066 --> 00:00:51,216
qu'on pouvait commander à distance

28
00:00:51,316 --> 00:00:52,971
et qu'on appelait des téléscripteurs.

29
00:00:53,335 --> 00:00:55,285
Ces systèmes ont été largement utilisés

30
00:00:55,385 --> 00:00:56,600
pour la transmission d'information

31
00:00:56,700 --> 00:00:58,207
à travers les lignes téléphoniques

32
00:00:58,307 --> 00:01:00,337
et ce, jusque dans les années 1980.

33
00:01:00,643 --> 00:01:02,269
Leur vitesse de transmission

34
00:01:02,369 --> 00:01:03,475
se mesurait en Baud

35
00:01:03,575 --> 00:01:05,445
en référence à Émile Baudot.

36
00:01:06,364 --> 00:01:07,947
La première solution informatique

37
00:01:08,047 --> 00:01:09,043
largement utilisée

38
00:01:09,143 --> 00:01:10,389
a été le code ASCII,

39
00:01:10,489 --> 00:01:11,095
qui est l'acronyme

40
00:01:11,195 --> 00:01:13,858
d'American Standard Code for Information Interchange.

41
00:01:14,761 --> 00:01:16,997
Il a été introduit en 1967

42
00:01:17,097 --> 00:01:17,871
et servait à l'origine

43
00:01:17,971 --> 00:01:19,437
pour l'échange d'informations bancaires.

44
00:01:19,934 --> 00:01:22,459
Il associe un nombre sur 7 bits

45
00:01:22,559 --> 00:01:24,013
à 128 caractères différents

46
00:01:24,113 --> 00:01:25,666
et on le voit sur la table à l'écran.

47
00:01:25,766 --> 00:01:28,009
Les 32 premiers caractères en jaune

48
00:01:28,109 --> 00:01:30,115
ne sont pas des lettres ni des signes de ponctuation

49
00:01:30,215 --> 00:01:31,009
mais bien des codes,

50
00:01:31,109 --> 00:01:33,383
destinés à contrôler la ligne de communication.

51
00:01:33,739 --> 00:01:36,617
Pour le reste, le tableau permet de retrouver le code.

52
00:01:36,717 --> 00:01:39,647
Par exemple, le caractère W en majuscule

53
00:01:39,747 --> 00:01:42,575
a ses bits de poids faible égaux à 7 en hexadécimal

54
00:01:42,675 --> 00:01:45,064
et ses bits de poids fort égaux à 5 en hexadécimal.

55
00:01:45,164 --> 00:01:48,373
Cela donne donc un code de 57 en base 16

56
00:01:48,473 --> 00:01:51,592
soit 101 0111 en binaire.

57
00:01:52,034 --> 00:01:54,740
On peut alors retrouver la représentation binaire d'un texte

58
00:01:54,840 --> 00:01:56,588
comme MOOC NSI par exemple ;

59
00:01:56,688 --> 00:01:58,873
on remplace simplement chaque lettre

60
00:01:58,973 --> 00:02:00,162
par son code dans la table.

61
00:02:00,680 --> 00:02:02,221
Ici, on l'a fait en hexadécimal

62
00:02:02,321 --> 00:02:03,091
pour commencer

63
00:02:03,191 --> 00:02:05,151
mais on peut ensuite traduire ces nombres en binaire.

64
00:02:06,340 --> 00:02:08,040
On peut se demander pourquoi les codes ASCII

65
00:02:08,140 --> 00:02:09,999
se limitent à 7 bits.

66
00:02:10,333 --> 00:02:11,526
Le huitième bit de l'octet

67
00:02:11,626 --> 00:02:12,992
était appelé bit de parité

68
00:02:13,092 --> 00:02:14,378
et utilisé pour détecter

69
00:02:14,478 --> 00:02:15,906
d'éventuelles erreurs de transmission.

70
00:02:16,236 --> 00:02:18,215
Ce bit de poids fort est calculé

71
00:02:18,315 --> 00:02:20,704
en faisant le XOR de tous les autres bits.

72
00:02:20,833 --> 00:02:22,965
Le bit de parité vaut donc 1

73
00:02:23,065 --> 00:02:24,900
quand il y a un nombre impair de bits à 1

74
00:02:25,000 --> 00:02:26,200
dans les données à transmettre,

75
00:02:26,300 --> 00:02:27,541
et 0 autrement.

76
00:02:27,849 --> 00:02:29,085
Dans cet exemple,

77
00:02:29,185 --> 00:02:30,703
on cherche à transmettre la lettre M

78
00:02:30,803 --> 00:02:32,161
avec son bit de parité à 0.

79
00:02:32,322 --> 00:02:33,973
Une erreur de transmission a lieu

80
00:02:34,073 --> 00:02:35,198
et un bit est modifié,

81
00:02:35,298 --> 00:02:37,468
la lettre E est donc reçue par le destinataire.

82
00:02:37,845 --> 00:02:40,128
Celui-ci peut refaire le calcul du bit de parité

83
00:02:40,228 --> 00:02:42,059
et se rendre compte qu'il devrait être à 1.

84
00:02:42,159 --> 00:02:44,991
Il y a donc eu une erreur quelque part dans la transmission

85
00:02:45,091 --> 00:02:47,026
et le destinataire peut donc demander

86
00:02:47,126 --> 00:02:48,284
de retransmettre la lettre.

87
00:02:48,731 --> 00:02:50,847
Plus tard, le huitième bit du code ASCII

88
00:02:50,947 --> 00:02:53,531
a été utilisé pour ajouter 128 caractères au code.

89
00:02:54,025 --> 00:02:55,457
Ces caractères supplémentaires

90
00:02:55,557 --> 00:02:57,590
permettent de s'en servir dans d'autres langues que l'anglais

91
00:02:57,690 --> 00:03:00,246
en introduisant des caractères propres à une autre langue.

92
00:03:00,711 --> 00:03:03,146
Par exemple, le code ISO-LATIN-1

93
00:03:03,246 --> 00:03:05,868
est le même que l'ASCII pour les 128 premiers caractères

94
00:03:05,968 --> 00:03:07,736
mais offrent des caractères accentués

95
00:03:07,836 --> 00:03:09,752
pour les codes dont le bit de poids fort est à 1.

96
00:03:10,301 --> 00:03:12,867
Récemment, un projet international de normalisation

97
00:03:12,967 --> 00:03:14,852
appelé Unicode a vu  le jour.

98
00:03:15,153 --> 00:03:16,494
Il a pour but de regrouper

99
00:03:16,594 --> 00:03:17,708
tous les symboles utilisés

100
00:03:17,808 --> 00:03:18,795
dans n'importe quelle langue

101
00:03:18,895 --> 00:03:19,959
et ce, dans un seul code.

102
00:03:20,370 --> 00:03:22,843
La version 13 d'Unicode publiée en mars 2020

103
00:03:22,943 --> 00:03:25,789
comporte 143 859 caractères.

104
00:03:25,934 --> 00:03:29,352
Par exemple, le caractère A majuscule avec accent circonflexe

105
00:03:29,452 --> 00:03:31,859
possède le code 00C2 en hexadécimal

106
00:03:31,959 --> 00:03:33,955
qu'on note U+00C2

107
00:03:34,055 --> 00:03:36,183
pour indiquer qu'il s'agit du code Unicode.

108
00:03:36,630 --> 00:03:38,516
En plus d'associer un entier unique

109
00:03:38,616 --> 00:03:40,054
appelé point de code à chaque caractère,

110
00:03:40,154 --> 00:03:41,770
la norme Unicode précise également

111
00:03:41,870 --> 00:03:43,842
comment ces entiers doivent être encodés en binaire.

112
00:03:43,990 --> 00:03:47,268
L'encodage le plus courant est appelé UTF-8

113
00:03:47,368 --> 00:03:51,319
et les caractères y sont encodés sur 1, 2, 3 ou 4 octets.

114
00:03:51,663 --> 00:03:54,404
La norme Unicode est devenue aujourd'hui bien adoptée

115
00:03:54,504 --> 00:03:56,650
notamment par les sites et les navigateurs web

116
00:03:56,750 --> 00:03:58,398
ainsi que par le langage Python.

