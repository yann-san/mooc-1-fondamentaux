\chapter{Leçon 16 et 17  --
  Gestion de la mémoire primaire}\label{cha:memoire}


Dans notre présentation des programmes en langage machine nous avons
volontairement passé sous silences certaines complications dues aux
adresses mémoires. Prenons comme exemple un programme très simple, en
<<~pseudo-code machine~>>{}:
\begin{verbatim}
   R1 := 5
   R2 := 3
   R3 := 0
d: R3 := R3 + R1
   R2 := R2 - 1
   if R2 != 0 jump d
\end{verbatim}
Ce programme initialise les 3 registres \verb-R1-, \verb-R2- et
\verb-R3- à 5, 3 et 0, puis commence une boucle dans laquelle il:
$(i)$ ajoute \verb-R1- dans \verb-R3-, et $(ii)$ décrémente \verb-R2-,
et ce, tant que \verb-R2- est différent de $0$. En d'autres termes, ce
programme multiplie \verb-R1- par \verb-R2- et met le résultat dans
\verb-R3-.

Concrètement, l'instruction de saut conditionnel doit contenir une
adresse mémoire, c'est-à-dire une valeur numérique, qui sera placée
dans le registre PC au moment de son exécution, et non pas d'un
symbole \verb-d- comme nous l'avons représenté pour faciliter la
lecture. Au moment où le programme en question est créé, il faut donc
disposer de l'adresse à laquelle l'instruction de la quatrième ligne
sera stockée en mémoire\ldots Une façon simple d'obtenir cette adresse
(et les autres adresse du programme) est de connaître \emph{l'adresse
  de début} du programme en mémoire (autrement dit, l'adresse de la
première instruction). Si on a cette information, connaître l'adresse
de chaque instruction est facile puisqu'on connaît la longueur de
l'instruction. Malheureusement, on ne dispose pas toujours de cette
information au moment où on \emph{écrit} le programme. Regardons
plusieurs cas:
\begin{itemize}
\item Sur une machine sans OS et mono-tâche, le problème se résout
  facilement. Le programme dispose normalement de toute la mémoire. On
  peut donc supposer qu'il est toujours chargé à partir de la même
  adresse, par exemple l'adresse $0$. C'est la situation la plus
  confortable, car l'adresse de début est connue \emph{à l'avance},
  c'est-à-dire au moment de la réalisation du programme
\item Sur une machine avec OS, l'OS prend de la place en
  mémoire. Comme l'OS est le premier programme chargé, il se placera
  typiquement au début de la mémoire. La place occupée n'est pas
  toujours la même car la taille de l'OS peut varier d'une version à
  l'autre, et que certains modules de l'OS ne sont pas forcément
  chargés à chaque fois (en fonction du matériel disponible sur la
  machine par exemple). La première adresse libre après l'OS peut donc
  changer, et il n'est pas acceptable d'avoir à ré-écrire/recompiler
  tous ses programmes chaque fois qu'on fait, par exemple, une mise à
  jour de l'OS. On pourrait imaginer réserver une zone (par exemple
  les 64 premiers Mo) pour l'OS, et charger les programmes
  utilisateurs toujours à partir de la même adresse, mais c'est
  limitatif pour l'OS, et on risque d'avoir beaucoup de mémoire
  inutilisée.
\item Dans le cas des systèmes multi-tâches, le problème est encore
  plus aigu, puisqu'il peut y avoir plusieurs programmes en mémoire,
  et l'adresse de début d'un programme dépend donc des autres
  programmes en mémoire\ldots Dans ce cas, l'adresse de début du
  programme ne sera connue que lors de l'exécution du programme.
\end{itemize}

Au moment où l'on connaît l'adresse de début du programme en mémoire
(dans les pires cas, c'est au moment de l'exécution), il faut alors
copier le programme en mémoire (le charger) en modifiant toutes les
adresses des \emph{sauts}. Cela signifie qu'on doit être capable de
savoir où ces sauts se trouvent, où se trouvent les étiquettes\ldots 

Cette solution est assez fastidieuse. La solution la plus simple est
clairement celle où le programme est réalisé en tenant compte qu'il
sera chargé à partir de l'adresse zéro, mais il faut clairement
adopter un mécanisme qui permet de combiner cette facilité avec le
fait que plusieurs programmes peuvent être présents en mémoire en même
temps\ldots

\section{Pagination} 
Le mécanisme de \emph{pagination} (à la demande) résout ce problème de
manière élégante et efficace (et permet d'augmenter les capacités de
l'ordinateur comme nous le verrons plus tard):
\begin{itemize}
\item Du côté du programme: celui-ci est réalisé et s'exécute comme
  s'il commençait à l'adresse $0$. Il est chargé tel quel en
  mémoire. \'Evidemment, si plusieurs programmes sont présents en même
  temps en mémoire, il n'est pas possible de les faire commencer tous
  à l'adresse 0, ce qui signifie que les adresses données dans les
  instructions de ces programmes ne sont, en général, pas
  \emph{correctes}: on parle d'\emph{adresses virtuelles} puisque le
  programme s'exécute \emph{virtuellement} à partir de l'adresse 0.
\item \emph{Concrètement}, le programme est réparti en différents
  endroits de la mémoire, et il faut donc avoir un mécanisme qui
  traduise les adresses virtuelles en \emph{adresse réelles}
  (c'est-à-dire les véritables numéros des cases mémoire). Cette
  traduction est réalisé à l'aide d'un circuit spécialisé du
  microprocesseur: le MMU, \textit{Memory Management Unit}. Le
  processeur manipule donc, lui aussi, des adresse virtuelles (en tous
  cas dans ses registres), et ce n'est qu'au moment où il doit accéder
  à la mémoire qu'il traduit les adresses virtuelles en adresses
  réelles, car la mémoire ne manipule que des adresses réelles.
\end{itemize}

Le mécanisme de \emph{pagination} fonctionne de la façon
suivante. Considérons un programme de longueur $L$, à exécuter sur un
ordinateur ayant une mémoire de taille $M$.
\begin{itemize}
\item Le programme est \emph{découpé} en portions qui sont toutes de
  taille égale $\ell$ (sauf potentiellement la dernière si $L$ n'est
  pas un multiple de $\ell$). Ces portions sont appelées des
  \emph{pages}. Chaque page correspond donc à une plage d'adresses
  virtuelles:
  \begin{itemize}
  \item La première page correspond aux adresse virtuelles de $0$ à
    $\ell-1$, puisque le programme a été conçu <<~comme si~>> il
    s'exécutait avec sa première instruction à l'adresse $0$.
  \item La seconde page contient les adresses virtuelles de $\ell$ à
    $2\ell-1$.
  \item \ldots
  \item De manière générale, la $i$\ieme{} page contient les adresses
    virtuelles allant de $(i-1)\times \ell$ à $i\times \ell -1$.
  \end{itemize}
  Le programme sera donc composé de $L/\ell$ pages si $L\mod \ell=0$,
  ou de $\lfloor L/\ell\rfloor+1$ pages si $L\mod\ell\neq 0$.
\item Symétriquement, la mémoire est découpée en portions de taille
  $\ell$ également qui peuvent donc chacune accueillir une page du
  programme. Ces portions sont appelées des \emph{cadres de
    pages}. Chaque cadre de page correspond lui à une plage d'adresses
  réelles: la page $i$ contient les adresses réelles allant de
  $(i-1)\times \ell$ à $i\times \ell -1$. La mémoire contient donc au
  plus $\lfloor M/\ell\rfloor$ cadres de page.
\item \`A chaque programme chargé en mémoire est associé une table qui
  indique \emph{dans quel cadre de page est chargée chacune des
    pages}. En effet, la souplesse du mécanisme de pagination permet
  de charger n'importe quelle page dans n'importe quel cadre de page,
  et ce, de manière non-contiguë, et dans le désordre si
  nécessaire. Cette table est appelée la \emph{table des pages}, et
  chacune des ses entrées est appelée un \emph{descripteur de
    page}. Elle est utilisée par le MMU pour assurer la traduction des
  adresses virtuelles vers les adresses réelles.
\end{itemize}

\subsection{Travail du MMU} Vérifions maintenant que la table des
pages est suffisante pour permettre au MMU de faire son travail de
traduction. Considérons une adresse virtuelle $\alpha$ (nous avons
donc $0\leq \alpha\leq L-1$:
\begin{enumerate}
\item La première chose à faire pour exploiter le descripteur consiste
  à trouver \emph{dans quelle page se trouve l'adresse}: il s'agit de
  la page:
  $$
  \alpha\div \ell
  $$ 
  (où $\div$ indique une division entière).
\item La table des pages nous renvoie \emph{le numéro du cadre de
    page contenant la page qui nous intéresse}:
  $$
  \desc(\alpha\div\ell)
  $$
\item On peut alors en déduire \emph{l'adresse réelle du début du
    cadre de page} c'est-à-dire \emph{l'adresse réelle du début de la
    page}, puisque chaque cadre de page est de longueur $\ell$:
  $$
  \ell\times\desc(\alpha\div\ell)
  $$
\item On calcule ensuite le décalage de l'adresse $\alpha$ par rapport
  au début de la page qui la contient: $\alpha\mod\ell$.
\item Ce décalage est le même par rapport au début du cadre de page,
  puisque la page est chargée intégralement et sans modification dans
  le cadre de page. On connaît donc maintenant le début de la page en
  mémoire et le décalage de l'adresse qui nous intéresse par rapport
  au début de la page. Il suffit alors de faire la somme. L'adresse
  réelle est donc: 
  $$
  \ell\times\desc(\alpha\div\ell) + (\alpha\mod\ell)
  $$
\end{enumerate}
Toutes ces opérations peuvent sembler lourdes, surtout quand on pense
qu'il faut les effectuer à chaque instruction (en effet, le registre
$IR$ est chargé à partir de l'adresse donnée dans $PC$, qui est une
adresse virtuelle). En pratique, elles peuvent être réalisées
facilement si $\ell$ est une puissance de $2$ (par exemple $2^k$). En
effet, pour des adresses sur $32$ bits par exemple:
\begin{itemize}
\item On obtient $\alpha\div\ell$ en isolant les $32-k$ bits de poids
  forts de l'adresse (revient à faire un décalage de $k$ positions
  vers la droite).
\item On réalise la multiplication de $\desc(\alpha\div\ell)$ par
  $\ell=2^k$ en décalant $\desc(\alpha\div\ell)$ de $k$ positions
  vers la gauche
\item On réalise le modulo en isolant les $k$ bits de poids faible de
  l'adresse virtuelle.
\end{itemize}
On constate alors que la valeur $\ell\times\desc(\alpha\div\ell)$ est
un nombre dont les $k$ bits de poids faibles sont à zéro. Comme on y
additionne le nombre $\alpha\mod\ell$ sur $k$ bits, cela revient à
remplacer les $k$ bits de poids faibles de
$\ell\times\desc(\alpha\div\ell)$ par les $k$ bits de
$\alpha\mod\ell$. Au final, l'adresse virtuelle $\alpha$ possède les
mêmes $k$ bits de poids faibles que son adresse physique
correspondante. Par contre, les $32-k$ bits de poids fort de
$\alpha$, qui constituent le numéro de page, ont été remplacés par les
$32-k$ bits de $\desc(\alpha\div\ell)$, soit le numéro de cadre de
page. Pour conclure, si la taille des pages est de $\ell=2^k$, le
travail du MMU \emph{consiste uniquement à remplacer les $32-k$ bits
  de poids forts de l'adresse -- qui constituent le numéro de page --
  par les bits du numéro de cadre de page correspondant}.

\section{Pagination à la demande} 
Ce système est un perfectionnement du précédent. Il se base sur
l'observation qu'à un moment donné, on n'a jamais besoin, pour
exécuter un programme, que de la page qui contient la prochaine
instruction à exécuter, et des pages qui contiennent les données
nécessaires à cette instruction. Il n'est donc pas nécessaire d'avoir
en permanence en mémoire toutes les pages du programme qui
s'exécute. Cette observation a plusieurs conséquences:
\begin{itemize}
\item Cela permet de ne charger que ce qui est nécessaire: économie de
  temps et de mémoire.
\item Cela permet potentiellement d'exécuter des programmes qui sont
  plus gros que la mémoire disponible (en ne chargeant que ce qu'il
  faut de pages).
\end{itemize}
Par contre, il faut $(i)$ trouver un endroit où stocker les pages qui
ne sont pas chargées, $(ii)$ que le MMU puisse déterminer quelles
pages sont en mémoire et $(iii)$ disposer d'un mécanisme qui permette
de rapatrier des pages en mémoire quand on veut y accéder et qu'elles
ne sont pas chargées. Voyons ces points en détail:
\begin{enumerate}
\item Les pages inutilisées seront stockées sur une mémoire, mais pas
  la mémoire principale, puisqu'on tente de la libérer. On stockera
  donc les pages inutilisées sur la mémoire secondaire.
\item Le MMU disposera d'un champ supplémentaire dans le descripteur
  de chaque page: un bit de présence indiquant si la page est présente
  en mémoire primaire ou pas.
\item Le travail du MMU sera quelque peu modifié. Considérons une
  adresse virtuelle $\alpha$: le MMU va, comme avant, déterminer le
  numéro de page $i=\alpha\div\ell$.  Il va consulter le bit de
  présence associé: $\pres(i)$. Si la page est présente en mémoire, le
  MMU continue comme dans le cas de la pagination simple. Sinon, il
  faut que le MMU fasse en sorte que la page soit chargée en mémoire,
  et que le numéro de cadre de page choisi soit indiqué dans le
  descripteur.

  Malheureusement, la page est présente en mémoire secondaire, et le
  MMU n'a pas accès directement à cette mémoire, pour plusieurs
  raisons: il n'a pas accès en général aux périphériques (seul le CPU,
  s'il s'exécute en mode maître, peut y accéder), et il ne dispose pas
  d'informations sur l'organisation du disque (celle-ci est dépendante
  de l'OS qui s'exécute sur la machine). Par contre, l'OS possède
  toutes ces informations: c'est lui qui gère les périphériques, et
  c'est lui qui a lancé les programmes qui s'exécutent. Le MMU doit
  donc s'arranger, en cas de \emph{défaut de page} (\textit{page
    fault}) pour faire en sorte que l'OS prenne la main et charge la
  page au bon endroit en mémoire. Pour ce faire, le MMU déclenche un
  \emph{appel système} pour gérer le défaut de page. Le gestionnaire
  correspondant chargera alors la page manquante et rendra la main au
  programme.
\end{enumerate}

\paragraph{Remarques} Il faut encore remarquer plusieurs choses:
\begin{enumerate}
\item Pour pouvoir charger une page en mémoire, encore faut-il qu'il y
  ait un cadre de page libre. Ce n'est pas toujours le cas, car la
  mémoire peut être bien plus petite que la somme des programmes en
  cours d'exécution. Il faut donc \emph{choisir une victime}, un cadre
  de page qu'on libérera, et dont on devra impérativement copier le
  contenu sur le disque (au cas où le programme aurait écrit dans la
  page correspondante). Le choix de la victime peut donner lieu à
  plusieurs stratégies:
  \begin{enumerate}
  \item Une technique classique consiste à choisir la page \emph{la
      moins récemment utilisée}. On peut néanmoins trouver un exemple
    où cette technique ne fonctionne pas de façon efficace: si on a
    une boucle qui parcourt itérativement les $n+1$ pages alors qu'il
    n'y a que $n$ cadres de page.
  \item Une autre technique est la technique FIFO, qui choisit la page
    \emph{qui a été chargée depuis le temps le plus long}, et ce,
    indépendemment de son utilisation. Pour ce faire, on maintient un
    compteur sur chaque cadre de page, qui vaut initialement 0. À
    chaque {\em page fault}, on incrémente ce compteur de 1 pour
    chaque cadre de page, sauf pour celui qui a déclenché le
    \textit{page fault}, pour lequel on met le compteur à 0 (cela
    permet de toujours borner les compteurs). On choisit toujours
    comme victime la page qui a la valeur maximale. Cet algorithme ne
    fonctionne néanmoins pas mieux sur l'exemple précédent.
  \end{enumerate}
\item Copier une page sur le disque et charger une nouvelle page prend
  énormément de temps, en raison de la lenteur de la mémoire
  secondaire. Dans le cas d'un système fortement chargé, ces
  chargements peuvent avoir lieu trop souvent, et ralentir très
  fortement la machine jusqu'à la rendre inutilisable. On parle alors
  de \textit{trashing}.
\item La pagination génère un phénomène de \emph{fragmentation
    interne}: la dernière page du programme n'est jamais complètement
  <<~remplie~>> (en moyenne, il y a $\ell/2$ cases mémoires
  inutilisées, si $\ell$ est la taille d'une page), et donc, il y a de
  la mémoire inutilisée dans le cadre de page qui la contient. On
  pourrait donc penser que diminuer la taille des pages $\ell$ est une
  bonne idée, car cela va diminuer la fragmentation. Malheureusement,
  des pages plus petites impliquent d'avantage de \textit{page faults}
  et donc d'avantage de lecture sur le disque, ce qui prend toujours
  du temps. Le cas problématique typique est celui où une matrice est
  stockée sur plusieurs pages, et où le parcours passe continuellement
  d'une page à l'autre, pages qui doivent être chargées depuis la
  mémoire secondaire. Par ailleurs, augmenter le nombre de pages (en
  diminuant la taille des pages) augmente également la taille de la
  table des pages qui risque de ne plus tenir en mémoire !
\item Pour pouvoir gérer un \textit{page fault}, il faut exécuter la
  routine de gestion d'interruption correspondante. Pour cela encore
  faut-il qu'elle soit en mémoire ! Autrement dit, il ne faut pas que
  le \textit{page fault} déclenche un nouveau \textit{page fault}. Ces
  pages doivent donc toujours rester en mémoire.
\end{enumerate}



% \section{Techniques de segmentation}
% Une technique similaire à la pagination est celle de la
% \emph{segmentation}. Cette technique est basée sur la constatation
% suivante. Supposons qu'un programme a besoin de 2 tables pour stocker
% ses données, et que ces tables peuvent devenir très grandes. Supposons
% également que le programme utilise des adresses virtuelles qui
% commencent à l'adresse $0$ (comme d'habitude l'OS gère cela, que ce
% soit à l'aide de pagination ou une autre technique). On peut donc
% arbitrairement décider, par exemple, de faire commencer la première
% table à l'adresse $0$, la seconde à l'adresse $1000$, et faire
% commencer le code à l'adresse $2000$.  Mais que se passe-t-il si on
% doit finalement stocker plus de $1000$ données dans une des tables ?
% Soit on écrase des données en mémoire si on ne détecte pas le
% problème, soit il faut le gérer ce qui peut être compliquée.

% Au contraire, il serait pratique que le programme puisse
% \emph{demander} à l'OS de disposer de $3$ zones de mémoire dont les
% adresses commencent toutes à $0$: les deux premières pour les deux
% tables, et la troisième pour le code. Chaque zone de mémoire ainsi
% obtenue est appelée un \emph{segment}. Le programme manipule alors des
% \emph{adresse virtuelles} qui sont des paires 
% $$
% \big(\textrm{num. segment}, \textrm{adresse relative dans le segment}\big)
% $$
% Comme tous les segments commencent à zéro ils sont \emph{virtuellement
%   illimités}, et on ne risque pas d'écraser des données d'un autre
% segment. C'est à l'OS de gérer les segments en mémoire de manière à ce
% que tout se passe bien. 

% La segmentation ressemble donc très fort à la pagination avec cette
% différence que \emph{les pages sont de taille fixes alors que les
%   segments sont de taille variable}.

% Quand un programme veut accéder à l'adresse $\alpha$ dans le segment
% $\sigma$, il faut donc:
% \begin{enumerate}
% \item vérifier que le segment $\sigma$ est bien mémoire. Si oui, on
%   doit connaître sont adresse de début $a_\sigma$, et l'adresse réelle
%   est donc $a_\sigma+\alpha$. L'OS doit donc s'assurer que cette
%   adresse réelle est bien disponible pour le segment, en augmentant sa
%   taille si nécessaire. Cela peut donner lieu à la suppression de la
%   mémoire de certains autres segments ($\rightarrow$ exemples).
% \item si le segment n'est pas en mémoire, il faut le charger. Cette
%   procédure est plus compliquée que pour les pages, car la mémoire
%   n'est pas découpée en parties de taille régulière comme les cadre de
%   page. Il faut donc potentiellement supprimer \emph{plusieurs
%     segments} de la mémoire pour en charger un.
% \end{enumerate}

% L'intérêt principal de la segmentation est donc d'offrir une plus
% grande flexibilité, mais la gestion en est plus lourde pour l'OS. Par
% ailleurs, avec la segmentation, il y a un risque de
% \emph{fragmentation externe}: à force de charger des segments en
% mémoire, on crée des petites portions de mémoire entre les segments
% qui risquent d'être trop petites que pour pouvoir être récupérées. On
% peut y remédier en procédant à un tassement, mais c'est fort coûteux
% ($\rightarrow$ exemple).

% Il est parfois possible de combiner pagination et segmentation
% (contenu du segment découpé en pages, etc) ou de choisir l'un ou
% l'autre sur la même machine (e.g.: Pentium IV).

\section{Exemple: mémoire virtuelle l'Ultra Sparc III}
Ce processeur est un processeur 64 bits, mais les adresses mémoire ne
peuvent faire que $44$ bits. La mémoire est virtuellement découpée en
deux blocs: un bloc pour le code et un bloc pour les données, qui
commencent tous les deux à $0$. Donc une adresse contient:
\begin{enumerate}
\item Un bit pour indiquer la zone mémoire
\item $43$ bits d'adresse dans cette zone. Un programme peut donc
  faire au maximum $2^{43}$ octets.
\end{enumerate}
Par ailleurs, la mémoire physique maximale est limitée à $2^{41}$
bits. Le mécanisme de traduction des adresse virtuelles en adresses
réelles doit donc transformer une adresse sur $64$ bits (dont seuls
$44$ sont significatifs) en une adresse sur $41$ bits.  La traduction
exacte dépend de la taille des pages, qui peut être configurée: soit
8Ko, soit 64Ko, soit 512 Ko, soit 4Mo. % La Fig. 6--17 du livre illustre
% les différentes traductions possibles.

Le problème de ce système est le nombre potentiellement très important
de pages. Pour le cas où on a des pages de 8Ko, le décalage dans une
page tient sur $13$ bits. Cela signifie donc que, des $44$ bits
significatifs restant, $31$ bits servent à coder le numéro de page. Il
peut donc y avoir $2^{31}$ pages, soit plus de $2$ milliards de
pages. Si un descripteur d'une page tient sur 1 octet (ce qui est très
optimiste), une table des pages peut donc occuper plus de 2 Go !

En pratique, l'Ultra Sparc utilise un mécanisme à deux niveaux. Pour
chaque partie de la mémoire (code ou données):
\begin{enumerate}
\item Les descripteurs des $64$ pages utilisées les plus récemment
  sont stockés dans un circuit spécial appelé TLB (\textit{Transaction
    Lookaside Buffer}), qui permet d'obtenir très rapidement
  l'information recherchée (en un seul cycle d'horloge, le numéro de
  page est comparé à toutes les infos stockées dans la table).
\item Si l'information cherchée est trouvée, elle est transmise au MMU
  qui travaille normalement (en déclenchant une \textit{page fault} si
  nécessaire).
\item Sinon, un autre type d'interruption est déclenché: un
  \textit{TLB miss}, et c'est donc à l'OS de gérer l'absence
  d'information dans la table des pages en chargeant si nécessaire les
  informations dans la TLB. Pour ce faire, il peut être aidé par un
  autre circuit du processeur le TSB \textit{Translation Storage
    Buffer}, qui est une sorte de TLB plus grande mais plus lente. Au
  pire cas, ni le TSB ni le TLB ne contiennent l'information demandée,
  et l'OS doit donc consulter d'autres tables qu'il maintient lui-même
  (ce qui est le plus lent).
\end{enumerate}

Il faut bien faire la différence entre \textit{TLB miss} et \emph{page
  fault}:
\begin{itemize}
\item Dans un \textit{TLB miss} on recherche l'information sur une
  page donnée. On ne sait donc pas si la page est présente en mémoire
  ou pas (elle pourrait l'être). Une fois le \textit{TLB miss} résolu,
  on peut encore avoir un \textit{page fault}, mais pas toujours.
\item Dans un \textit{page fault}, on a l'information sur la page
  recherchée et on sait qu'elle n'est pas en mémoire. Un \textit{page
    fault} ne sera jamais suivi d'un \textit{TLB miss} pour la mémoire
  page.
\end{itemize}

\begin{center}
  \aldineleft\vspace*{1cm} \decoone\vspace*{1cm} \aldineright
\end{center}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
