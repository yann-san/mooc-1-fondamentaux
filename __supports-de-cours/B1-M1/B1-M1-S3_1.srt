1
00:00:00,705 --> 00:00:01,893
Dans cette vidéo,

2
00:00:01,993 --> 00:00:03,565
nous allons voir comment représenter

3
00:00:03,665 --> 00:00:05,936
en binaire des informations très simples

4
00:00:06,036 --> 00:00:07,321
à savoir les nombres.

5
00:00:07,762 --> 00:00:09,367
Pour ce faire, nous allons d'abord étudier

6
00:00:09,467 --> 00:00:10,479
la notion de base

7
00:00:10,579 --> 00:00:12,262
en partant d'un exemple.

8
00:00:12,781 --> 00:00:16,390
Considérons le nombre 314,15.

9
00:00:16,490 --> 00:00:18,192
On peut le décomposer en

10
00:00:18,292 --> 00:00:19,133
trois centaines

11
00:00:19,233 --> 00:00:20,308
une dizaine

12
00:00:20,408 --> 00:00:21,402
quatre unités

13
00:00:21,502 --> 00:00:22,704
un dixième

14
00:00:22,804 --> 00:00:24,559
et cinq centièmes.

15
00:00:25,065 --> 00:00:26,884
On voit que cette décomposition

16
00:00:26,984 --> 00:00:28,644
se réfère à des puissances de 10

17
00:00:28,744 --> 00:00:30,407
qui sont comptées à partir de la virgule.

18
00:00:30,507 --> 00:00:33,566
En effet, une centaine est égale à 10 exposant 2,

19
00:00:33,666 --> 00:00:35,453
une dizaine est égale à 10 exposant 1,

20
00:00:35,553 --> 00:00:37,465
une unité est égale à 10 exposant 0,

21
00:00:37,565 --> 00:00:39,676
un dixième égale à 10 exposant -1

22
00:00:39,776 --> 00:00:42,273
et un centième correspond à 10 exposant -2.

23
00:00:42,373 --> 00:00:44,753
On dit alors que cette valeur de référence,

24
00:00:44,853 --> 00:00:46,726
10, est la base.

25
00:00:47,039 --> 00:00:49,178
On parle de représentation décimale

26
00:00:49,278 --> 00:00:50,356
et positionnelle.

27
00:00:50,817 --> 00:00:52,438
On peut aussi observer qu'en base 10,

28
00:00:52,538 --> 00:00:55,574
on utilise les chiffres de 0 à 9 dans cette décomposition.

29
00:00:55,674 --> 00:00:56,319
Ce qui est logique,

30
00:00:56,419 --> 00:00:57,809
car quand on dépasse 9,

31
00:00:57,909 --> 00:00:59,828
on passe à la puissance de 10 suivante.

32
00:01:00,339 --> 00:01:03,495
Que se passe-t-il si nous considérons maintenant la base 2 ?

33
00:01:03,692 --> 00:01:04,619
Tout d'abord,

34
00:01:04,719 --> 00:01:06,490
on n'utilisera que les chiffres 0 et 1,

35
00:01:06,590 --> 00:01:07,816
c'est-à-dire les bits.

36
00:01:07,916 --> 00:01:11,336
Ensuite, ces chiffres se référeront à des puissances de 2.

37
00:01:11,436 --> 00:01:15,136
Ainsi, le nombre 101,11 en base 2

38
00:01:15,236 --> 00:01:17,599
se décompose en 1 fois 2 exposant 2,

39
00:01:17,699 --> 00:01:19,295
0 fois 2 exposant 1,

40
00:01:19,395 --> 00:01:20,869
1 fois 2 exposant 0,

41
00:01:20,969 --> 00:01:22,207
1 fois 2 exposant -1

42
00:01:22,307 --> 00:01:23,785
et 1 fois 2 exposant -2.

43
00:01:23,938 --> 00:01:27,035
Soit 4 + 0 + 1 + 1/2 + 1/4

44
00:01:27,135 --> 00:01:30,076
ce qui fait 5,75 en base 10.

45
00:01:30,638 --> 00:01:33,068
Pour être certains que nous interprétons correctement les nombres,

46
00:01:33,168 --> 00:01:35,186
nous noterons souvent la base en indice.

47
00:01:35,361 --> 00:01:37,944
Enfin, notons que le bit le plus à droite

48
00:01:38,044 --> 00:01:39,286
est appelé le bit de poids faible

49
00:01:39,386 --> 00:01:40,769
et que celui le plus à gauche

50
00:01:40,869 --> 00:01:42,556
est appelé le bit de poids fort.

51
00:01:43,121 --> 00:01:44,428
En informatique,

52
00:01:44,528 --> 00:01:46,072
les bases généralement utilisées

53
00:01:46,172 --> 00:01:47,928
sont la base 2, qu'on appelle binaire,

54
00:01:48,028 --> 00:01:49,705
la base 8, qu'on appelle octale,

55
00:01:49,805 --> 00:01:51,722
la base 10 qu'on appelle décimale

56
00:01:51,822 --> 00:01:52,695
et la base 16.

57
00:01:52,845 --> 00:01:55,664
Pour cette dernière, on parle d'hexadécimal

58
00:01:55,764 --> 00:01:57,734
et on remplace les chiffres de 10 à 15

59
00:01:57,834 --> 00:01:59,845
par les lettres de a à f

60
00:01:59,945 --> 00:02:01,191
pour éviter toute confusion.

61
00:02:01,698 --> 00:02:03,636
Enfin, notons qu'en Python, on peut utiliser les préfixes

62
00:02:03,736 --> 00:02:06,352
0b, 0o et 0x

63
00:02:06,452 --> 00:02:08,046
pour indiquer que des entiers sont exprimés

64
00:02:08,146 --> 00:02:10,383
en binaire, en octal ou en hexadécimal.

65
00:02:10,739 --> 00:02:12,952
Et on peut aussi utiliser les fonctions bin,

66
00:02:13,052 --> 00:02:14,382
oct et hex

67
00:02:14,482 --> 00:02:15,951
pour obtenir la chaîne de caractères

68
00:02:16,051 --> 00:02:18,033
qui représente un entier en binaire,

69
00:02:18,133 --> 00:02:20,502
en octal ou en hexadécimal.

