MOOC NSI - fondamentaux
chapitre 1
section 1
video 1_1_8
durée : 5'

Titre :  Représentation des nombres réels

Dans la vidéo précédente, nous avons vu comment représenter des nombres négatifs en binaire, c'est-à-dire sans devoir recourir au signe -

Dans cette vidéo nous allons voir comment représenter des nombres avec une partie fractionnaire, sans devoir utiliser la virgule.


Supposons que nous voulons représenter l'information sur 32 bits. Considérons le nombre
7,625
qui se représente 
111,101
en binaire.

On peut l'écrire sur 32 bits en ajoutant des 0 à gauche et à droite. Observons bien que ces 0 supplémentaires ne modifient pas la valeur du nombre

On peut alors imaginer une technique simple pour représenter ces nombres qui consiste à fixer arbitrairement la position de la virgule . Par exemple, on pourrait dire que les 16 bits de poids fort représentent la partie entière, et que les 16 bits de poids faible représentent la partie fractionnaire.

Dans notre exemple, on obtient donc cette représentation.


C'est une solution simple, mais elle est en fait trop... simpliste !

Avec cette solution, par exemple, on ne peut pas représenter la valeur 2^-17, alors que cette valeur tient sur 18 bits en binaire.


On voit bien que le problème est que la position de la virgule est fixe. De ce fait, les bits de la partie entière ne peuvent pas être utilisés pour gagner de la précision dans la partie fractionnaire, et vice-versa.

En pratique, la solution retenue est donc celle de la virgule flottante. Celle-ci consiste à réserver une partie de la représentation à un entier qui indique la position de la virgule.

On en trouve un exemple dans la norme IEEE754, qui est une norme industrielle utilisée par de nombreux microprocesseurs. Nous allons maintenant en expliquer l'idée sur un exemple sur 32 bits.


Considérons le nombre 0,00101 à représenter selon la norme IEEE754. On commence par déplacer la virgule de façon à obtenir un nombre de la forme 1,quelque chose.

Pour compenser ce déplacement de la virgule, on doit multiplier cette représentation par une puissance de 2. Dans notre cas, on doit déplacer la virgule de 2 positions vers la gauche, ce qui revient à multiplier par 2^-2.


On voit maintenant apparaître les deux éléments principaux de la représentation: 

- d'une part, les bits de données 01 qui sont ceux qui apparaissent après la virgule. On sait de toute manière qu'il y aura toujours un 1 avant la virgule.
- d'autre part, l'entier qui indique la position de la virgule, c'est-à-dire l'exposant -2 de la puissance de 2.

Ces deux informations sont suffisantes pour reconstruire la valeur 0,00101. Il faudra y ajouter une troisième au cas où le nombre à représenter est négatif.


Voyons maintenant comment on représente tout cela en pratique. Les 32 bits de la représentation sont divisés en:
- 1 bit de signe, qui vaut 1 quand le nombre est négatif
- 8 bits qui représentent l'exposant de la puissance de 2
- 23 bits pour les bits après la virgule.


Reprenons notre exemple. On commence par remplir les 23 bits de données. On y indique 01 suivi de 21 zéros pour avoir 23 bits. 

Attentions à bien ajouter les zéros à droite afin de ne pas modifier la partie fractionnaire.

Ensuite, on peut mettre le bit de signe à 0.

Finalement, on peut stocker la valeur -2 dans les 8 bits d'exposant. Comme cette valeur est signée, il faut choisir une technique pour les nombres négatifs. 

Dans ce cas, on n'utilisera pas le complément à deux, mais l'excès à 127. Cette technique consiste à ajouter 127 à la valeur à représenter pour avoir un nombre positif. On représente ensuite ce nombre positif en base 2.

Dans notre exemple, -2 + 127 = 125 qui se représente 0111 1101 en binaire.



Cet exemple donne une idée de la norme IEEE754, qui est beaucoup plus étendue. En particulier, on peut noter qu'il faut trouver une représentation spéciale pour le zéro. Ou encire qu'il existe des versions plus précises sur 64 et 128 bits, etc

 