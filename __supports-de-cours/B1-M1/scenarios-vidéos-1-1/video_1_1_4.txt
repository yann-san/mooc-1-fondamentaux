MOOC NSI - fondamentaux
chapitre 1
section 1
video 1_1_4
durée : 2'

Titre :  Changements de base

Dans la vidéo précédente, nous avons vu qu'un même nombre peut être exprimé dans plusieurs bases. Dans cette vidéo, nous allons voir comment changer de base pour exprimer un nombre dans une base b donnée.




Pour comprendre la technique, nous allons voir un exemple où l'on veut exprimer la valeur 13 en base 2.

Pour ce faire, nous allons commencer par diviser 13 par 2. Nous allons faire une division entières, nous obtenons donc, comme résultat, 6, et un reste de 1.

Comme nous divisons par 2, nous pouvons observer que le rester sera toujours soit 0, soit 1.

Cette division nous permet d'exprimer la valeur 13 comme 1 (le reste) + 6 x 2

Nous allons maintenant recommencer, en divisant à nouveau par 2 le résultat 6. Nous obtenons 3 et un reste de 0. 

Cela nous permet d'affirmer que 6 vaut 0 + 3 x 2. En injectant ce résultat dans l'expression de 13 trouvée avant, on voit maintenant que 13 vaut 1 plus 0 fois 2  plus 3 fois 2^2.

On voit que les restes que nous avons calculés (en rouge sur l'écran) apparaissent multipliés par des puissances de la nouvelle base 2, dans cette décomposition que nous sommes occupés à calculer.

On continue de cette façon: 3 divisé par 2 donne 1 reste 1, qu'on reporte dans l'expression de 13, et finalement, 1 divisé par 2 donne 0 reste 1.

L'expression qu'on obtient de la valeur d'origine 13 est bien une décomposition en puissances de nouvelle base 2. On voit donc que 13 s'exprime comme 1101 en base 2, ce qui revient à lire la suite des restes calculés du dernier au premier.





Certains changements de base sont plus faciles, quand on passe à une base qui est une puissance de la base d'origine. C'est le cas avec les bases usuelles en informatique, à savoir l'hexadécimal, l'octal et le binaire. En effet 16 = 2^4 et 8 = 2^3. Convertir de la base 2 aux bases 8 et 16, et vice-versa, est donc simple.

Si on veut convertir le nombre binaire 110 1111 en base 8, on commence par diviser ce nombre en paquets de 3 bits, en commençant par le bit des poids faible. On choisit des paquets de 3 bits car 8 vaut 2^3.

On convertit ensuite chacun de ces paquets en un chiffre en octal. Par exemple, 111 en binaire se représente 7 en octal, et ainsi de suite. On obtient alors le résultat: 110 1111 s'exprime 157 en base 8.

Pour la base 16, on procède en groupant les bits par paquet de 4, puisque 16 = 2^4. 


Et on trouve que 110 1111 se note 6F en hexadécimal. 




