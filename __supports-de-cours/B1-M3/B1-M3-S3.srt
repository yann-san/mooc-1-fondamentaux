1
00:00:04,379 --> 00:00:07,472
Nous nous retrouvons avec nos pays

2
00:00:07,572 --> 00:00:10,722
et nous allons cette fois effectuer des tris

3
00:00:13,604 --> 00:00:14,943
dans nos données.

4
00:00:15,830 --> 00:00:18,464
Par exemple, un tri simple

5
00:00:18,564 --> 00:00:20,646
nous voulons tout simplement trier nos pays

6
00:00:20,746 --> 00:00:22,664
par population décroissante.

7
00:00:23,382 --> 00:00:26,056
Nous allons pour cela avec Python

8
00:00:26,156 --> 00:00:28,381
utiliser la fonction prédéfinie sorted

9
00:00:29,166 --> 00:00:33,346
qui est une fonction qui prend en premier paramètre un itérable

10
00:00:33,446 --> 00:00:37,918
ici, l'itérable va être un itérateur sur les pays,

11
00:00:38,018 --> 00:00:40,448
nous parcourons notre tableau des pays

12
00:00:41,107 --> 00:00:42,952
et nous fournissons ces pays.

13
00:00:44,734 --> 00:00:49,080
Et nous allons lui passer en deuxième paramètre

14
00:00:49,953 --> 00:00:51,705
le paramètre nommé key

15
00:00:51,805 --> 00:00:55,206
qui est une fonction qui va être le critère sur lequel on va trier.

16
00:00:55,718 --> 00:00:58,212
Et enfin, dernier paramètre optionnel

17
00:00:58,312 --> 00:00:59,485
que nous allons passer ici,

18
00:00:59,585 --> 00:01:02,614
c'est le paramètre reverse

19
00:01:03,351 --> 00:01:04,609
qui par défaut est faux

20
00:01:04,709 --> 00:01:06,291
et que nous allons mettre à vrai

21
00:01:06,391 --> 00:01:08,658
pour obtenir un tri par ordre décroissant.

22
00:01:15,662 --> 00:01:20,355
Alors ici, l'itérateur est un peu artificiel,

23
00:01:20,455 --> 00:01:25,235
on aurait pu directement mettre notre tableau, notre liste L_PAYS,

24
00:01:25,787 --> 00:01:31,604
mais nous avons pris l'habitude de présenter,

25
00:01:31,704 --> 00:01:37,111
de mettre un itérateur, une fonction génératrice

26
00:01:37,211 --> 00:01:38,600
pour pouvoir justement

27
00:01:38,700 --> 00:01:43,408
fournir éventuellement une condition sur les pays

28
00:01:43,508 --> 00:01:48,433
et non pas faire le tri sur tous les pays de la table.

29
00:01:51,185 --> 00:01:55,424
Ici, la fonction de critère de tri,

30
00:01:55,524 --> 00:01:57,687
la fonction de tri, c'est la population

31
00:01:57,787 --> 00:02:00,486
qui est une fonction que nous avons écrite ici

32
00:02:00,586 --> 00:02:03,094
et qui consiste à retourner simplement

33
00:02:03,194 --> 00:02:05,535
la valeur du descripteur "pop"

34
00:02:05,635 --> 00:02:07,890
en l'ayant transformée en entier

35
00:02:07,990 --> 00:02:10,474
puisqu'il s'agit bien d'une donnée entière.

36
00:02:12,655 --> 00:02:17,143
Nous allons afficher les dix premières valeurs de ce tri.

37
00:02:18,827 --> 00:02:20,902
Notre tri a consisté à

38
00:02:21,002 --> 00:02:25,973
trier les enregistrements de notre table pays

39
00:02:26,073 --> 00:02:28,871
et à les mettre dans une autre table.

40
00:02:28,971 --> 00:02:33,554
pays_tries_par_pop_decroissante est une table

41
00:02:35,072 --> 00:02:38,985
des pays, mais triés comme nous le souhaitons.

42
00:02:43,040 --> 00:02:47,272
Évidemment, on peut avoir plusieurs critères pour le tri.

43
00:02:47,372 --> 00:02:49,964
Nous allons ici rajouter une fonction surface

44
00:02:50,064 --> 00:02:51,557
similaire à la fonction population

45
00:02:51,657 --> 00:02:55,373
mais cette fois, qui transforme en float

46
00:02:55,473 --> 00:02:58,109
la valeur associée au descripteur "aire".

47
00:02:58,671 --> 00:03:02,471
Et nous allons faire un tri par continent et par taille.

48
00:03:02,571 --> 00:03:09,240
Ici, l'itérable que nous passons à la fonction sorted

49
00:03:10,078 --> 00:03:12,283
est constitué des pays

50
00:03:12,383 --> 00:03:15,075
dont la surface est supérieure à 0.

51
00:03:15,175 --> 00:03:17,665
Nous voulons donc un tri

52
00:03:17,765 --> 00:03:20,446
par continent et par surface

53
00:03:20,546 --> 00:03:23,399
mais en éliminant les éventuels pays à surface nulle,

54
00:03:23,499 --> 00:03:24,404
il y en a quelques-uns.

55
00:03:27,585 --> 00:03:30,471
Ici, nous avons deux critères

56
00:03:30,837 --> 00:03:33,916
nous pouvons utiliser une fonction lambda

57
00:03:34,479 --> 00:03:39,566
qui, à un élément de notre itérable,

58
00:03:39,666 --> 00:03:41,247
donc ici, ce sont des pays,

59
00:03:41,347 --> 00:03:43,545
donc à un pays va associer le couple

60
00:03:44,778 --> 00:03:47,881
constitué de la chaîne de caractères du continent,

61
00:03:47,981 --> 00:03:50,865
de la valeur associée au descripteur "continent",

62
00:03:50,965 --> 00:03:52,958
et la surface.

63
00:03:53,236 --> 00:03:57,439
Le tri se fera d'abord sur le continent,

64
00:03:57,539 --> 00:04:01,333
et, pour des continents de même valeur,

65
00:04:01,433 --> 00:04:03,157
ensuite sur la surface.

66
00:04:08,841 --> 00:04:11,985
Donc ici, en affichant les dix premières valeurs,

67
00:04:13,442 --> 00:04:16,181
évidemment comme c'est d'abord trié par continent,

68
00:04:16,281 --> 00:04:18,391
nous n'avons que le continent africain

69
00:04:18,491 --> 00:04:23,271
et les dix premiers pays classés par ordre croissant sur la surface.

70
00:04:26,287 --> 00:04:27,566
Une deuxième façon de faire

71
00:04:27,666 --> 00:04:33,832
aurait été de construire une autre structure,

72
00:04:33,932 --> 00:04:35,957
donc ici, par exemple, on construit un dictionnaire

73
00:04:36,057 --> 00:04:39,744
qui a comme clé le code des continents

74
00:04:40,395 --> 00:04:42,917
puisque ce code est unique pour chaque continent

75
00:04:43,017 --> 00:04:43,793
donc il n'y a pas de problème,

76
00:04:44,427 --> 00:04:46,424
et lui associer une liste

77
00:04:46,524 --> 00:04:48,147
vide dans un premier temps.

78
00:04:49,719 --> 00:04:51,359
Et ensuite, dans un deuxième parcours,

79
00:04:51,648 --> 00:04:55,133
trier les pays

80
00:04:55,233 --> 00:04:56,783
par surface

81
00:04:57,603 --> 00:05:00,992
et les ranger dans la liste correspondant

82
00:05:01,092 --> 00:05:02,629
à chacun des continents.

83
00:05:02,729 --> 00:05:04,993
Encore une fois, en ne gardant que les pays

84
00:05:05,093 --> 00:05:06,042
à surface non nulle.

85
00:05:07,232 --> 00:05:09,926
C'est ce que fait cette petite boucle

86
00:05:10,607 --> 00:05:12,365
et nous pouvons maintenant afficher

87
00:05:15,127 --> 00:05:16,947
le résultat de ce dictionnaire

88
00:05:17,047 --> 00:05:18,158
en limitant à cinq

89
00:05:18,853 --> 00:05:21,853
les cinq premiers pays par continent.

90
00:05:22,444 --> 00:05:25,829
Donc ici, le premier continent, c'est l'Europe

91
00:05:25,929 --> 00:05:27,256
avec ses cinq pays,

92
00:05:28,727 --> 00:05:30,316
en commençant par le plus petit.

93
00:05:30,416 --> 00:05:32,082
Donc les cinq plus petits pays de l'Europe,

94
00:05:32,182 --> 00:05:37,119
les cinq plus petits pays de la zone Asie,

95
00:05:37,219 --> 00:05:37,857
et cætera.

96
00:05:42,409 --> 00:05:47,292
Donc voilà pour la manipulation consistant à trier nos tables.

97
00:05:47,392 --> 00:05:49,587
Encore une fois, on trie les données

98
00:05:49,687 --> 00:05:52,085
on les extrait en fait pour en faire une autre table.

99
00:05:52,599 --> 00:05:56,109
Nous allons nous retrouver dans une autre vidéo

100
00:05:56,209 --> 00:05:58,452
pour manipuler cette fois plusieurs tables

101
00:05:58,552 --> 00:05:59,552
et parler de fusion.

