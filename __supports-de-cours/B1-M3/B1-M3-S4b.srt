1
00:00:01,043 --> 00:00:04,545
Nous avons donc refait la fonction de lecture

2
00:00:04,645 --> 00:00:07,674
qui va permettre de lire les tables,

3
00:00:07,774 --> 00:00:09,060
les stocker dans des listes

4
00:00:09,160 --> 00:00:10,810
et ici, nous avons un petit peu modifié

5
00:00:10,910 --> 00:00:12,873
par rapport aux versions précédentes

6
00:00:12,973 --> 00:00:17,343
en rajoutant, en retour de fonction

7
00:00:17,618 --> 00:00:21,913
la liste des descripteurs, la liste des clés des dictionnaires.

8
00:00:22,912 --> 00:00:28,582
Ici, nous allons donc lire le carnet de contacts de Pierre

9
00:00:29,507 --> 00:00:31,250
Dans la variable CLES_P,

10
00:00:31,350 --> 00:00:34,124
nous stockons donc les clés que nous retrouvons ici

11
00:00:34,224 --> 00:00:36,126
"nom", "prenom", "tel" et "email".

12
00:00:36,792 --> 00:00:38,824
Et, dans la variable que j'ai appelée LP,

13
00:00:38,924 --> 00:00:40,828
L pour rappeler que c'est une liste,

14
00:00:41,188 --> 00:00:44,336
nous avons donc le carnet de contacts de Pierre.

15
00:00:47,508 --> 00:00:50,188
Nous allons vérifier le contenu

16
00:00:50,288 --> 00:00:51,539
puisqu'il n'est pas grand.

17
00:00:51,639 --> 00:00:55,583
Donc dans LP, nous avons bien la liste des enregistrements

18
00:00:55,683 --> 00:00:58,532
des dictionnaires qui correspondent aux enregistrements

19
00:00:58,632 --> 00:00:59,829
des contacts de Pierre.

20
00:01:00,327 --> 00:01:03,501
Nous faisons la même chose avec les contacts de Marie,

21
00:01:05,161 --> 00:01:08,202
et pareil, nous pouvons vérifier

22
00:01:08,812 --> 00:01:14,728
que nous avons bien la liste de ses contacts.

23
00:01:17,481 --> 00:01:18,998
Maintenant pour la fusion,

24
00:01:19,098 --> 00:01:20,403
donc ce qu'on voudrait faire,

25
00:01:20,855 --> 00:01:24,716
on pourrait faire en première solution,

26
00:01:24,816 --> 00:01:27,118
ce serait simplement concaténer

27
00:01:27,218 --> 00:01:30,144
au sens de Python, au sens des séquences de Python,

28
00:01:30,244 --> 00:01:32,903
concaténer les deux listes LP et LM.

29
00:01:33,826 --> 00:01:35,571
Dans un second temps,

30
00:01:35,671 --> 00:01:37,092
faire la chasse aux doublons

31
00:01:37,192 --> 00:01:38,449
et supprimer les doublons

32
00:01:38,549 --> 00:01:42,906
éventuellement en fusionnant les données.

33
00:01:43,006 --> 00:01:43,895
Par exemple ici,

34
00:01:43,995 --> 00:01:47,690
si on fusionne et si on regarde l'enregistrement

35
00:01:47,790 --> 00:01:49,345
correspondant à Derand Michel,

36
00:01:49,445 --> 00:01:50,970
on se rend compte que

37
00:01:51,668 --> 00:01:54,996
si on a l'information de téléphone sur le carnet de Pierre,

38
00:01:55,096 --> 00:01:58,314
on ne l'a pas sur le carnet de Marie.

39
00:01:58,414 --> 00:02:00,622
Donc en fusionnant et en supprimant les doublons,

40
00:02:00,722 --> 00:02:02,248
il va falloir évidemment garder

41
00:02:03,761 --> 00:02:05,521
l'endroit où on a l'information

42
00:02:05,621 --> 00:02:08,631
et non cette information vide.

43
00:02:10,164 --> 00:02:12,059
Donc ça, c'est une première solution,

44
00:02:12,159 --> 00:02:15,425
ça oblige à faire la chasse aux doublons.

45
00:02:16,654 --> 00:02:18,518
Une deuxième possibilité

46
00:02:20,202 --> 00:02:24,135
serait d'avoir non pas une liste

47
00:02:24,235 --> 00:02:28,881
comme structure pour un des deux contacts

48
00:02:28,981 --> 00:02:30,288
une des deux listes,

49
00:02:30,388 --> 00:02:32,324
au lieu d'avoir une liste, d'avoir un dictionnaire.

50
00:02:33,120 --> 00:02:35,998
Et donc, c'est ce que nous allons faire.

51
00:02:36,098 --> 00:02:37,549
Ici, une petite fonction

52
00:02:37,649 --> 00:02:40,920
qui prend une table sous forme de liste

53
00:02:41,020 --> 00:02:43,654
et qui va construire un dictionnaire

54
00:02:44,192 --> 00:02:46,384
une copie donc des entrées

55
00:02:46,484 --> 00:02:48,122
en ayant un dictionnaire.

56
00:02:48,222 --> 00:02:50,661
Ici par exemple, nom et prénom peuvent servir de clés

57
00:02:50,761 --> 00:02:55,478
puisqu'on a dit qu'on prenait comme simplification

58
00:02:55,578 --> 00:02:57,462
que le nom et le prénom étaient discriminants,

59
00:02:57,562 --> 00:02:58,554
qu'il n'y avait pas d'homonymes,

60
00:02:58,654 --> 00:03:01,086
il n'y a pas deux Michel Derand dans les tables,

61
00:03:01,896 --> 00:03:04,201
et donc, on va pouvoir créer un dictionnaire

62
00:03:04,301 --> 00:03:06,563
où le couple nom, prenom

63
00:03:06,663 --> 00:03:08,390
constitue la clé du dictionnaire

64
00:03:08,490 --> 00:03:11,779
et la valeur associée est le dictionnaire

65
00:03:11,879 --> 00:03:13,437
de l'enregistrement.

66
00:03:13,968 --> 00:03:15,739
Une copie plus exactement

67
00:03:15,839 --> 00:03:21,671
pour ne pas avoir d'éventuels conflits,

68
00:03:21,771 --> 00:03:24,427
en tout cas, de ne pas manipuler le même objet.

69
00:03:24,969 --> 00:03:27,716
Ici, on utilise la fonction copy simple

70
00:03:27,816 --> 00:03:31,259
puisque, à l'intérieur de ce dictionnaire,

71
00:03:31,359 --> 00:03:35,817
les valeurs ne sont pas des objets mutables.

72
00:03:36,912 --> 00:03:38,832
Évidemment, si on avait des objets mutables,

73
00:03:38,932 --> 00:03:41,018
donc une structure plus complexe,

74
00:03:41,118 --> 00:03:43,249
il faudrait utiliser deepcopy

75
00:03:43,349 --> 00:03:45,559
du module copy

76
00:03:45,659 --> 00:03:48,735
qui est évidemment une fonction plus gourmande

77
00:03:48,835 --> 00:03:52,235
puisqu'elle va aller faire la copie de façon récursive

78
00:03:52,335 --> 00:03:53,918
sur les valeurs.

79
00:03:54,850 --> 00:03:57,482
Mettons en place cette fonction

80
00:03:57,582 --> 00:03:59,908
et nous allons donc construire ici

81
00:04:00,008 --> 00:04:05,932
à partir de la liste de Pierre

82
00:04:06,817 --> 00:04:09,761
nous allons construire un dictionnaire commun.

83
00:04:09,861 --> 00:04:12,022
Alors dans un premier temps, ce dictionnaire commun

84
00:04:12,122 --> 00:04:16,286
sera simplement une copie

85
00:04:16,386 --> 00:04:18,551
de la liste des contacts de Pierre,

86
00:04:18,651 --> 00:04:19,517
on le voit ici, 

87
00:04:19,617 --> 00:04:20,995
nous avons un dictionnaire

88
00:04:21,095 --> 00:04:24,769
la clé, c'est un couple nom/prénom

89
00:04:24,869 --> 00:04:26,117
donc ici, "Derand", "Michel"

90
00:04:26,217 --> 00:04:28,007
et la valeur associée, le dictionnaire

91
00:04:28,107 --> 00:04:30,916
qui était déjà présent dans la liste.

92
00:04:34,708 --> 00:04:42,462
Et donc maintenant, à partir de ce dictionnaire,

93
00:04:42,989 --> 00:04:45,195
il y a une petite fonction

94
00:04:45,295 --> 00:04:47,043
qui prend donc un dictionnaire

95
00:04:47,853 --> 00:04:49,785
qui est censé être le dictionnaire commun

96
00:04:49,885 --> 00:04:52,624
et c'est ce dictionnaire que nous allons venir compléter

97
00:04:52,724 --> 00:04:55,334
des informations de la deuxième table.

98
00:04:55,434 --> 00:04:57,493
Le premier argument de notre fonction, 

99
00:04:57,593 --> 00:04:58,856
c'est le dictionnaire qui va être modifié,

100
00:04:59,599 --> 00:05:01,109
enrichi donc,

101
00:05:01,209 --> 00:05:03,152
et le deuxième argument,

102
00:05:03,252 --> 00:05:05,950
c'est la liste des enregistrements

103
00:05:06,050 --> 00:05:08,628
qui vont venir nous aider à enrichir le dictionnaire.

104
00:05:08,728 --> 00:05:11,325
Il nous faut également, nous le verrons,

105
00:05:11,425 --> 00:05:13,601
la liste des descripteurs.

106
00:05:14,871 --> 00:05:17,540
L'idée, c'est de parcourir simplement

107
00:05:18,363 --> 00:05:20,749
la liste qui est en deuxième argument,

108
00:05:20,849 --> 00:05:24,252
de récupérer ce qui constitue pour nous une clé,

109
00:05:24,352 --> 00:05:26,312
c'est-à-dire un nom et un prénom,

110
00:05:26,412 --> 00:05:27,552
et de tester.

111
00:05:27,652 --> 00:05:31,604
Si ce nom et ce prénom apparaissent dans le dictionnaire

112
00:05:31,704 --> 00:05:35,156
c'est qu'il faut mettre à jour éventuellement ses champs.

113
00:05:35,967 --> 00:05:40,514
Avec une simple boucle for, nous parcourons les descripteurs

114
00:05:40,614 --> 00:05:41,546
c'est pour cela qu'ils sont là,

115
00:05:45,337 --> 00:05:51,917
et nous testons, avant de mettre à jour,

116
00:05:52,017 --> 00:05:55,837
nous regardons si la valeur associée est la chaîne vide,

117
00:05:55,937 --> 00:05:57,527
nous mettons à jour,

118
00:05:57,627 --> 00:05:59,701
sinon, nous ne mettons pas à jour et nous gardons

119
00:05:59,801 --> 00:06:02,655
nous supposons que l'information du carnet de Pierre

120
00:06:02,755 --> 00:06:04,468
est à jour et est la bonne information.

121
00:06:04,568 --> 00:06:06,355
Encore une fois, c'est un choix,

122
00:06:06,455 --> 00:06:07,635
c'est une solution possible,

123
00:06:08,724 --> 00:06:11,696
tout va dépendre évidemment des données

124
00:06:11,796 --> 00:06:12,938
de ce qu'il en est.

125
00:06:14,323 --> 00:06:19,130
Et si cette clé nom, prenom n'apparaît pas déjà dans le carnet de Pierre,

126
00:06:19,230 --> 00:06:20,974
nous créons une entrée

127
00:06:21,594 --> 00:06:23,482
avec comme valeur associée

128
00:06:23,582 --> 00:06:25,647
la copie de l'entrée qui, je vous le rappelle,

129
00:06:25,747 --> 00:06:29,883
fait partie du carnet du deuxième argument

130
00:06:29,983 --> 00:06:31,439
et donc du carnet de Marie.

131
00:06:36,054 --> 00:06:38,736
Et donc, ce que nous voulons faire, c'est fusionner

132
00:06:39,538 --> 00:06:42,612
ajouter au dictionnaire commun

133
00:06:42,712 --> 00:06:46,632
les entrées qui font partie de la liste de Marie,

134
00:06:46,732 --> 00:06:48,055
c'est-à-dire les contacts de Marie.

