1
00:00:06,930 --> 00:00:08,155
Nous avons vu la partie

2
00:00:08,255 --> 00:00:09,823
langage de programmation Python,

3
00:00:09,923 --> 00:00:11,696
normalement suffisante pour réaliser

4
00:00:11,796 --> 00:00:13,565
les codes demandés dans le cours NSI.

5
00:00:13,665 --> 00:00:15,654
Prenons maintenant un peu de recul

6
00:00:15,754 --> 00:00:17,585
dans l'étude des langages de programmation.

7
00:00:18,020 --> 00:00:19,286
Comment sont-ils définis ?

8
00:00:19,818 --> 00:00:21,253
Quels sont les éléments communs

9
00:00:21,353 --> 00:00:22,397
à différents langages

10
00:00:22,497 --> 00:00:23,448
et propres à chacun ?

11
00:00:23,548 --> 00:00:25,149
Quels sont les mécanismes utilisés

12
00:00:25,249 --> 00:00:26,548
en programmation impérative

13
00:00:26,648 --> 00:00:27,749
et orientée objet ?

14
00:00:27,873 --> 00:00:29,301
Quels sont les paradigmes

15
00:00:29,401 --> 00:00:31,571
sur lesquels repose un langage de programmation.

16
00:00:32,486 --> 00:00:34,652
Comme une telle étude est titanesque,

17
00:00:34,752 --> 00:00:35,911
nous ne donnerons ici

18
00:00:36,011 --> 00:00:37,234
qu'un résumé des points

19
00:00:37,334 --> 00:00:38,425
qui nous semblent importants.

20
00:00:38,525 --> 00:00:40,658
Commençons, dans la vidéo suivante,

21
00:00:40,758 --> 00:00:42,645
par expliquer ce que l'on entend

22
00:00:42,745 --> 00:00:44,468
par paradigme de programmation.

23
00:00:44,765 --> 00:00:47,342
Notons que les vidéos que nous vous présentons

24
00:00:47,922 --> 00:00:51,120
sont un résumé et morceaux choisis du support écrit

25
00:00:51,220 --> 00:00:53,059
qui lui-même est en grande partie

26
00:00:53,159 --> 00:00:55,389
un résumé du cours Langages de programmation

27
00:00:55,489 --> 00:00:57,037
volumes 1 et 2

28
00:00:57,137 --> 00:00:58,493
du professeur Yves Roggeman,

29
00:00:58,950 --> 00:01:00,961
donné en référence dans les notes.

30
00:01:01,445 --> 00:01:02,445


