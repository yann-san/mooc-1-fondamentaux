# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 2.2.1 : Paradigmes des langages de programmation



Vu le grand nombre de langages existants, une classification s’est fait jour.

On regroupe en général les langages en « familles ».

Divers classifications peuvent être proposés.

- Selon le paradigme de programmation auquel ils sont le mieux adaptés.  Nous verrons un peu plus loin ce qu'est un paradigme.

- Selon leur généalogie
.  En effet, la majorité des langages de programmation  ont été créés comme évolution ou variante d’un ou plusieurs langages qui leur préexistaient.

- ou encore selon différents critères dont le plus utilisés est le fait que le langage est compilé (comme C, C++, Fortran, Cobol, Java, OCaml) ou interprété (comme Python, Basic, Ruby, PHP, Javascript).

Nous parlerons de compilateur et d'interpréteur et de leur fonctionnement plus loin.

Commençons par brièvement voir la notion de paradigme de programmation, qui est un concept assez difficile à définir précisément.

Wikipedia nous dit que c'est une façon d'approcher la programmation informatique et de traiter les solutions aux problèmes et leur formulation dans un langage de programmation approprié.

C'est assez flou comme définition.

On parle donc de façon de formuler la solution à un problème, par opposition à la méthode pour le résoudre.

La notion de paradigme devient généralement plus claire grâce aux instances de langages de programmation qui l'utilisent.

Deux grandes familles de langages de programmation occupent une place prépondérante dans ce paysage :

les langages impératifs, dont sont issus les langages orientés-objets, même s'ils sont étudiés de façon spécifique,

et les langages déclaratifs dont font partie les langages fonctionnels que nous aborderons brièvement.

Dans un premier temps, nous regarderons les langages impératifs qui définissent le comportement de base de beaucoup de langages comme  Python, C, C++, Fortran, Cobol, Java, ...,

même si certains d'entres-eux incluent dans leur conception, le paradigme orienté-objet

C'est le cas de Python, C++ et Java par exemple.

Pour ces langages, il est généralement admis que l'on travail sur des modèles d'ordinateurs qui suivent une [architecture de von Neumann](https://fr.wikipedia.org/wiki/Architecture_de_von_Neumann).

En bref, l’architecture de von Neumann décompose l’ordinateur en 4 parties distinctes :

- l’unité arithmétique et logique ou unité de traitement : son rôle est d’effectuer les opérations de base ;

- l’unité de contrôle, chargée du « séquençage » des opérations ;

- la mémoire qui contient à la fois les données et le programme qui indiquera à l’unité de contrôle quels sont les calculs à faire sur ces données ;

- et les dispositifs d’entrée-sortie, qui permettent de communiquer avec le monde extérieur.

Avant de parler plus des langages impératifs, nous allons d'abord  brièvement nous arrêter sur la notion de compilateur et d'interpréteur.
