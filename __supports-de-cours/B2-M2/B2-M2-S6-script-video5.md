# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 2.2.6.5 : Autres paradigmes de programmation



Il existe de nombreux autres langages qui n’entrent pas parfaitement dans l’une des catégories « impératif »,  « orienté-objet » ou « déclaratif ».

L'entrée Paradigme de programmation, section Autres_types
de wikipedia donne des dizaines de paradigmes différents.

Citons en particulier

- la programmation événementielle où un programme sera principalement défini par ses réactions aux différents événements qui peuvent se produire (Par exemple, TcL/Tk gère un tel paradigme)

- la programmation concurrente où plusieurs processus (appellés threads, ou tâches) s'éxécutent en concurrence, donnant lieu à de nombreux problèmes informatiques complexes dont les célèbres problèmes d'interblocage et de famines

De nombreux langages dédiés (domain-specific languages en anglais) sont conçus pour répondre aux contraintes d’un domaine d'application précis.

On peut citer, comme simple exemple parmi beaucoup d’autres :

- Les langages de requêtes comme SQL

- Les langages de script comme Perl, Raku, Ruby, Lua, Tcl/Tk

- Les langages d’analyse statistique comme R

- Les langages de calcul numérique comme MATLAB ou Mathematica

- Les langages de calcul formel comme Maple, Maxima, Reduce, Derive

- etcetara

Notons enfin que souvent les langages sont multi-paradigmes:

ainsi Python est un langage impératif, orienté-objet et permet une certaine programmation fonctionnelle.

Si l'on regarde l'ébauche d'article : Comparaison_des_langages_de_programmation_multi-paradigmes sur wikipedia,

10 paradigmes sont cités pour Python

Le langage Julia en a plus de 17 !



