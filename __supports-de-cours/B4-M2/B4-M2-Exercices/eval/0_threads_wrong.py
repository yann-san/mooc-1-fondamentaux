#!/usr/bin/env python3
import threading

class myThread (threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        
    def run(self):
        print("Starting " + self.name)
        print("Exiting " + self.name)


def main():
    print("Starting the main thread!!!")
    
    # Create a new thread
    thread = myThread()
    
    # start the thread
    thread.run()

    print("Exiting the main thread!!!")

main()
