# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 4.3.2.2 : Le chiffrement part 1

**[00:00:01]**

Nous allons voir dans cette vidéo la notion de chiffrement et les principes utilisés pour assurer la sécurité des réseaux. 
Alors, la sécurité est une fonction incontournable des réseaux. 
Puisqu'on ne voit pas son correspondant directement, il faut l’authentifier. Puisqu'on ne sait pas par où passent les données, il faut les chiffrer. 
Puisqu'on ne sait pas si quelqu'un ne va pas modifier les informations transmises, il faut vérifier leur intégrité. 
Nous pourrions ajouter une longue suite de requêtes du même genre qui doivent être prises en charge par les réseaux. 

Ainsi, généralement, les services de sécurité qui sont à assurer dans une communication en réseau sont : 
La confidentialité, qui désigne la capacité à garder une information secrète. Les paquets sont chiffrés de sorte que, même s'ils sont interceptés, il ne dévoile pas les informations transportées.
L'authentification, qui vise à assurer que ne se connecte que des clients autorisés, cela se fait le plus souvent à l'aide d'un login et d'un mot de passe. Pour assurer un minimum de sécurité, le mot de passe doit circuler de manière chiffrée sur le réseau. 
L'intégrité des données, qui vise à vérifier que ne passent par la connexion que des paquets non modifiés envoyés par le client connecté.
Et enfin, la non-répudiation qui caractérise le fait qu'un échange passé ne peut être nié. Le destinataire ne doit pouvoir contester la réception du message. Et de même l'émetteur, l'émission du message. 

Le chiffrement est un mécanisme utilisé pour répondre au besoin de confidentialité et d'intégrité présenté ci-dessus. Il se fait en utilisant des fonctions appelées algorithmes de chiffrement, qui permettent de transformer un message écrit en clair et lisible en un message chiffré, donc illisible, appelé cryptogramme.

**[00:02:07]**

Cette transformation se fonde sur une ou plusieurs clés. Ainsi, lorsqu'on désire transmettre un document confidentiel à travers le réseau, on chiffre le document sur son poste de travail avec une clé de chiffrement et on envoie la version chiffrée du document. Le destinataire déchiffre le document sur son poste de travail avec la clé de déchiffrement. Si une troisième personne intercepte le message durant le transfert, elle ne pourra pas le déchiffrer, car elle ne connaîtra pas la valeur de la clé de déchiffrement. Cela assure la confidentialité du message envoyé. De même, si le message est impossible à déchiffrer par le destinataire, cela prouve qu'il a été modifié durant son transfert. Le chiffrement assure donc aussi la vérification d'intégrité. 
Il faut noter que les algorithmes de chiffrement sont publics et ont fait l'objet de standardisation. C'est par conséquent le secret des clés utilisées qui permet à ces algorithmes d'assurer le service de confidentialité.
Généralement, plus la clé de chiffrement est longue, plus elle est difficile à déchiffrer. Par contre, elle introduit également une latence lors du chiffrement. Il y a deux grandes familles d'algorithmes de chiffrement les algorithmes symétriques et les algorithmes asymétriques.


Le chiffrement symétrique. C'est le cas le plus simple dans lequel on a une clé unique et secrète que seuls l'émetteur et le récepteur connaissent. Comme le montre la figure à droite de l'écran, les systèmes à clé secrète sont caractérisés par une transformation F et une transformation inverse F-1 qui s'effectuent à l'aide de la même clé. C'est la raison pour laquelle on appelle ce système “à chiffrement symétrique”. L'avantage de ce procédé est que les opérations de chiffrement et déchiffrement sont rapides à exécuter sur des ordinateurs classiques.

**[00:04:08]**

Par contre, le problème est la gestion des clés. En effet, chaque clé qu'on utilise avec un correspondant doit rester unique et secrète. On a donc autant de clés que de correspondants et il faut trouver un moyen d'échanger chaque clé secrète avec chaque correspondant de manière sûre. Un exemple est le DES pour Data Encryption Standard de 1974, qui est l'algorithme symétrique historiquement le plus connu. 

L'autre ensemble d'algorithmes sont les algorithmes asymétriques ou algorithmes à clé publique conçus pour utiliser des clés possédant les propriétés suivantes. 
La clé de chiffrement est différente de la clé de déchiffrement, d'où le terme asymétrique. 
Les deux clés, une pour chiffrer et l'autre pour déchiffrer, sont créées ensemble avec une fonction mathématique et forment un couple. L'une ne va pas sans l'autre. 
Il est impossible avec l'une des deux clés de découvrir l'autre et tout texte chiffré avec l'une des deux clés peut être déchiffré avec l'autre et uniquement avec celle-ci. 

En pratique, pour utiliser ces algorithmes, il faut générer un couple de clés, l'une pour chiffrer et l'autre pour déchiffrer pour chaque utilisateur. L'utilisateur le fera lui-même ou quelqu'un de confiance le fera pour lui. Il gardera sa clé de déchiffrement secrète, que l'on appelle clé privée. À l'inverse, il rendra sa clé de chiffrement publique que l'on appelle clé publique et la diffusera le plus largement possible. On la trouvera dans des annuaires électroniques, par exemple. Ainsi, le couple de clés est formé d'une clé privée secrète pour déchiffrer et une clé publique pour chiffrer. Ce découpage entre clé publique et clé privée est très utile pour une utilisation planétaire du chiffrement. Alors que les algorithmes symétriques obligent à échanger un secret différent avec chaque interlocuteur, la clé secrète, il suffit maintenant d'avoir un annuaire qui permet de retrouver la clé publique de chaque internaute.

**[00:06:10]**

Et ce système peut fonctionner entre tous les internautes. Quand un utilisateur voudra envoyer un message chiffré à un correspondant, il consultera l'annuaire qui lui indiquera la clé publique de son correspondant. Avec cette clé, il chiffrera le message et celui-ci ne pourra être déchiffré qu'avec la clé privée du correspondant et donc que par le correspondant. Ainsi, les propriétés des algorithmes asymétriques vont permettre de s'affranchir du problème de la gestion des clés et ainsi d'envisager de déployer l'utilisation du chiffrement à très grande échelle. Mais il reste un problème avec les algorithmes asymétriques. Le temps pour les opérations de chiffrement et de déchiffrement est long. La clé de session est un moyen pour atténuer ces mauvaises performances. Un exemple est RSA pour Rivest, Shamir et Adleman, conçu en 1978. C'est l'algorithme de chiffrement asymétrique le plus célèbre et le plus répandu. 
Nous avons tout à l'heure évoqué la lenteur des algorithmes asymétriques comme une limite de ceux-ci, et bien à titre comparatif, voici les performances d'un algorithme asymétrique et RSA et d'un algorithme symétrique DES. En logiciel, DES est 100 fois plus rapide que le chiffrement RSA. Sur une implantation matérielle, il est entre 1000 et 10.000 fois plus rapide. On a donc un débit de chiffrement entre 300 mégabits par seconde et 3 gigabits par seconde comparé à 300 kbit par seconde pour RSA. 


