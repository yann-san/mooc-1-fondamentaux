# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 4.3.6.1 : Service de Nom de Domaine (DNS)

**[00:00:01]**

Bienvenue dans ce chapitre 6 traitant des services applicatifs au sein d'un réseau. Dans ce chapitre, nous nous attarderons sur quelques services associés à la couche 7 du modèle OSI et analyserons leur fonctionnement afin d'en comprendre les rôles et les protocoles qui leur sont associés. Pour rappel, la couche 7 contient les protocoles de plus haut niveau lors de l'accès au réseau. C'est à ce niveau que les données utilisateur sont contenues et transmises. Dans ce chapitre, nous commencerons par un protocole d'exploitation qu'est le DNS, puis nous verrons un protocole orienté vers l'envoi de pages Internet HTTP. Un protocole pour l'envoi de mail, le SMTP et enfin FTP pour l'envoi de fichiers. Nous verrons dans la dernière partie le protocole NTP pour la synchronisation d'horloge. Commençons tout de suite par le protocole DNS. 

Pour accéder à un site Internet, il faut évidemment contacter le serveur sur lequel sont hébergées les données de ce site. Comme tout nœud du réseau, un serveur Web s'identifie et se contacte à l'aide de son adresse IP. Elle peut être de quatrième génération IPV4 ou de sixième génération IPV6. Comme nous l'avons vu précédemment. 
Prenons l'exemple d'un utilisateur désirant aller sur le site fr.wikipédia.org. Le serveur de Wikipédia fait bien partie des serveurs accessibles représentés en orange ici. Mais pour le contacter, il faut connaître son adresse IP.

**[00:01:18]**

Or, une adresse IP, qu'elle soit version 4 ou version 6, n'est pas pratique d'utilisation pour un humain. On se voit mal demander à quiconque de retenir l'adresse IP du site Wikipédia, que ce soit en version 6 ou version 4. Et même s'il en connaissait l'adresse IP, celle-ci pourrait éventuellement changer plus tard. Et il faudrait alors en retenir une nouvelle. On préfère donc largement utiliser et retenir la version texte et pérenne de cette adresse, par exemple : fr.wikipedia.org. 
C'est celle-ci que l'utilisateur tapera dans la barre du navigateur et l'ordinateur se débrouillera pour déterminer l'adresse IP du serveur en question. Et c'est là qu'intervient le protocole DNS. L'ordinateur interroge le service DNS sur l'adresse IP correspondant à un nom de domaine. Le serveur DNS la lui fournit. On obtient alors l'adresse IP du serveur de Wikipédia, que l'on est donc capable de contacter. 
Le protocole DNS permet d'obtenir l'adresse IP d'un serveur à partir de son nom de domaine. On appelle cela résoudre un nom de domaine. La question que l'on se pose ici est donc comment résout-on un nom de domaine? Pour bien comprendre, nous allons d'abord analyser sa structure. Le DNS fonctionne avec une structure hiérarchique. Le point le plus haut dans cette hiérarchie est appelé racine et est souvent représentée par un point, comme sur cette image sous la racine.

**[00:02:34]**

On trouve des domaines de premier niveau TLD pour Top Layer Domaine. Ces domaines correspondent à la dernière partie de l'adresse. Il en existe aujourd'hui de nombreux comme .org, .com, .fr ou même .paris, par exemple. 
On trouve ensuite des domaines de second niveau. Par exemple, Wikipédia, Campus France et chacun de ses domaines va être divisé en des sous domaines, par exemple impots.gouv.fr ou fr.wikipedia.org dans notre exemple. Cette adresse peut être découpée en trois parties hiérarchisées. org pour le domaine de premier niveau, wikipedia pour le domaine de second niveau et enfin fr pour com sous domaines. L
a résolution d'une adresse se fait selon cette hiérarchie et on l'appelle une résolution récursive. 
Un hôte souhaitant résoudre récursivement l'adresse fr.wikipedia.org interrogera d'abord le serveur racine sur le domaine point .org, ce serveur lui enverra alors une liste de serveurs pouvant lui répondre pour cette zone. On interroge l'un de ces serveurs TLD org qui nous répondra pour le serveur de second niveau Wikipédia. Enfin, ce dernier serveur nous répondra avec l'adresse IP du serveur hébergeant la page fr.wikipedia.org. On pourra alors contacter ce serveur puisqu'on connectera son adresse IP.

**[00:03:53]**

En pratique, cette résolution récursive est réalisée par un serveur DNS hébergé par exemple par les fournisseurs d'accès Internet FAI. 
D’ailleurs si l'on observe la configuration d'un PC personnel, le serveur DNS et souvent la box du domicile lors de l'ouverture d'une page Web. Le PC interrogera la box, qui interrogera alors le DNS du FAI, qui fera alors au besoin, une résolution récursive pour éviter un nombre trop important de requêtes et afin d'accélérer le processus. Les serveurs DNS intermédiaires, la box et le serveur de résolution récursive sauvegardent en cache une association noms de domaine - adresses IP durant une durée appelée Time to live et propre à chaque adresse. On dit que ces réponses ne font pas autorité, car elles n'ont pas été récupérées directement à partir des serveurs racines. 

Pour résumer, le DNS Domain Name Service a pour objectif d'obtenir l'adresse IP d'un serveur à partir de son nom de domaine. Il a une structure hiérarchique de la racine en passant par des domaines de premier niveau, de second niveau, puis de sous niveaux, et la résolution se fait de manière hiérarchique. Chaque serveur DNS peut mettre en cache certaines données qu'il a obtenues précédemment. Enfin, le protocole de transport le plus souvent utilisé pour le serveur DNS et le protocole UDP.

