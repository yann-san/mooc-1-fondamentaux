#4.3 Réseaux - Chap5 - Transport

## 4.3.5  vidéo 1 : Présentation de la couche transport 

Intro du chapitre : Dans les chapitres précédents, nous avons vu comment le réseau Internet était organisé et comment il permettait l'acheminement d'un paquet IP de sa source à sa destination. Des paquets sont parfois perdus, les paquets correspondant à un même message peuvent arriver dans le désordre et les machines étant multitâche, il est nécessaire d'indiquer de séparer les flux liés à chaque service. C'est pourquoi les communications sur Internet utilisent une couche transport.


**diapo 3** Le rôle de la couche transport

Revenons sur le modèle en couche de la pile TCP/IP, inspiré du modèle OSI.
La couche transport fournit des services pour la couche application.
Elle utilise les services de la couche réseau.

La couche réseau est unique sur Internet : IP. Elle est sans connexion. 

Les paquets IP sont nous l'avons vu acheminés par les routeurs de l'Internet. Des paquets sont perdus, certains arrivent dans un ordre dispersé. 
La couche transport va permettre notamment, si besoin, de réemmettre les paquets perdus et de réordonner les paquets reçus. La couche réseau ne faisant pas de contrôle d'erreur, la couche transport va ajouter un calcul d'erreur de bout-en-bout, ce que ne font pas les différentes couches physiques traversées. De plus, sur une machine ayant plusieurs applications s'exécutant en même temps, la couche transport va permettre la délivrance des messages au service concerné.

2 protocoles de transport sont possibles : un protocole sans connexion UDP et un protocole avec connexion, TCP. 

Notons que la couche transport est le niveau utilisé généralement par les programmeurs. De nombreux langages (C/C++, python, java notamment) proposent une bibliothèque nommée socket utilisée pour émettre et recevoir sur Internet. Cette bibliothèque fonctionne au niveau transport.


[//]: # (primitives très simples : voir en python ?
socket
bind
listen
accept
connect
send
receive
close)


**diapo 4** Les ports

Lorsque un patient rentre au centre municipal de santé, suivant le besoin exprimé, il est dirigé par l'accueil vers la porte du service d'ophtalmologie, de radiologie ou d'infirmerie.

De même un serveur physique ayant une adresse IP unique peut proposer plusieurs services (serveur web et stockage de fichiers par exemple). Il est donc nécessaire d'indiquer dans chaque paquet IP à quel service il est destiné. C'est le rôle du port serveur, champ de l'en-tête des protocoles de transport. (__*CLIC*__) Ici, sur la machine de Free d'adresse 212.27.63.3 le port 80 est le port du serveur web et le port 21 celui du serveur FTP de stockage de fichier.

De même au niveau du client, plusieurs applications peuvent être exécutées en parallèle. Il est donc nécessaire d'indiquer quelle application est destinataire des paquets reçus. C'est le rôle du port client. (__*CLIC*__) Ici, sur le PC, le port 16348 désigne le navigateur web et le port 16391 désigne le client FTP du service de stockage de fichier.

(__*CLIC*__) Les extraits wireshark ci-dessous ont été acquis lors d'une communication entre un PC et le serveur de Free, pour en même temps afficher une page web et télécharger un fichier. Les ports serveurs et clients entourés en rouge concerne les échanges entre le navigateur et le serveur web et les ports entourés en vert concernent les échanges entre le client et le serveur FTP. Le port permet ainsi à chaque message d'être acheminé de l'interface réseau au service ou à l'application concernés.

**diapo 5** Les ports 2

Pour atteindre un service proposé par un serveur il est donc nécessaire de connaître l'adresse IP du serveur mais aussi le numéro du port du service. Pour cela, les services courants ont des ports réservés entre 0 et 1023 et d'autres services ont des ports usuels, entre 1024 et 49152.

Dans cet extrait de la liste officielle de l'IANA, on peut noter :

* (__*CLIC*__) les ports 20 et 21 pour le service de stockage de fichier FTP, 
* (__*CLIC*__) les ports 25 et 220 pour les serveurs de mail
* (__*CLIC*__) les port 80 et 443 pour les serveurs web
* (__*CLIC*__) au-delà de 1023, les ports sont attribués à des services divers, par exemple 1194 pour les connexions sécurisées OpenVPN, 3306 pour les requêtes de base de données mySQL et 47808 pour les échanges BACnet entre équipements de gestion technique de bâtiments.

Les ports clients sont choisis successivement au-delà de 49152 de préférence mais aussi dans la plage 1024-49152.

La commande netstat permet de connaître les ports ouverts sur une machine. (__*CLIC*__) A l'instant d'écriture de ce cours, cela permet de voir que 3 connexions HTTPs entre mon navigateur firefox et des serveurs google sont actives alors qu'aucune recherche n'est en cours et qu'une connexion, aussi HTTPs est fermée avec un serveur Github.









