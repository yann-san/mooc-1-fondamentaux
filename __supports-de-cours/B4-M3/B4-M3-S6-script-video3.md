# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 4.3.6.3 : NTP

**[00:00:01]**

Dans cette dernière vidéo du chapitre 6, nous allons parler du protocole NTP Network Time Protocol, le protocole de synchronisation d'horloge des appareils informatiques. Ce protocole est un des plus anciens encore en utilisation. Dans les années 1980, un besoin de synchronisation pour les protocoles de routage émerge et un premier protocole voit le jour en 1981. DCNET Internet Clock Service, suivi en 1985 par le NTP qui nous intéresse dans cette vidéo. 

Dans une architecture informatique, il est important d'avoir des horloges synchronisées. Sans parler du confort utilisateur qui désirent connaître l'heure, prenons l'exemple de la gestion d'un parc informatique. Les évènements importants se produisant sur un appareil du réseau ou sur le réseau en général sont enregistrés dans des fichiers log. Si une panne se produit à un instant donné, on cherchera dans ses fichiers les événements qui se sont produits à ce moment-là et il est donc important que tous les appareils partagent la même heure. Interpréter et corréler des données d'appareils différents devient un véritable casse-tête. Si les horloges ne sont pas précisément synchrones, peut être plus important encore, beaucoup de protocoles sécurisés utilisent des certificats qui ont une date et une heure d'expiration. Il est alors capital pour un ordinateur de savoir si le certificat qu'il reçoit est toujours valable au moment où il l'utilise. 

Mais alors, comment fonctionne ce protocole NTP? C'est le sujet de cette vidéo. 

Pour synchroniser les horloges, il faut tout d'abord une référence. Cette référence est donnée par une horloge de haute précision, comme par exemple une horloge atomique.

**[00:01:25]**

À ces horloges de référence sont reliées à des ordinateurs qui se synchronisent souvent par des interruptions à ces horloges. Le protocole NTP définit une hiérarchie. On dit que les horloges de référence appartiennent à la couche 0, tandis que les serveurs qui sont reliés à ces horloges appartiennent à la couche 1. Les appareils de la couche 1 peuvent communiquer entre eux pour effectuer des vérifications et des sauvegardes. 
Ces ordinateurs de la couche 1 ne sont pas accessibles publiquement sur Internet, mais d'autres serveurs se synchronisent avec ceux-ci. En suivant la hiérarchie, ces appareils appartiennent à la couche 2 du modèle et ainsi de suite. D'une façon générale, un appareil se synchronisant avec une couche N appartient à la couche N+1. Ces couches sont une image de la précision de l'horloge de l'appareil en question. En effet, un appareil de la couche 1 communiquant directement avec une horloge de référence sera plus précis qu'un appareil de la couche 4, qui aura 3 intermédiaires avant cette horloge. 
Les appareils d’une même couche peuvent communiquer entre eux afin d'augmenter la précision de leur horloge. Lorsque l'on veut configurer un appareil pour qu'il utilise le protocole NTP, on lui donnera une référence avec laquelle se synchroniser. À part les serveurs de la couche 1. Il est possible de se synchroniser avec n'importe quel serveur NTP des autres couches à partir du moment où ce serveur est public.

**[00:02:36]**

Par exemple, cet ordinateur peut se connecter à ce serveur de la couche 3 ou à ce serveur de la couche 2 pour récupérer les informations d'horloge. 
Dans une grande infrastructure comme une entreprise ou une université, afin d'éviter un trop grand nombre de requêtes sortant vers un serveur NTP. On synchronise généralement un appareil de l'infrastructure vers un serveur public, puis cet appareil sert de référence pour les ordinateurs du parc informatique. 

Pour résumer, le protocole NTP sert à synchroniser les horloges des appareils informatiques. Cela est utile pour la maintenance, pour le routage des paquets, pour l'établissement de connexions sécurisées et, accessoirement, pour le confort des utilisateurs. Ce protocole utilise le protocole de transport UDP sur le port réservé 123. Les appareils se connectent de façon hiérarchique à une référence de plus haut niveau. La couche 0 comportant des horloges de référence. 

La hiérarchie du protocole NTP est composée de 16 couches de 0 à 15. Au-delà, on considère que l'heure n'est plus exploitable. Enfin, on peut noter que la synchronisation des appareils se fait de manière progressive lorsque les écarts ne sont pas trop importants et que le protocole estime les délais de transmission d'informations entre les appareils et s'en sert pour préciser la synchronisation. 

Dans ce chapitre 6 applications et services, nous avons vu quelques uns des services représentatifs des quelques milliers d'autres services qui existent pour cette couche 7 du modèle OSI. La couche application.

