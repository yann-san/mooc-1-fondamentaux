# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 4.3.3.1 : IPV4 part 1


**[00:00:01]**

Bienvenue dans la première vidéo de cette série consacrée à la couche réseau. L'objectif principal de ce chapitre est de comprendre les mécanismes d'échanges de messages entre des hôtes de différents réseaux. Pour cela, nous parlerons dans un premier temps du protocole IPv4 et des adresses associées. Puis, nous aborderons la configuration dynamique des hôtes avec le protocole DHCP et enfin, nous aborderons les adresses IPV6. Commençons donc par le protocole IPv4. Pour rappel, nous avons vu dans les vidéos précédentes qu'au sein d'un réseau local, les appareils étaient identifiés de manière unique par leur adresse MAC issus du protocole Ethernet. En résumé, pour communiquer au sein d'un réseau local, un appareil doit connaître l'adresse MAC du destinataire. Mais la tâche se complique lorsque l'on désire communiquer avec une machine très éloignée physiquement de l'expéditeur. En effet, l'affectation des adresses MAC n'observe aucune logique quant à la localisation des machines associées et il est donc quasiment impossible de localiser un hôte sur la planète à l'aide uniquement de son adresse MAC. La problématique devient donc comment communiquer avec un hôte précis dans un réseau distant? En résumé, on a plusieurs réseaux. On veut retrouver un appareil dans un autre réseau. Comment fait-on?
Eh bien, il apparaît la nécessité d'un adressage supplémentaire qui sera hiérarchique. Et c'est ce que va apporter le protocole IP. Pour donner une analogie rapide expliquant en adressage hiérarchique, on peut penser à une adresse postale si on envoie un courrier depuis l'Australie à cette adresse. Le facteur australien ne sera intéressé que par le pays, la France.

**[00:01:32]**

Une fois arrivé en France, le paquet sera redirigé vers le département, la Lozère 48 et enfin jusqu'à la commune, puis jusqu'au domicile par le facteur de la rue. On comprend bien ici que la hiérarchisation de l'adresse facilite l'acheminement du paquet. 

Une adresse IP est composée de 4 octets, permettant de puissance 32 adresses possibles, dont on donne un exemple ici. Pour simplifier la lecture de cette adresse, on utilise une adresse décimale, en l'occurrence ici 132.34.5.253 . Comme nous l'avons dit, cette adresse est hiérarchique. Elle est divisée en deux parties : la partie réseau à gauche, puis la partie haute à droite. L'identifiant réseau identifie le réseau. Il est identique pour tous les hôtes du réseau et l'identifiant de l'hôte identifie l'hôte au sein de ce réseau. En dehors des adresses privées que nous évoquerons plus tard, une adresse IP est unique au monde. Elle permet donc l'identification unique d'un appareil. À la différence de l'adresse MAC, l'attribution de l'adresse IP dépend de la localisation de l'hôte. 
Les adresses IP sont des adresses logiques par opposition aux adresses MAC, qui sont les adresses dites physiques. Nous verrons que ces deux types d'adresses sont complémentaires dans l'acheminement d'un message entre deux hôtes. La question que nous allons nous poser à présent est comment séparer la partie réseau de la partie hôte dans une adresse IP? Dans l'exemple que nous venons de voir, je vous ai dit que les deux premiers octets composaient la partie réseau. Mais la séparation n'est pas la même suivant les adresses IP. Anciennement, les adresses IP étaient répartis selon plusieurs classes.

**[00:03:01]**

Les premiers bits déterminaient la classe à laquelle appartenait une adresse IP. Si le premier bit était 0, alors on avait une adresse de classe A. Si les deux premiers bits étaient 1 0, ont une adresse de classe B et ainsi de suite. En fonction de la classe de l'adresse, on connaissait alors la taille de la partie réseau : 1 octet pour la classe A, 2 octets pour la classe B et 3 octets pour la classe C. Les classes D et E servent respectivement à la multidiffusion ou sont réservées. 
A partir de l'adresse IP, on pouvait alors déterminer sa classe et donc différencier la partie réseau de la partie haute. L'inconvénient principal de l'adressage par classe est son manque de souplesse dans la définition des tailles de réseau. En effet, on peut voir qu'un réseau de classe A contient 2²⁴ adresses, un réseau de classe B et 2¹⁶, soit 65536, et un réseau de classe C 256 adresses. 
Il peut en résulter notamment un gaspillage d'adresse. Par exemple, une structure ayant besoin de 300 adresses devra se faire attribuer une plage de classe B consommant 65536 adresses pour uniquement 300 utilisées. Aujourd'hui, la découpe partie réseau - partie hôte se fait grâce à un masque de sous-réseau. 
Un masque de sous réseau comporte 4 octets autant que les adresses IP et est composé d'une série de 1 suivie d'une série de zéros. Les uns correspondent à la partie réseau et les zéros à la partie hôte. Avec cet exemple de masque de sous réseau, ici, on a les 2 premiers octets qui sont associés à la partie réseau.

**[00:04:29]**

Les deux derniers à la partie hôte. Encore une fois, on préfère utiliser une notation décimale ici, en l'occurrence 255.255.. On a également une notation CIDR qui correspond au nombre de 1 dans le masque. Ici, nous avons 16 1, car 2 octets. Le CIDR sera donc Slash 16. Enfin, pour obtenir l'adresse réseau à partir de l'adresse IP et du masque, on fera l'opération ET logique entre le masque et l'adresse IP. L'adresse réseau est une adresse réservée qui identifie le réseau de manière unique. 
Alors ici, quand on fait l'opération et on obtient le résultat suivant. Donc 1 et 1, ça donne 1 0 ET 1 = 0 ; 1 ET 1 = 1 ; 0 ET 1  = 0 et ainsi de suite. En fait pour les 2 premiers octets, je suis en train de recopier les octets de l'adresse IP. Ensuite, lorsqu'on arrive au niveau des 0, eh bien quoi que ce soit, ET 0 fera toujours 0 1 0, ça fait 0 0 0, ça fait 0. 
J'obtiens donc que des zéros sur la suite de mon adresse réseau, ce qui nous donne l'adresse réseau suivante en décimal 170.7.0.0. L'opération logique ET entre le masque et l'adresse IP nous a donc donné l'adresse du réseau, l'adresse réseau première de la plage et donc réservée à l'identification du réseau. Une deuxième adresse est réservée : la dernière de la plage, qui sert d'adresse de diffusion.

**[00:06:00]**

Elle permet l'envoi d'un message à tous les hôtes d'un réseau. 

Dans ce deuxième exemple, calculons l'adresse réseau et l'adresse de diffusion. Cette fois, le masque de sous réseau n'est plus composé uniquement d'octets complets. En effet, le troisième octet commence par deux 1 lorsque l'on fait l'opération logique et le troisième octet de l'adresse réseau devient donc 1000 0000, ce qui nous donne 128. On obtient donc l'adresse de réseau  170.7.128.0 et on peut également calculer l'adresse de broadcast, qui est 170.7.191.255. 
On comprend donc ici la nécessité de passer en binaire pour appliquer le masque à l'adresse IP. Comme nous l'avons dit précédemment, les adresses IP sont uniques dans le monde. Pour permettre cette unicité, les adresses IP sont réparties par l’ICANN selon une configuration géographique. Le schéma affiché ici représente la répartition par territoire des différentes plages d'adresses. On constate une forte représentation de l'Amérique du Nord, de l'Europe et de l'Asie. En revanche, le continent africain a très peu de plages attribuées. Cette répartition très inégale du nombre d'adresses IP apparaît problématique dans un monde qui se numérise un peu plus chaque jour et peut justifier à lui seul un passage à un protocole IP plus adapté. 

Certaines adresses IP sont réservées, comme le montre le tableau suivant. Ces adresses ne sont pas routable sur Internet. Cela signifie qu'aucune machine possédant une adresse de ce type ne pourra communiquer sur Internet. Nous nous concentrons sur trois plages réservées importantes les plages d'adresses IP, celles ci.

**[00:07:38]**

Celle là et celle là.

**[00:07:42]**

Ces plages ne sont pas utilisables sur Internet, mais peuvent être utilisées librement sur un réseau privé. Cela permet par exemple à un fournisseur d'accès de donner une seule adresse publique à un domicile. 
Les appareils de ce domicile auront alors en local une adresse privée, souvent de classe C, par exemple 192 168.1.10. La box du client utilisera alors ce que l'on appelle le protocole NAT, que nous verrons plus tard pour transmettre les messages sur Internet à l'aide de sa propre adresse publique. 
D'autres plages réseau sont réservées pour d'autres usages, par exemple le 127.0.0.0 avec un masque de sous réseau à huit 1 qui va être une adresse de bouclage, c'est-à-dire qu'elle revient sur l'hôte en question. Les adresses 224.0.0.0/4 vont être des adresses de multicast pour s'adresser à plusieurs appareils à la fois.

