# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 4.3.2.2 : Le protocole Ethernet


**[00:00:00]**
Comme on a vu avec le modèle OSI, Ethernet est un protocole très utilisé au niveau de la couche 2 liaison de données. 
Aussi, on a vu que le PDU est manipulé au niveau de la couche 2 était appelé la trame qui est le message construit et envoyé par le protocole Ethernet. 

Nous verrons dans cette vidéo plus amplement les notions de trame, ce qu'est une adresse MAC et enfin l'algorithme de CRC. 

Imaginez une conversation à deux. Ce n'est pas bien compliqué. Une personne parle et l'autre l'écoute. Mais dès que le nombre de participants augmente, ça devient plus compliqué, car l'on peut vouloir s'adresser à une personne en particulier pour lui communiquer une information secrète. En réseau, c'est pareil. 
On veut parfois parler à tout le monde, mais aussi et la plupart du temps, parler à une machine en particulier. Et pour pouvoir le faire, il faut être capable d'identifier la machine en question. Les chercheurs ont donc créé un identifiant particulier à la couche 2, qui permettrait de distinguer les machines entre elles. Il s'agit de l'adresse MAC. 

Afin de communiquer on utilise une carte réseau, ça peut être une carte Ethernet ou une carte Wi-Fi. Ces cartes possèdent un numéro de série unique au monde généré par le constructeur, qui identifie de façon unique la carte réseau d'un appareil. 
Sur la figure à droite, on peut par exemple voir l'adresse Mac de la carte Wi-Fi d'un iPhone. L'adresse Mac est composée de deux parties. La partie OUI est donnée par un organisme international pour identifier le vendeur. Dans notre cas, elle fait référence à Apple. La seconde partie est assignée par le vendeur pour identifier l'appareil.

**[00:01:43]**
Vous pouvez également sur votre machine connaître l'adresse MAC de votre carte réseau au moyen de la commande ifconfig ou ip a si vous utilisez un système Linux ou ipconfig sur Windows. L'adresse MAC s'écrit en hexadécimal. Elle est donc codée sur 48 bits, c'est à dire 6 octets, ce qui donne près de 256 000 milliards de possibilités, de telle sorte que chaque adresse soit unique au monde. Parmi les adresses MAC, il y en a une particulière. C'est l'adresse dans laquelle tous les bits sont à 1, ce qui donne une adresse hexadécimal avec tous les caractères à F. Cette adresse est appelée l'adresse de broadcast. L'adresse de broadcast est une adresse universelle qui cible toutes les cartes Ethernet du sous réseau. Elle permet ainsi d'envoyer un message à toutes les cartes réseau des machines présentes sur le sous réseau en une seule fois. 

Le protocole Ethernet est un langage de communication entre les machines. Ce langage permettra de définir le format des messages que les ordinateurs vont s'échanger. Il n'est pas le seul protocole de la couche 2, mais il est de loin le plus utilisé. Ainsi, le protocole Ethernet construit le PDU de la couche 2, qui est appelé trames Ethernet. 
Une trame Ethernet contient les adresses MAC de destination et source. Le contenu du message envoyé et le CRC, elle commence par l'adresse de destination suivie par l'adresse source, et l’EtherType qui indique le protocole utilisé à la couche 3, qui peut être IPv4, IPv6 ou RP, tel que le montre ce tableau. Ce premier bloc est codé sur 14 octets avec deux fois 6 octets pour les adresses MAC et deux octets pour l’EtherType.


**[00:03:30]**
Le message en lui-même ou payload a une taille de 46 à 1500 octets et enfin, le CRC, codé sur 4 octets, est un code mathématique représentant les données, qui permet de réaliser la détection des erreurs dans la trame due à la transmission, par exemple. 
En tout, la trame Ethernet standard a une taille minimale de 64 octets pour un pilote de 46 octets et une taille maximale de 518 octets. La taille maximale est utile pour s'assurer qu'une gigantesque trame n'occupe pas tout le réseau, empêchant les autres machines de communiquer. Il existe cependant des trames plus grandes, appelées jumbo frames, pouvant avoir une taille jusqu'à 9 000 octets, qui ne sont par contre pas acceptées par tous les réseaux. 

Ce format de trames Ethernet n'est pas le seul, bien qu'étant le plus répandu. En effet, il existe quatre types de trames Ethernet, Ethernet II, Novell raw IEEE 802.3, IEEE 802.2 LLC et IEEE 802.2 SNAP. Celui que nous vous avons présenté correspond à Ethernet 2, qui est utilisé pour TCP/IP. 

Enfin, le CRC d'une trame est un code mathématique qui représente le contenu de la trame. Il est calculé avant la transmission de la trame et inscrit sur les quatre octets à la fin de la trame. Après transmission, il est recalculé et  comparé au nombre de fin de trame pour s'assurer que les données sont probablement identiques. Une différence conduit à la non-acceptation de la trame. Certains protocoles de transport demandent alors la réémission de la trame et d'autres non.

**[00:05:16]**
Le principe du CRC consiste à traiter les séquences binaires comme des polynômes binaires, c'est-à-dire des polynômes dont les coefficients correspondent à la séquence binaire. Ainsi, la séquence binaire 0110101001 peut être représentée sur la forme du polynôme suivant. 
Le CRC est obtenu alors par calcul du reste de la division du polynôme M(X) obtenu, qui est le message en clair par un polynôme G(X) qui est le polynôme générateur d'Ethernet. De ce fait, l'émetteur envoie des bits correspondant au polynôme M(X), suivi de CRC(X). 
Le récepteur doit alors s'assurer que le message M’(X) n'a pas été corrompu. Il calcule dans le CRC :  CRC’(X) = M’(X) modulo G(X).  G(X) est connu par les deux, car il est fixé par le standard. 
Si CRC’(X) = CRC(X), alors la probabilité est extrêmement faible que le message ait été corrompu. 

En résumé, nous avons vu dans cette vidéo ce qu'est l'adresse MAC, qui est le numéro de série identifiant de façon unique une carte réseau. Une adresse MAC est codée sur 6 octets, ce qui correspond à 48 bits. Ensuite, nous avons vu le protocole Ethernet, qui est le protocole le plus utilisé à la couche 2 du modèle OSI. Il définit le format du message envoyé qui est la trame. Enfin, nous avons vu le CRC qui est une valeur mathématique représentative des données envoyées utilisée pour la détection des erreurs de transmission.

