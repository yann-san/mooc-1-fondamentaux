
## 4.3.1.5 : le modèle OSI (MOOC NSI)
###  Transcription de la vidéo

**[00:00:00]**
Dans les vidéos précédentes, nous avons vu différents types de réseaux dans cette partie, nous allons vous parler du modèle qui tente de normaliser les communications sur ces réseaux informatiques. Il s'agit du modèle OSI. 

Le modèle OSI est une norme de communication en réseau pour tous les systèmes informatiques, que ce soit des PC, des tablettes, des smartwatches ou même des machines industrielles. Il a été proposé en 1984 par l'ISO pour International Organization for Standardisation et il s'agit d'un modèle conceptuel qui caractérise et normalise la façon dont les différents composants, logiciels et matériels impliqués dans une communication en réseau doivent se diviser le travail et interagir les uns avec les autres. 
Ainsi, il identifie et organise tous les éléments et fonctionnalités nécessaires à la communication pour garantir une facilité d'évolutivité et d'interopérabilité entre les équipements de différents constructeurs par exemple. 

Le modèle OSI est un modèle en couches. Il est composé de sept couches, tel que le montre la figure à gauche de l'écran. Chaque couche a un rôle bien défini et assure donc une fonctionnalité bien spécifique dans la communication. 
Les deux couches inférieures ou couches matérielles sont plutôt orientées communication et sont souvent fournies par un système d'exploitation et par le matériel. Par ailleurs, ces couches sont normalement transparentes pour les données à transporter et l'utilisateur final ne voit pas ce qui s'y passe, à moins de le chercher. 
Les trois couches supérieures aux couches hautes sont plutôt orientées applications et plutôt réalisées par des bibliothèques ou en programmes spécifiques. Dans le monde IP, ces trois couches sont rarement distinguées. 
Enfin, les couches intermédiaires, comme leur nom l'indique, servent de relais entre les couches matérielles et les couches hautes.

**[00:01:52]**
Pour chaque couche, il existe des protocoles qui définissent comment est réalisée la fonctionnalité de la couche. 
Les messages échangés par les protocoles sont appelés PDU pour Protocol Data Unit et représentent l'unité de données manipulée ou produite au niveau de la couche.
Sur la figure à gauche de l'écran, on peut voir les différents PDU manipulés à chaque couche du modèle. 
Dans une telle architecture, les données sont transmises de couche en couche depuis la couche application où elles sont produites jusqu'à la couche physique. 
Pendant la transmission, chaque couche ajoute un entête au PDU provenant de la couche supérieure qui dirige et identifie le paquet. Ce processus s'appelle l'encapsulation. L'entête et les données forment ensemble le PDU pour la couche suivante. Le processus se poursuit ainsi jusqu'à atteindre la couche du plus bas niveau, qui est la couche physique, à partir de laquelle les données sont transmises sur le réseau jusqu'aux dispositifs de réception. 
Le dispositif de réception inverse le processus en desencapsulant les données à chaque couche avec les informations d'entête dirigeant les opérations. 

Dans ce slide, nous allons vous présenter les rôles de chaque couche du modèle OSI. Les trois couches hautes ont des rôles confondus en fonction des cas. Elles servent notamment de point d'accès aux services réseau, mais peuvent aussi assurer le codage des données applicatives ainsi que la synchronisation des échanges et transactions. 
La couche transport gère les communications de bout en bout entre les processus qui sont des programmes en cours d'exécution.

**[00:03:26]**
La couche réseau, elle, interconnecte, comme son nom l'indique, les réseaux différents entre eux. Elle assure pour cela les fonctions de routage et d'adressage des paquets au moyen d'un équipement appelé routeur. Il s'agit de transmettre les messages au destinataire sur la base de leur adresse, éventuellement en les acheminant par d'autres nœuds. Si le message est trop lent, le réseau peut le diviser en plusieurs segments au niveau d'un nœud et les envoyer séparément. Tous ces mécanismes sont gérés par la couche réseau. 
La couche liaison de données gère les communications entre deux machines directement connectées entre elles ou connectées à un équipement qui émule une connexion directe comme un commutateur ou switch. Ainsi, elle définit le protocole permettant d'établir et de terminer une connexion entre deux dispositifs physiquement connectés. La couche liaison de données est généralement divisée en deux sous couches : la couche de contrôle d'accès aux médias ou MAC et la couche de contrôle de liaison logique ou LLC. La couche MAC est chargée de contrôler la façon dont les appareils d'un réseau accèdent à un support et sont autorisés à transmettre des données dessus et la couche LLC est responsable du contrôle et de la vérification des erreurs et de synchronisation des trames. 
Enfin, la couche physique est chargée de la transmission effective des signaux entre les interlocuteurs. Son service est limité à l'émission et à la réception d'un train de bits continu sur le support de transmission. Ainsi, elle pourra produire un signal électrique si le support est un câble Ethernet ou un signal lumineux s'il s'agit de la fibre optique.

**[00:05:11]**
Le modèle TCP/IP est également un modèle de référence en couches, mais c’est un modèle à quatre couches. Il est également connu sous le nom de suite de protocole Internet. Il est généralement appelé TCP/IP parce que les protocoles de base sont TCP et IP, mais ce ne sont pas les seuls protocoles utilisés dans ce modèle. 
Il est à noter que le modèle OSI n'est qu'un modèle conceptuel. Il est principalement utilisé pour la description et la compréhension des fonctions individuelles des réseaux. Cependant, TCP/IP est d'abord conçu pour résoudre un ensemble spécifique de problèmes et non pour fonctionner comme une description générique pour toutes les communications réseau, comme l'est le modèle OSI. 
Ainsi, le modèle OSI est générique, indépendant du protocole, mais la plupart des protocoles et des systèmes y adhèrent, tandis que le modèle TCP/IP est basé sur des protocoles standards que l'Internet a développés. 
Le modèle TCP/IP est plus ancien que le modèle OSI. La figure à gauche de l'écran montre les relations correspondantes entre leurs couches. 

La couche application du modèle TCP IP permet aux applications d'accéder au service des autres protocoles et détermine les protocoles que les applications utilisent pour échanger les données. Les protocoles de couche d'applications les plus connus sont HTTP, FTP ou encore SMTP. 
La couche transport est chargée de fournir à la couche application des services de communication, de session et de trames. Les protocoles de base de cette couche sont TCP pour Transmission Control Protocol et UDP pour User Datagramme Protocol. 
La couche Internet est responsable des fonctions d'adressage, du conditionnement et de routage des hôtes.

**[00:06:55]**
Les principaux protocoles de la couche Internet sont IP, Le protocole de résolution d'adresse ARP, le protocole de messagerie de contrôle Internet ICMP et le protocole de gestion de groupe Internet IGMP. 
Enfin, la couche d'accès réseau ou couche de liaison est responsable de placer et de recevoir les paquets TCP/IP dedans et en dehors du support réseau. 
TCP/IP est conçu pour être indépendant de la méthode d'accès au réseau, du format de trame et du support et peut donc être utilisé pour connecter différents types de réseau tels que l'Ethernet ou le Token Ring. 

Une autre chose à noter dans le modèle OSI est que toutes les couches ne sont pas utilisées dans des applications plus simples. C'est le cas du bus CAN pour Controller Area Network utilisé pour les interactions en réseau dans les voitures, par exemple. L'ensemble des nœuds d'un même réseau est connecté sur le même câble. Les couches réseau et transport sont alors inutiles. On obtient en modèle à trois couches avec les couches physique, liaison de données et applications tel que le montre la figure sur l'écran. 

En résumé, on a vu que le modèle OSI est une norme qui décrit les fonctionnalités nécessaires à la communication et les organise. C'est un modèle composé de sept couches qui s'échangent de façon adjacente des PDU. Le modèle TCP IP est aussi un modèle de référence, mais lui est composé de quatre couches. Enfin, le modèle OSI est un modèle théorique et dans la pratique, ses implémentations peuvent être plus simples, comme on l'a vu avec l'exemple du bus CAN.

