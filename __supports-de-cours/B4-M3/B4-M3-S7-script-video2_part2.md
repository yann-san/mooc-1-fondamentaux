# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 4.3.7.2 : Le chiffrement part


**[00:00:01]**
Nous avons tout à l'heure évoqué la lenteur des algorithmes asymétriques comme une limite de ceux-ci, et bien à titre comparatif, voici les performances d'un algorithme asymétrique et RSA et d'un algorithme symétrique DES. En logiciel, DES est 100 fois plus rapide que le chiffrement RSA. Sur une implantation matérielle, il est entre 1000 et 10.000 fois plus rapide. On a donc un débit de chiffrement entre 300 mégabits par seconde et 3 gigabits par seconde comparé à 300 kbit par seconde pour RSA. 
Afin de contourner ces mauvaises performances du RSA, les deux types de chiffrement symétrique et asymétrique sont utilisés lors de la même transmission ; on parle alors de chiffrement mixte. 
Pour envoyer un message chiffré, le programme émetteur ne chiffrera pas le message complet avec un algorithme asymétrique. Il chiffrera en asymétrique, avec la clé publique du destinataire uniquement un petit nombre aléatoire de quelques dizaines de caractères choisi par lui. Ce nombre servira de clé secrète qui sera utilisée pour chiffrer de manière symétrique le texte. 
À l'arrivée, le programme du destinataire déchiffrera la clé secrète chiffrée en asymétrique avec sa clé privée. Muni de la clé secrète ainsi déchiffrée, il déchiffre ensuite le message chiffré en symétrique. Dans ce processus, l'algorithme asymétrique n'est utilisé que sur quelques dizaines de caractères. L'opération sera donc relativement rapide. Le message, qui peut être un fichier de plusieurs gigabytes, sera lui, chiffré et déchiffré avec un algorithme symétrique.

**[00:01:47]**

Le temps de traitement de l'ensemble sera donc du même ordre que si l'on n'avait utilisé que du chiffrement symétrique. D'autre part, l'introduction du chiffrement asymétrique permet de s'affranchir du problème de la gestion de la clé secrète des algorithmes symétriques. En effet, cette clé secrète n'a pas besoin d'être connue par le destinataire avant la communication, elle est choisie par l'émetteur et le destinataire en prend connaissance en déchiffrant une partie de ce qu'il a reçu. Ce mécanisme à deux étapes est employé par les outils sécurisés de messagerie, transfert de fichiers et navigation. La clé secrète ne servant que pour l'opération en cours et appelée clé de session. Au prochain transfert, le programme de l'émetteur choisira une autre clé de session. Un intrus qui récupère le document chiffré ne pourra pas déchiffrer la clé de session, car il ne possède pas la clé privée du destinataire, et, sans cette clé de session, il ne pourra pas déchiffrer le texte du message. 

Nous pouvons parler de quelques exemples d'algorithme de chiffrement. 
Dans les symétriques, nous avons DES de 1974. Il a été élaboré par IBM, puis adopté comme norme de chiffrement par l'administration américaine en 1977, qui est à ce jour le plus répandu. Les données sont codées par blocs de 64 bits avec une clé de 56 bits. Cet algorithme est très utilisé dans les applications financières. Il existe de nombreuses variantes de l'algorithme DES comme 3DES, qui utilise trois niveaux de chiffrement, ce qui implique une clé de chiffrement sur 168 bits.

**[00:03:30]**

Ensuite, nous avons IDEA pour International Data Encryption Algorithm de 1992 qui est une initiative de développement ouvert qui a abouti à un algorithme de chiffrement presque symétrique, fiable et performant. Les bases théoriques ont été largement diffusées et les développements effectués au grand jour. Il est aussi performant que DES et s'implante bien sur des composants matériels. 

Du côté du chiffrement asymétrique, nous avons un RSA de 1978, du nom de ses inventeurs Rivest, Shamir et Adleman, qui est l’algorithme de chiffrement asymétrique le plus répandu et le plus célèbre. La longueur de la clé générée par RSA de 512 bits n'est aujourd'hui plus vraiment suffisante. On lui préférera du 1024 ou du 2048 bits. Le principe de chiffrement de RSA se base sur le choix de deux grands nombres premiers d'au moins 200 chiffres. La fiabilité de RSA repose sur le problème de la factorisation des grands nombres. 
En choisissant un autre problème mathématique réputé difficile, on peut créer un autre algorithme de chiffrement asymétrique. L'autre méthode populaire est basée sur le problème du logarithme discret. L'algorithme El Gamal est basé sur ce problème. Il a donné naissance aux DSA/DSS pour Digital Signature Algorithm/System, sélectionné par les agences gouvernementales américaines NIST et MSA.


