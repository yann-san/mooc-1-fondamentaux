1
00:00:01,630 --> 00:00:04,258
Prenons maintenant un exemple.

2
00:00:04,358 --> 00:00:05,581
On va faire un exercice.

3
00:00:05,845 --> 00:00:07,477
J'ai quatre programmes,

4
00:00:07,751 --> 00:00:11,901
ici, qui, soi-disant, vont me calculer

5
00:00:12,001 --> 00:00:13,331
la factorielle d'un nombre.

6
00:00:13,628 --> 00:00:15,774
Je rappelle que factorielle n,

7
00:00:20,753 --> 00:00:22,944
c'est un schéma d'accumulation classique

8
00:00:23,244 --> 00:00:24,634
dans une variable

9
00:00:24,734 --> 00:00:26,923
où on va mettre au fur et à mesure.

10
00:00:27,023 --> 00:00:28,825
C'est exactement la même chose que la puissance

11
00:00:28,925 --> 00:00:30,561
que nous avons vue avec la complexité.

12
00:00:31,142 --> 00:00:34,698
Ici, vous avez quatre programmes,

13
00:00:34,798 --> 00:00:36,063
quatre algorithmes,

14
00:00:36,394 --> 00:00:39,652
qui soi-disant calculent

15
00:00:40,110 --> 00:00:41,042
factorielle n.

16
00:00:41,349 --> 00:00:42,884
Je vous demande,

17
00:00:42,984 --> 00:00:44,960
donc là, vous pouvez faire une petite pause,

18
00:00:45,233 --> 00:00:47,141
je vous demande de regarder

19
00:00:47,439 --> 00:00:49,565
lesquels de ces algorithmes sont corrects,

20
00:00:49,665 --> 00:00:51,615
pourquoi, comment, qu'est ce qu'il y a derrière,

21
00:00:51,917 --> 00:00:54,490
et de faire la preuve de ces algorithmes.

22
00:00:58,313 --> 00:01:01,873
Reprenons maintenant le cours de notre vidéo.

23
00:01:03,808 --> 00:01:05,287
Si je prends le programme A,

24
00:01:05,559 --> 00:01:07,040
je vais me poser la question

25
00:01:07,444 --> 00:01:09,483
quel peut être l'invariant

26
00:01:09,583 --> 00:01:11,213
qui est associé à ce programme A ?

27
00:01:11,492 --> 00:01:13,907
Je peux me donner comme invariant

28
00:01:14,465 --> 00:01:17,710
par exemple F est égal à factorielle i.

29
00:01:18,015 --> 00:01:18,950
Alors, il faut bien comprendre,

30
00:01:19,050 --> 00:01:20,013
ici, on dit toujours

31
00:01:20,113 --> 00:01:21,386
la valeur de F

32
00:01:21,979 --> 00:01:26,045
est égale à la factorielle de la valeur de la variable i.

33
00:01:26,925 --> 00:01:29,185
Alors, au départ, effectivement,

34
00:01:29,285 --> 00:01:30,329
c'est bien vérifié

35
00:01:30,429 --> 00:01:34,539
F est égal à 1 qui est égal à factorielle 0 qui est égal à factorielle i.

36
00:01:37,608 --> 00:01:40,630
Supposons que cette condition soit vérifiée

37
00:01:40,730 --> 00:01:41,726
en début de boucle,

38
00:01:42,031 --> 00:01:45,559
j'incrémente i de 1,

39
00:01:45,941 --> 00:01:48,235
je vais avoir F'

40
00:01:48,743 --> 00:01:53,226
qui est égal à F fois i'

41
00:01:53,326 --> 00:01:55,215
mais i', c'est i plus 1

42
00:01:55,634 --> 00:01:58,461
et là, j'utilise mon invariant,

43
00:01:58,561 --> 00:02:07,193
ici, l'invariant, il arrive là,

44
00:02:08,066 --> 00:02:10,086
et donc j'obtiens ici

45
00:02:14,441 --> 00:02:17,944
que la valeur de F'

46
00:02:18,044 --> 00:02:22,470
est égale à la valeur de factorielle i + 1

47
00:02:22,750 --> 00:02:26,448
et donc, comme la valeur de i + 1, c'est la valeur de i',

48
00:02:27,807 --> 00:02:32,377
j'ai donc bien F' égal à factorielle i'.

49
00:02:32,477 --> 00:02:34,782
Donc mon programme est super cool,

50
00:02:35,104 --> 00:02:37,392
l'invariant est bien propagé,

51
00:02:37,817 --> 00:02:40,317
et donc j'arrive à la solution.

52
00:02:42,638 --> 00:02:43,934
Sauf que

53
00:02:44,331 --> 00:02:46,494
qu'est-ce qu'il se passe quand je sors de la boucle ?

54
00:02:47,035 --> 00:02:48,721
Quand je sors de la boucle,

55
00:02:49,053 --> 00:02:51,263
j'ai bien la valeur de F

56
00:02:51,930 --> 00:02:56,730
qui est égale à factorielle i.

57
00:02:57,629 --> 00:02:58,787
Donc ça, c'est acquis,

58
00:02:58,887 --> 00:03:00,583
mais j'ai la condition

59
00:03:02,243 --> 00:03:08,042
i est inférieur ou égal à n

60
00:03:08,142 --> 00:03:09,191
qui n'est pas vérifiée.

61
00:03:11,778 --> 00:03:14,525
Quelle est la valeur de i à ce moment-là ?

62
00:03:14,799 --> 00:03:16,069
La valeur de i

63
00:03:17,737 --> 00:03:20,975
va être égale à n + 1

64
00:03:22,648 --> 00:03:25,119
et donc, en sortie de l'itération,

65
00:03:25,219 --> 00:03:26,991
au lieu de fournir factorielle n,

66
00:03:27,327 --> 00:03:30,197
je fournis factorielle n + 1.

67
00:03:30,673 --> 00:03:33,879
Donc mon programme n'est pas correct

68
00:03:34,167 --> 00:03:36,513
même si j'ai des invariants qui marchent bien,

69
00:03:36,890 --> 00:03:38,515
la postcondition

70
00:03:38,615 --> 00:03:39,827
n'est pas vérifiée

71
00:03:40,234 --> 00:03:42,455
et donc je me suis trompé

72
00:03:42,555 --> 00:03:44,597
dans mes indices quelque part.

73
00:03:45,539 --> 00:03:46,638
Aïe.

74
00:03:47,315 --> 00:03:48,706
Regardons le programme suivant,

75
00:03:48,806 --> 00:03:49,510
le programme B.

76
00:03:49,837 --> 00:03:52,617
Donc j'ai i égal 0 et F égal 1,

77
00:03:52,717 --> 00:03:54,922
et puis while i plus petit que n,

78
00:03:55,022 --> 00:03:56,534
je fais i égale i + 1

79
00:03:56,634 --> 00:03:57,780
et je multiplie.

80
00:03:58,136 --> 00:04:00,442
Et là, je vais prendre comme invariant,

81
00:04:00,542 --> 00:04:03,324
pourquoi pas, f égale factorielle i.

82
00:04:03,849 --> 00:04:05,899
Alors regardons d'un peu plus près.

83
00:04:06,401 --> 00:04:08,665
Au départ, c'est bien vérifié.

84
00:04:09,783 --> 00:04:13,005
Si je suppose que j'ai F égal à factorielle i

85
00:04:13,105 --> 00:04:15,224
en début de bloc,

86
00:04:15,324 --> 00:04:16,625
qu'est-ce qu'il va se passer ?

87
00:04:16,895 --> 00:04:18,475
À la fin du bloc,

88
00:04:18,575 --> 00:04:20,918
F', c'est F fois i',

89
00:04:21,018 --> 00:04:22,243
et cætera, et cætera.

90
00:04:23,468 --> 00:04:25,761
Et F' égale factorielle i'.

91
00:04:26,110 --> 00:04:27,959
Quand je sors de ma boucle,

92
00:04:28,499 --> 00:04:30,649
j'ai i qui est égal à n

93
00:04:30,749 --> 00:04:32,021
puisque je sors de la boucle

94
00:04:32,339 --> 00:04:35,362
et que j'ai la négation de la condition i inférieur à n,

95
00:04:36,270 --> 00:04:38,179
et j'ai F qui est égal à factorielle n.

96
00:04:38,674 --> 00:04:41,139
Donc la valeur de F

97
00:04:41,239 --> 00:04:43,396
en fin de mon algorithme

98
00:04:43,928 --> 00:04:45,747
est bien égale à factorielle n.

99
00:04:46,232 --> 00:04:48,545
Donc ça marche.

100
00:04:48,645 --> 00:04:50,030
Alors, le programme C.

101
00:04:50,730 --> 00:04:51,986
Regardons le programme C.

102
00:04:52,086 --> 00:04:53,865
i égale 1, F égale 1,

103
00:04:54,277 --> 00:04:55,891
while i inférieur ou égal à n

104
00:04:55,991 --> 00:04:57,928
et puis je fais F égale F fois i

105
00:04:58,220 --> 00:04:59,984
et i égale i plus 1.

106
00:05:01,027 --> 00:05:02,306
Donc j'ai changé

107
00:05:02,406 --> 00:05:05,896
l'ordre de mes deux instructions

108
00:05:05,996 --> 00:05:07,012
à l'intérieur du bloc.

109
00:05:07,813 --> 00:05:11,290
Alors je vais reprendre mon invariant F égale factorielle i.

110
00:05:13,933 --> 00:05:15,837
Qu'est-ce qu'il se passe à la fin ?

111
00:05:17,207 --> 00:05:18,776
Quand je vais sortir de ma boucle,

112
00:05:18,876 --> 00:05:21,624
je vais avoir F qui va être égal

113
00:05:21,724 --> 00:05:23,607
à factorielle i

114
00:05:24,877 --> 00:05:27,051
mais i' est différent de i.

115
00:05:27,151 --> 00:05:28,160
Et donc je n'ai pas

116
00:05:28,260 --> 00:05:30,774
la valeur de la variable F

117
00:05:31,203 --> 00:05:32,475
en sortant de la boucle

118
00:05:39,210 --> 00:05:43,223
ne sera pas égale à la factorielle de i'.

119
00:05:43,490 --> 00:05:46,154
Donc ce programme n'est pas correct,

120
00:05:46,254 --> 00:05:48,875
je vais un cran trop loin sur l'indice i,

121
00:05:49,435 --> 00:05:51,429
donc ça ne marche pas.

122
00:05:51,529 --> 00:05:53,253
Par contre, j'étais bien sur factorielle i

123
00:05:53,353 --> 00:05:55,869
pour la valeur de F.

124
00:05:58,391 --> 00:05:59,573
Donc là, j'ai un petit souci.

125
00:05:59,673 --> 00:06:01,245
Alors, comment est-ce que je peux faire ?

126
00:06:01,743 --> 00:06:04,072
Je peux essayer de regarder ce qu'il se passe.

127
00:06:04,172 --> 00:06:05,790
En fait, mon incrément, je le fais après.

128
00:06:06,582 --> 00:06:10,799
Si je veux démontrer mon programme,

129
00:06:11,098 --> 00:06:13,106
je vais regarder comme invariant

130
00:06:13,206 --> 00:06:15,259
non plus F égale factorielle i,

131
00:06:15,359 --> 00:06:17,172
mais F égale factorielle de i moins 1.

132
00:06:17,670 --> 00:06:19,782
Mon décalage, je le fais aussi

133
00:06:19,882 --> 00:06:20,947
sur mon invariant.

134
00:06:21,530 --> 00:06:24,240
Dans ce cas-là, j'ai F égale factorielle de i - 1

135
00:06:24,563 --> 00:06:26,257
F égale F fois i

136
00:06:26,757 --> 00:06:27,699
donc ça va nous donner

137
00:06:27,799 --> 00:06:29,618
F' égale F fois i

138
00:06:29,893 --> 00:06:31,505
égale factorielle de i - 1 fois i

139
00:06:31,605 --> 00:06:33,319
égale factorielle i.

140
00:06:33,638 --> 00:06:36,566
Et i ', c'est i + 1

141
00:06:37,291 --> 00:06:42,570
donc ça fait F' égale factorielle de i' - 1.

142
00:06:42,884 --> 00:06:43,588
Waouh !

143
00:06:43,863 --> 00:06:44,908
Donc ça marche.

144
00:06:45,848 --> 00:06:49,226
En sortie de boucle, j'ai i égale n + 1

145
00:06:53,087 --> 00:06:55,057
et j'ai F qui est égal à factorielle n,

146
00:06:55,422 --> 00:06:57,305
donc factorielle de i - 1.

147
00:06:57,685 --> 00:07:00,514
donc factorielle de n + 1 - 1.

148
00:07:00,614 --> 00:07:02,343
Donc cet algorithme est correct.

149
00:07:04,333 --> 00:07:05,560
Évidemment, il est un peu tordu

150
00:07:05,660 --> 00:07:09,271
mais on aura, dans les copies des élèves,

151
00:07:09,371 --> 00:07:11,362
énormément de choses de ce type-là.

152
00:07:12,143 --> 00:07:15,690
Je change un petit peu mon dernier programme,

153
00:07:15,904 --> 00:07:17,982
i égale 0, F égale 1,

154
00:07:18,082 --> 00:07:19,032
et je recommence.

155
00:07:19,339 --> 00:07:21,906
Je reprends mon invariant F égale factorielle i - 1.

156
00:07:22,750 --> 00:07:25,423
Il est correctement propagé par la boucle,

157
00:07:26,744 --> 00:07:31,003
mais par contre, à l'entrée de la boucle,

158
00:07:34,119 --> 00:07:35,605
il ne va pas marcher,

159
00:07:35,900 --> 00:07:37,816
parce que je commence avec i égal à 0,

160
00:07:38,223 --> 00:07:41,788
et donc je vais avoir, à la première étape ici,

161
00:07:45,332 --> 00:07:48,300
F qui est égal à 0.

162
00:07:48,623 --> 00:07:51,564
C'est-à-dire que j'ai eu une erreur d'initialisation

163
00:07:51,833 --> 00:07:53,099
par là.

164
00:07:53,500 --> 00:07:55,104
Aïe, aïe, aïe, aïe, aïe.

165
00:07:55,204 --> 00:07:56,530
Dans ce cas-là,

166
00:07:56,846 --> 00:07:58,295
il me renvoie toujours 0

167
00:07:58,395 --> 00:08:00,788
et donc le programme n'est pas correct.

