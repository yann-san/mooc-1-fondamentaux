1
00:00:01,067 --> 00:00:04,517
Lorsqu'on a le problème qui nous est posé,

2
00:00:04,967 --> 00:00:07,470
donc on a ici nos crêpes qui sont posées

3
00:00:07,570 --> 00:00:10,087
dans un ordre un peu bizarre,

4
00:00:10,527 --> 00:00:12,485
beaucoup d'élèves en fait,

5
00:00:12,585 --> 00:00:16,168
la majorité des algorithmes qui ressortent,

6
00:00:16,651 --> 00:00:17,619
mais il y en a d'autres,

7
00:00:17,719 --> 00:00:20,154
la majorité des algorithmes qui ressortent

8
00:00:20,254 --> 00:00:21,897
sont des algorithmes qui sont basés

9
00:00:21,997 --> 00:00:24,262
sur le fait qu'on va sélectionner petit à petit

10
00:00:24,775 --> 00:00:26,626
les crêpes les plus grandes

11
00:00:26,726 --> 00:00:28,327
et les mettre en bonne position.

12
00:00:28,731 --> 00:00:31,847
Alors, c'est déjà dans la démarche algorithmique

13
00:00:31,947 --> 00:00:32,898
quelque chose d'important,

14
00:00:33,328 --> 00:00:36,034
puisqu'on a décomposé le problème

15
00:00:36,134 --> 00:00:37,375
en plusieurs sous-problèmes,

16
00:00:37,475 --> 00:00:39,982
qui vont être d'abord de sélectionner la plus grande crêpe,

17
00:00:40,082 --> 00:00:41,839
et de la mettre en bonne position,

18
00:00:41,939 --> 00:00:44,060
puis de sélectionner la deuxième plus grande crêpe

19
00:00:44,160 --> 00:00:45,295
et de la mettre en bonne position.

20
00:00:45,395 --> 00:00:47,145
Ce n'est pas si évident que ça

21
00:00:47,245 --> 00:00:49,483
et on verra à la fin du corrigé

22
00:00:49,583 --> 00:00:51,128
et de la définition de l'algorithme,

23
00:00:52,163 --> 00:00:53,843
on verra que ce n'est pas trivial

24
00:00:53,943 --> 00:00:55,309
de résoudre ce problème-là.

25
00:00:55,746 --> 00:00:57,252
Donc, comment est-ce qu'on peut faire ?

26
00:00:57,352 --> 00:00:58,221
Et bien tout simplement,

27
00:00:58,321 --> 00:01:00,461
on va regarder toutes nos crêpes.

28
00:01:00,561 --> 00:01:01,557
Alors là, ça ne se voit pas bien,

29
00:01:01,657 --> 00:01:03,727
on ne voit pas bien comment elles sont,

30
00:01:04,125 --> 00:01:06,489
si je les reprends maintenant et je les tasse,

31
00:01:07,360 --> 00:01:08,883
là, ça va être plus clair,

32
00:01:10,168 --> 00:01:12,272
donc je vois que la plus grande est ici,

33
00:01:12,778 --> 00:01:14,519
c'est celle-là,

34
00:01:15,065 --> 00:01:17,737
je vais glisser ma spatule en-dessous,

35
00:01:19,088 --> 00:01:20,794
je retourne,

36
00:01:21,315 --> 00:01:24,483
donc là, ma plus grande est située tout en haut,

37
00:01:24,583 --> 00:01:27,362
je glisse ma spatule tout au fond,

38
00:01:27,921 --> 00:01:29,204
je retourne,

39
00:01:29,304 --> 00:01:32,113
et ma plus grande crêpe est bien placée.

40
00:01:32,547 --> 00:01:34,496
Et donc là, il y a deux façons de faire,

41
00:01:34,596 --> 00:01:36,380
soit on dit : j'itère,

42
00:01:36,480 --> 00:01:39,654
soit on dit : tiens, je me suis ramené

43
00:01:39,754 --> 00:01:41,441
à exactement le même problème

44
00:01:41,831 --> 00:01:46,023
mais avec un tas de crêpes qui est plus petit

45
00:01:46,596 --> 00:01:48,378
donc tout se passe comme si

46
00:01:48,478 --> 00:01:50,054
je pouvais glisser

47
00:01:51,069 --> 00:01:52,239
une feuille ici,

48
00:01:52,903 --> 00:01:54,399
et maintenant, je me ramène à un problème

49
00:01:54,499 --> 00:01:55,780
de taille plus petite que je résous

50
00:01:55,880 --> 00:01:57,177
exactement de la même façon.

51
00:01:57,583 --> 00:02:01,081
Alors, on va le faire de façon tout à fait itérative.

52
00:02:01,497 --> 00:02:04,234
Donc maintenant je dois chercher la deuxième plus grande crêpe,

53
00:02:04,631 --> 00:02:07,339
la deuxième plus grande crêpe, c'est celle-ci,

54
00:02:07,826 --> 00:02:10,992
je vais donc glisser ma spatule en-dessous,

55
00:02:11,457 --> 00:02:12,909
retourner le tas,

56
00:02:13,009 --> 00:02:16,952
et puis je sais que je dois placer cette deuxième plus grande crêpe

57
00:02:17,052 --> 00:02:18,988
au-dessus de la plus grande

58
00:02:19,088 --> 00:02:22,007
mais en-dessous de toutes les autres,

59
00:02:22,448 --> 00:02:23,605
donc je retourne

60
00:02:23,705 --> 00:02:25,031
le tas correspondant.

61
00:02:25,585 --> 00:02:27,692
Donc ici, j'ai fait une deuxième opération.

62
00:02:28,101 --> 00:02:31,697
Ici, je vais faire pareil avec

63
00:02:31,797 --> 00:02:33,653
la troisième plus grande,

64
00:02:36,082 --> 00:02:37,926
puis la quatrième plus grande,

65
00:02:38,026 --> 00:02:40,306
ah, elle était tout en haut déjà mais bon,

66
00:02:40,406 --> 00:02:42,060
je vais la retourner,

67
00:02:42,631 --> 00:02:45,064
et puis je vais la retourner,

68
00:02:45,164 --> 00:02:46,607
la cinquième et la sixième,

69
00:02:46,707 --> 00:02:49,226
je suis déjà en place.

70
00:02:49,326 --> 00:02:51,269
Donc j'ai fini ici

71
00:02:51,691 --> 00:02:53,515
par faire mon tas de crêpes

72
00:02:53,615 --> 00:02:56,143
et j'ai mes crêpes qui sont bien triées.

73
00:02:56,243 --> 00:02:58,100
Donc on peut noter là-dedans

74
00:02:58,200 --> 00:02:59,342
qu'il y a plusieurs étapes,

75
00:02:59,834 --> 00:03:01,706
que l'on va pouvoir faire un peu d'abstraction

76
00:03:01,806 --> 00:03:02,992
au sens où on va dire :

77
00:03:03,092 --> 00:03:06,227
rechercher la crêpe de plus grande taille

78
00:03:06,327 --> 00:03:07,644
dans un sous-ensemble de crêpes,

79
00:03:08,045 --> 00:03:09,269
donc ça, ça revient à dire :

80
00:03:09,369 --> 00:03:10,686
on a délégué

81
00:03:11,253 --> 00:03:15,139
à un autre objet, un autre élève par exemple, de faire ça,

82
00:03:15,575 --> 00:03:17,734
et puis après, on fait nos opérations

83
00:03:17,834 --> 00:03:19,073
de retournement de tas de crêpes.

