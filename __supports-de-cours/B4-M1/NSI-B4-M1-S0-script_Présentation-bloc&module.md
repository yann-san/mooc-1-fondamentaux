# 4.1.0 Script Introduction au Module .
## Architecture matérielle. Introduction

### Vidéo Introduction au bloc 4

**diapo 2** 

Les blocs 1 à 3 de cette formation au CAPES NSI traitent de la structure des données, des langages de programmation d'applications et des algorithmes.
Ces applications et systèmes de gestion de bases de données fonctionnent sur un ordinateur ayant des ressources matérielles , un système d'exploitation faisant l'interface entre ces applications et ces ressources  et les applications sont en interactions avec d'autres applications via Internet 

Ces autres disciplines de l'informatique sont les champs de recherche et de développement à l'origine des performances des programmes cités avant. En comprendre les fondements est indispensable pour aborder l'informatique dans son ensemble et ouvrir le champ des possibles des applications en terme de performances et de communication. 

**diapo 3** Introduction au bloc 4 - suite
Le bloc s'intéressera donc : 
*  à l'architecture des ordinateurs
*  aux systèmes d'exploitation
*  aux réseaux
*  aux technologies Web
Notons que ces deux derniers thèmes sont au coeur de nombreux emplois à des niveaux de diplômes variés.

### Vidéo Présentation du Module 

**diapo 4** Introduction au module 4.1

Bonjour, je m'appelle Anthony Juton, je suis professeur agrégé à l'ENS Paris Saclay, au département Nicola Tesla ou j'enseigne notamment l'architecture, les systèmes d'exploitation et les réseaux.

L'objectif principal de ce module est de Comprendre le fonctionnement d’un système informatique (microcontrôleur, ordinateur) pour être capable de sélectionner les caractéristiques d’une machine correspondant à une application donnée, de les exploiter et d'en appréhender les limites.

Le second objectif est d'acquérir les bases en architecture pour suivre les nombreuses évolutions technologiques de l'informatique, afin de pouvoir continuer à en exploiter les possibilités.

Le module nécessite d'avoir les bases en Électronique numérique : la numération binaire et hexadécimal, la logique booléenne et la connaissance des portes logiques de base.

**diapo 5** Plan du module 4.1

Après une courte introduction à l'architecture des systèmes informatiques, nous étudierons l'architecture minimale d'un système informatique puis, une présentation de l'architecture des systèmes embarqués permettra d'aborder les premiers concepts nous menant vers l'architecture des ordinateurs actuels.
Ce module donnera ainsi une base solide pour aborder le suivant sur les systèmes d'exploitation.

