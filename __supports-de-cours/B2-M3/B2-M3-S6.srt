1
00:00:00,000 --> 00:00:01,961
Ayant les règles d'évaluation du grand O

2
00:00:02,061 --> 00:00:03,336
pour chaque type d'instruction,

3
00:00:03,436 --> 00:00:05,775
le calcul de la complexité d'un algorithme

4
00:00:05,875 --> 00:00:08,351
se fait en partant des complexités des instructions simples

5
00:00:08,451 --> 00:00:10,775
et de calculant de proche en proche

6
00:00:11,174 --> 00:00:13,368
les complexités des instructions non-simples

7
00:00:13,468 --> 00:00:15,109
à partir des résultats déjà calculés.

8
00:00:15,609 --> 00:00:17,875
Ces calculs procèdent en quelque sorte

9
00:00:17,975 --> 00:00:18,781
par des regroupements

10
00:00:18,881 --> 00:00:21,501
en suivant la structure de l'algorithme.

11
00:00:22,038 --> 00:00:24,059
Prenons l'exemple de la recherche du minimum,

12
00:00:24,159 --> 00:00:26,161
c'est-à-dire du traitement suivant,

13
00:00:26,801 --> 00:00:29,386
après avoir transformé l'algorithme

14
00:00:29,486 --> 00:00:31,248
en mettant un while à la place du for,

15
00:00:31,348 --> 00:00:36,006
et en ayant n égal à la longueur de s, len(s).

16
00:00:36,603 --> 00:00:38,449
Nous supposons ici aussi

17
00:00:38,549 --> 00:00:40,354
que chaque instruction simple est prise en compte

18
00:00:40,454 --> 00:00:42,001
pour le calcul de la complexité.

19
00:00:43,200 --> 00:00:45,266
Si l'on considère l'arbre syntaxique du code,

20
00:00:45,942 --> 00:00:48,623
les feuilles de l'arbre sont

21
00:00:48,723 --> 00:00:53,236
les instructions 1, 2, 5, 6 et 7,

22
00:00:53,336 --> 00:00:55,229
toutes en O(1).

23
00:00:56,370 --> 00:00:57,566
De ce fait,

24
00:00:58,100 --> 00:00:59,553
d'après la règle 3,

25
00:01:00,794 --> 00:01:03,680
le nœud 4-5 qui correspond à l'instruction if

26
00:01:03,780 --> 00:01:05,507
est également en O(1).

27
00:01:06,079 --> 00:01:08,180
D'après la règle 2,

28
00:01:08,280 --> 00:01:10,609
la complexité de la séquence

29
00:01:10,709 --> 00:01:13,598
des instructions 4 jusqu'à 6

30
00:01:13,698 --> 00:01:15,590
est également en O(1).

31
00:01:16,124 --> 00:01:18,353
On peut maintenant évaluer la complexité du while

32
00:01:18,453 --> 00:01:20,453
qui correspond au nœud 3-6

33
00:01:20,865 --> 00:01:22,875
qui s'exécute n - 1 fois,

34
00:01:22,975 --> 00:01:27,425
c'est-à-dire que la complexité est en O(n)

35
00:01:27,525 --> 00:01:29,264
puisque c'est n fois O(1).

36
00:01:29,723 --> 00:01:31,339
Finalement, d'après la règle 2,

37
00:01:31,439 --> 00:01:33,303
la complexité de tout le traitement

38
00:01:33,403 --> 00:01:36,154
est en O de 1 + 1 + n + 1,

39
00:01:36,254 --> 00:01:38,191
c'est-à-dire en O(n).

40
00:01:39,229 --> 00:01:41,955
Il est facile de voir que l'algorithme de recherche séquentielle

41
00:01:42,055 --> 00:01:42,846
vu plus haut

42
00:01:42,946 --> 00:01:44,442
est également en O(n),

43
00:01:44,542 --> 00:01:46,376
où n est la longueur de la liste

44
00:01:46,476 --> 00:01:48,256
en supposant bien sûr que le test

45
00:01:48,356 --> 00:01:51,008
s[i] est-il égal à x

46
00:01:51,108 --> 00:01:53,077
est effectué en O(1).

47
00:01:54,454 --> 00:01:56,407
Une version Python de l'algorithme classique

48
00:01:56,507 --> 00:01:57,740
de recherche dichotomique de l'indice

49
00:01:57,840 --> 00:02:00,219
d'un élément dans un vecteur s

50
00:02:00,319 --> 00:02:01,418
est donnée ici.

51
00:02:02,590 --> 00:02:05,114
Attention, ici on suppose que chaque élément de s

52
00:02:05,214 --> 00:02:06,510
contient un couple

53
00:02:06,610 --> 00:02:09,086
avec la composante 0 qui correspond en quelque sorte

54
00:02:09,186 --> 00:02:10,182
à la clé de recherche

55
00:02:10,282 --> 00:02:11,588
et la composante 1

56
00:02:11,688 --> 00:02:13,533
qui correspond aux informations satellites.

57
00:02:14,070 --> 00:02:19,269
Ainsi s[m][0] représente la clé

58
00:02:19,369 --> 00:02:22,398
de l'élément d'indice m dans la liste

59
00:02:22,498 --> 00:02:24,294
et s[m] représente tout l'élément,

60
00:02:24,394 --> 00:02:26,202
clé et information satellite.

61
00:02:26,794 --> 00:02:29,465
Pour calculer la complexité de la recherche dichotomique

62
00:02:29,565 --> 00:02:31,343
en utilisant les règles vues précédemment,

63
00:02:31,443 --> 00:02:33,198
on peut assez rapidement observer

64
00:02:33,298 --> 00:02:35,281
que la complexité moyenne ou maximale

65
00:02:35,381 --> 00:02:37,883
en nombre d'actions élémentaires de cet algorithme

66
00:02:37,983 --> 00:02:39,641
est en O de f(n)

67
00:02:39,741 --> 00:02:41,851
où f(n) est le nombre respectivement

68
00:02:41,951 --> 00:02:44,047
moyen et maximum

69
00:02:44,147 --> 00:02:46,782
de fois que la boucle while est exécutée.

70
00:02:47,519 --> 00:02:50,598
Pour évaluer f max de n,

71
00:02:50,698 --> 00:02:52,707
commençons par analyser le comportement de l'algorithme.

72
00:02:53,587 --> 00:02:55,986
Nous pouvons tout d'abord remarquer

73
00:02:56,086 --> 00:02:57,312
qu'à chaque tour de boucle,

74
00:02:57,412 --> 00:02:59,865
au pire des cas, l'intervalle de recherche est divisé par 2,

75
00:03:00,323 --> 00:03:01,967
quelle que soit la valeur de x.

76
00:03:02,067 --> 00:03:04,625
La figure suivante donne une évolution possible

77
00:03:04,725 --> 00:03:07,431
de l'intervalle de recherche dans s,

78
00:03:07,531 --> 00:03:09,238
pour une liste de 100 éléments

79
00:03:09,338 --> 00:03:11,362
en supposant que x se trouve

80
00:03:11,462 --> 00:03:14,393
entre les éléments d'indices 40 et 41.

81
00:03:15,050 --> 00:03:17,722
Le while s'exécute au maximum 7 fois,

82
00:03:17,822 --> 00:03:19,575
de façon générale, on peut voir que

83
00:03:19,675 --> 00:03:23,288
n est entre 2 exposant f(max - 1)

84
00:03:23,388 --> 00:03:25,381
et 2 exposant f(max).

85
00:03:25,681 --> 00:03:27,288
Et donc, f(max),

86
00:03:27,918 --> 00:03:30,230
le nombre maximal de phases

87
00:03:30,330 --> 00:03:33,270
d'exécution de l'algorithme dichotomique,

88
00:03:33,370 --> 00:03:37,478
est inférieur ou égal au log en base 2 de n + 1,

89
00:03:37,578 --> 00:03:40,300
donc on peut en conclure

90
00:03:40,400 --> 00:03:41,818
que la recherche dichotomique

91
00:03:41,918 --> 00:03:43,854
est d'une complexité maximale,

92
00:03:43,954 --> 00:03:46,858
et on peut voir que c'est la même chose pour la complexité moyenne,

93
00:03:46,958 --> 00:03:49,299
en O de log en base 2  de n,

94
00:03:49,399 --> 00:03:50,938
c'est-à-dire en O(log n).

