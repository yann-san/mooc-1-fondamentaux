1
00:00:00,635 --> 00:00:02,693
Nous nous retrouvons donc dans une troisième vidéo,

2
00:00:02,793 --> 00:00:05,357
nous allons continuer notre projet château

3
00:00:05,474 --> 00:00:08,110
et nous allons mettre en place

4
00:00:08,210 --> 00:00:10,732
le lien entre les actions de l'utilisateur

5
00:00:10,832 --> 00:00:12,666
donc les événements provoqués par l'utilisateur

6
00:00:12,766 --> 00:00:16,697
et les actions à réaliser dans le jeu.

7
00:00:17,108 --> 00:00:18,799
Nous en étions restés

8
00:00:19,323 --> 00:00:20,684
au démarrage du jeu

9
00:00:20,784 --> 00:00:23,847
donc à la méthode start de notre maître de jeu

10
00:00:23,947 --> 00:00:26,767
avec l'affichage du château et du héros,

11
00:00:26,867 --> 00:00:27,838
notre jeu est lancé

12
00:00:27,938 --> 00:00:29,783
et nous avons des choses qui sont affichées

13
00:00:29,883 --> 00:00:31,795
mais nous n'avons pas encore de lien

14
00:00:31,895 --> 00:00:33,089
avec les interactions.

15
00:00:33,657 --> 00:00:35,122
Ce lien avec les interactions

16
00:00:35,222 --> 00:00:36,383
vont se faire

17
00:00:36,483 --> 00:00:38,731
par la définition de la méthode

18
00:00:38,831 --> 00:00:41,371
que j'ai appelée bind ici.

19
00:00:41,471 --> 00:00:44,552
La gestion des événements avec le module turtle

20
00:00:44,652 --> 00:00:46,583
se passe de la façon suivante,

21
00:00:49,493 --> 00:00:52,991
en tout cas, concernant la gestion des événements clavier,

22
00:00:53,091 --> 00:00:55,130
nous avons une des méthodes possibles,

23
00:00:55,230 --> 00:00:56,394
c'est la méthode onkey

24
00:00:56,494 --> 00:00:59,057
qui permet de détecter que l'utilisateur

25
00:00:59,157 --> 00:01:01,435
a pressé une touche du clavier.

26
00:01:02,818 --> 00:01:05,508
Cette méthode onkey,

27
00:01:05,608 --> 00:01:07,095
c'est une méthode qui

28
00:01:07,720 --> 00:01:10,198
est une méthode des écrans,

29
00:01:10,298 --> 00:01:12,154
des objets screen de turtle.

30
00:01:12,254 --> 00:01:13,201
Donc ici, je vous rappelle que

31
00:01:13,746 --> 00:01:15,143
c'est le contrôleur

32
00:01:15,243 --> 00:01:16,184
de notre maître de jeu

33
00:01:16,284 --> 00:01:17,458
qui est un objet screen.

34
00:01:17,558 --> 00:01:19,803
self.controleur.onkey

35
00:01:20,054 --> 00:01:22,630
et nous allons passer les paramètres qu'il faut.

36
00:01:24,642 --> 00:01:27,710
La méthode onkey prend deux paramètres.

37
00:01:28,846 --> 00:01:31,149
Le premier paramètre, c'est une fonction,

38
00:01:31,249 --> 00:01:33,390
c'est l'action qui va être déclenchée

39
00:01:33,490 --> 00:01:36,467
lorsque la touche est pressée

40
00:01:36,567 --> 00:01:38,768
et la touche est définie par le deuxième paramètre

41
00:01:38,868 --> 00:01:40,688
qui va préciser quelle touche effectivement

42
00:01:40,788 --> 00:01:42,552
déclenche notre action.

43
00:01:43,021 --> 00:01:45,610
Donc deux paramètres, et nous allons évidemment

44
00:01:45,710 --> 00:01:53,140
devoir répéter cette méthode onkey

45
00:01:55,869 --> 00:01:57,333
quatre fois puisque

46
00:01:57,644 --> 00:01:59,793
sur chacune des quatre directions.

47
00:02:03,055 --> 00:02:04,429
Donc ici, nous avons une constante

48
00:02:04,529 --> 00:02:06,704
que nous avons appelée KEY_UP

49
00:02:06,804 --> 00:02:09,643
et lorsque cette touche 

50
00:02:09,743 --> 00:02:10,773
flèche vers le haut

51
00:02:10,873 --> 00:02:11,748
est activée,

52
00:02:11,848 --> 00:02:13,652
il va falloir lancer une action sur notre jeu.

53
00:02:13,752 --> 00:02:15,951
Et donc l'action, ici, est réalisée par le héros,

54
00:02:16,051 --> 00:02:18,811
c'est le héros qui va devoir bouger finalement.

55
00:02:18,911 --> 00:02:20,510
Et nous allons avoir une méthode

56
00:02:20,610 --> 00:02:22,113
que nous pouvons appeler move_up.

57
00:02:22,374 --> 00:02:23,545
Attention, cette méthode

58
00:02:23,645 --> 00:02:24,886
ne doit pas prendre de paramètre.

59
00:02:26,085 --> 00:02:28,170
Et que fait cette méthode ?

60
00:02:28,994 --> 00:02:30,416
On peut l'écrire tout de suite.

61
00:02:32,409 --> 00:02:34,789
Donc une méthode qui ne prend pas

62
00:02:35,149 --> 00:02:38,683
d'autre paramètre que l'objet lui-même,

63
00:02:39,412 --> 00:02:41,183
et que fait cette move_up ?

64
00:02:41,283 --> 00:02:44,235
Et bien simplement, elle va appeler la vraie méthode

65
00:02:44,335 --> 00:02:45,412
qui fait avancer

66
00:02:45,512 --> 00:02:48,730
et qui elle, va prendre en paramètre

67
00:02:48,830 --> 00:02:50,074
une direction.

68
00:02:50,174 --> 00:02:51,764
Donc là encore, c'est une constante

69
00:02:51,864 --> 00:02:54,875
KEY_UP, KEY_DOWN, et cætera

70
00:02:54,975 --> 00:02:56,153
sont des constantes

71
00:02:56,253 --> 00:02:57,598
qui correspondent aux noms

72
00:02:57,698 --> 00:02:59,734
donnés par le module turtle

73
00:02:59,834 --> 00:03:02,327
aux différentes touches fléchées,

74
00:03:02,427 --> 00:03:03,796
donc ici, ce sont des chaînes de caractères

75
00:03:03,896 --> 00:03:06,249
et cette constante-là peut varier

76
00:03:06,349 --> 00:03:09,105
en fonction de l'interface graphique qu'on utilise

77
00:03:09,938 --> 00:03:14,068
et les constantes UP, DOWN, et cætera

78
00:03:14,168 --> 00:03:16,492
sont des tuples qui vont correspondre

79
00:03:16,592 --> 00:03:18,631
au delta de déplacement à faire

80
00:03:18,731 --> 00:03:20,315
sur le x et le y

81
00:03:20,415 --> 00:03:21,744
pour mettre à jour

82
00:03:21,844 --> 00:03:23,403
le modèle de notre héros.

83
00:03:25,375 --> 00:03:29,904
Ici, vont se réaliser

84
00:03:30,004 --> 00:03:31,388
les changements du modèle

85
00:03:31,488 --> 00:03:33,119
de notre jeu,

86
00:03:33,548 --> 00:03:36,087
lancés par le contrôleur

87
00:03:36,775 --> 00:03:40,270
sur interaction de l'utilisateur.

88
00:03:40,370 --> 00:03:42,518
Et une fois que cette bind est mise en place,

89
00:03:43,016 --> 00:03:45,657
il y a encore un petit appel technique

90
00:03:47,150 --> 00:03:49,354
du module turtle

91
00:03:49,454 --> 00:03:51,312
qui correspond à la méthode listen

92
00:03:51,412 --> 00:03:53,967
qui effectivement pose ce qu'on appelle l'écoute

93
00:03:54,067 --> 00:03:56,103
des événements.

94
00:04:00,661 --> 00:04:01,914
La méthode onkey

95
00:04:02,014 --> 00:04:03,249
va permettre de lier

96
00:04:04,629 --> 00:04:06,223
un événement, donc ici en l'occurrence

97
00:04:06,323 --> 00:04:09,075
un événement clavier

98
00:04:09,175 --> 00:04:10,567
avec une méthode,

99
00:04:10,667 --> 00:04:12,519
ici donc, la méthode du héros

100
00:04:12,619 --> 00:04:13,983
qui s'appelle move_up

101
00:04:14,083 --> 00:04:14,996
et qui elle-même

102
00:04:15,096 --> 00:04:16,589
va appeler une méthode

103
00:04:16,689 --> 00:04:19,063
qui va faire les modifications du modèle,

104
00:04:19,163 --> 00:04:20,343
à savoir changer

105
00:04:21,570 --> 00:04:24,407
l'emplacement, donc les coordonnées x, y du héros

106
00:04:24,507 --> 00:04:26,479
et lancer les interactions avec

107
00:04:27,194 --> 00:04:29,002
la cellule destination rencontrée.

108
00:04:30,535 --> 00:04:33,284
Voilà, nous allons détailler cette partie-là

109
00:04:33,384 --> 00:04:34,239
pas trop, pour ne pas trop en dévoiler,

110
00:04:34,339 --> 00:04:35,254
dans une quatrième vidéo.

