1
00:00:01,606 --> 00:00:04,460
Dans le cadre du MOOC Apprendre à coder avec Python,

2
00:00:04,560 --> 00:00:06,802
Jean Olgiati et Thierry Massart proposent

3
00:00:06,902 --> 00:00:08,728
un mini-projet

4
00:00:08,828 --> 00:00:10,837
qui consiste en la réalisation

5
00:00:10,937 --> 00:00:12,966
d'un petit jeu de type escape game,

6
00:00:13,066 --> 00:00:14,905
évidemment en mode non orienté objet

7
00:00:15,005 --> 00:00:16,265
puisque le MOOC ne porte pas

8
00:00:16,365 --> 00:00:18,339
sur ce paradigme de programmation.

9
00:00:18,879 --> 00:00:20,325
Ce que je vous propose dans ce bloc,

10
00:00:20,425 --> 00:00:21,875
c'est de réaliser ce projet

11
00:00:21,975 --> 00:00:23,524
en mode objet évidemment

12
00:00:24,122 --> 00:00:25,507
et nous allons voir ensemble

13
00:00:25,607 --> 00:00:26,942
le début de la conception.

14
00:00:27,042 --> 00:00:30,544
Nous allons voir le début de la mise en place

15
00:00:30,644 --> 00:00:32,232
des objets qui vont nous servir.

16
00:00:32,673 --> 00:00:36,415
Tout de suite, je vous propose de basculer sur un écran

17
00:00:36,515 --> 00:00:37,811
et de voir un petit peu de quoi il s'agit.

18
00:00:37,911 --> 00:00:39,660
Donc d'abord le jeu, je vous le présente rapidement.

19
00:00:39,760 --> 00:00:41,127
Vous le voyez ici.

20
00:00:41,227 --> 00:00:43,585
Il s'agit d'un jeu qui va utiliser le module turtle.

21
00:00:43,685 --> 00:00:45,309
Je vous rappelle que c'est un petit jeu très simple

22
00:00:45,705 --> 00:00:49,048
fait pour des débutants,

23
00:00:49,148 --> 00:00:50,510
pour que des débutants commencent à

24
00:00:51,124 --> 00:00:54,337
utiliser des programmes un petit peu plus costauds

25
00:00:54,437 --> 00:00:56,972
et structurent leurs différentes fonctions.

26
00:00:57,072 --> 00:00:58,231
Ça, c'était dans le cadre du MOOC.

27
00:00:58,331 --> 00:01:00,388
Nous, nous allons utiliser ce projet

28
00:01:00,488 --> 00:01:02,227
pour manipuler des objets.

29
00:01:02,866 --> 00:01:04,948
Nous manipulerons aussi des événements

30
00:01:05,048 --> 00:01:07,211
puisque, je vous montre le jeu.

31
00:01:07,311 --> 00:01:10,799
Il s'agit d'un petit escape game,

32
00:01:10,899 --> 00:01:13,128
vous avez un château, un labyrinthe,

33
00:01:13,228 --> 00:01:16,128
une succession de couloirs et de pièces

34
00:01:17,952 --> 00:01:22,171
un château dans lequel se déplace un héros,

35
00:01:22,271 --> 00:01:24,972
ici représenté donc par la petite puce rouge.

36
00:01:26,348 --> 00:01:28,570
Donc le héros évidemment se déplace

37
00:01:28,670 --> 00:01:31,010
via les interactions de l'utilisateur

38
00:01:31,110 --> 00:01:33,403
sur le clavier, en l'occurrence ici les flèches,

39
00:01:33,503 --> 00:01:35,216
les quatre flèches directionnelles.

40
00:01:35,792 --> 00:01:36,968
C'est pour cela, nous le verrons,

41
00:01:37,068 --> 00:01:39,476
que nous avons de la programmation événementielle.

42
00:01:41,006 --> 00:01:42,559
Donc l'idée c'est de savoir

43
00:01:42,659 --> 00:01:44,760
quels sont les objets que nous allons mettre en place

44
00:01:44,860 --> 00:01:48,022
et quelles vont être les interactions entre ces objets.

45
00:01:48,122 --> 00:01:49,527
Puisque dans la programmation objet,

46
00:01:49,627 --> 00:01:54,718
ce qui compte, c'est vraiment ce découpage en objets

47
00:01:54,818 --> 00:01:56,569
qui sont les acteurs, qui sont les objets,

48
00:01:56,669 --> 00:01:58,672
qui interviennent dans ce programme

49
00:02:00,027 --> 00:02:01,790
et surtout les interactions

50
00:02:02,636 --> 00:02:05,229
entre ces différents objets.

51
00:02:06,736 --> 00:02:08,356
Donc ici nous avons le château,

52
00:02:08,456 --> 00:02:10,778
ça, c'est déjà un objet qui semble évident,

53
00:02:11,687 --> 00:02:12,786
et un héros.

54
00:02:15,886 --> 00:02:16,909
Nous allons avoir aussi

55
00:02:17,009 --> 00:02:18,132
des interactions nous l'avons dit

56
00:02:18,232 --> 00:02:19,260
avec l'utilisateur

57
00:02:19,360 --> 00:02:22,708
ne serait-ce que par la gestion des événements.

58
00:02:23,442 --> 00:02:25,449
Nous allons aussi avoir des interactions avec l'utilisateur

59
00:02:25,549 --> 00:02:27,410
puisqu'il va falloir lui afficher des choses

60
00:02:27,510 --> 00:02:29,155
à un moment donné donc dans ce jeu.

61
00:02:29,255 --> 00:02:30,635
Il y a des portes,

62
00:02:30,735 --> 00:02:32,303
les petits carrés orange,

63
00:02:32,403 --> 00:02:35,460
et lorsqu'on tente de franchir une porte,

64
00:02:35,560 --> 00:02:36,961
une question est posée

65
00:02:37,061 --> 00:02:39,572
et on doit donner la bonne réponse.

66
00:02:39,672 --> 00:02:42,607
Voilà, ici, j'ai déverrouillé une porte

67
00:02:42,707 --> 00:02:45,445
J'ai un message du jeu

68
00:02:45,545 --> 00:02:46,959
qui me dit que la porte a été déverrouillée,

69
00:02:47,059 --> 00:02:49,810
elle disparaît effectivement du labyrinthe

70
00:02:49,910 --> 00:02:51,244
et on peut continuer à progresser.

71
00:02:52,014 --> 00:02:54,578
On a aussi des carrés verts

72
00:02:54,678 --> 00:02:55,874
qui sont des objets,

73
00:02:55,974 --> 00:02:57,440
des indices en fait,

74
00:02:57,540 --> 00:02:58,382
que l'on peut ramasser.

75
00:02:58,482 --> 00:03:00,821
Il suffit d'aller sur l'indice,

76
00:03:00,921 --> 00:03:02,403
voyez ici à droite

77
00:03:02,503 --> 00:03:05,376
j'ai un indice qui est de type Python ici

78
00:03:06,397 --> 00:03:07,794
le fond du jeu, si vous voulez,

79
00:03:07,894 --> 00:03:10,089
c'est un petit questionnaire

80
00:03:10,189 --> 00:03:13,563
sur des concepts de programmation,

81
00:03:13,663 --> 00:03:14,923
c'est pour cela que l'indice ici,

82
00:03:15,023 --> 00:03:16,512
c'est un dictionnaire.

83
00:03:19,512 --> 00:03:22,136
Et donc nous avons un certain nombre d'interactions avec l'utilisateur

84
00:03:22,236 --> 00:03:24,121
et ces interactions vont être évidemment

85
00:03:24,221 --> 00:03:27,339
réalisées par un des objets de notre programme.

86
00:03:28,647 --> 00:03:30,278
La question qui se pose, c'est

87
00:03:30,378 --> 00:03:31,718
quels sont les objets

88
00:03:31,818 --> 00:03:32,830
qu'on va mettre en place ?

89
00:03:33,930 --> 00:03:35,944
On l'a vu, le château et le héros

90
00:03:36,044 --> 00:03:38,884
semblent être deux objets incontournables.

91
00:03:39,404 --> 00:03:41,403
Dans ce type de programmation et en général

92
00:03:41,503 --> 00:03:42,890
quand on fait de la programmation orientée objet,

93
00:03:42,990 --> 00:03:45,028
on a aussi un point d'entrée

94
00:03:45,128 --> 00:03:47,352
qu'on peut appeler le contrôleur ;

95
00:03:47,452 --> 00:03:49,944
c'est lui qui va un petit peu orchestrer

96
00:03:50,372 --> 00:03:52,436
l'application, ici, notre jeu.

97
00:03:52,536 --> 00:03:56,371
 C'est lui qui va avoir le rôle d'interagir avec l'utilisateur

98
00:03:56,471 --> 00:03:58,971
et c'est ce contrôleur aussi

99
00:03:59,071 --> 00:04:01,502
qui va créer les autres objets

100
00:04:01,602 --> 00:04:04,423
donc notamment ici le château et le héros.

101
00:04:04,523 --> 00:04:06,952
Nous avons déjà trois objets

102
00:04:07,052 --> 00:04:09,331
qui vont commencer à interagir.

103
00:04:09,941 --> 00:04:12,946
On peut dessiner un schéma,

104
00:04:13,046 --> 00:04:15,457
là, c'est le schéma que je vous propose ici.

105
00:04:15,557 --> 00:04:18,329
Alors ce n'est pas la seule structuration possible

106
00:04:18,429 --> 00:04:22,073
mais disons que c'est une structuration à la fois simple

107
00:04:22,173 --> 00:04:26,107
et relativement claire

108
00:04:26,207 --> 00:04:29,222
et découpée en tout cas pour que

109
00:04:29,322 --> 00:04:32,081
les différentes actions, les différentes tâches

110
00:04:32,181 --> 00:04:33,631
soient clairement identifiées.

111
00:04:33,731 --> 00:04:36,909
Donc ici, le code couleur correspond

112
00:04:37,009 --> 00:04:38,138
nous y reviendrons,

113
00:04:38,238 --> 00:04:41,630
en gris, nous avons tout ce qui est interaction avec l'utilisateur

114
00:04:41,730 --> 00:04:43,739
que ce soit la gestion des événements ou les affichages

115
00:04:45,096 --> 00:04:48,023
de notre château et de notre héros à l'écran.

116
00:04:48,813 --> 00:04:50,427
Tout ce qui est en rose

117
00:04:50,527 --> 00:04:52,694
c'est ce que j'appelle le modèle

118
00:04:54,671 --> 00:04:58,347
c'est comment est-ce qu'on représente, comment est-ce qu'on va manipuler le héros,

119
00:04:58,447 --> 00:05:01,292
qu'est-ce qu'on a choisi pour manipuler et représenter le château,

120
00:05:01,392 --> 00:05:02,194
et cætera.

121
00:05:02,294 --> 00:05:06,144
Et en bleu, donc le seul objet en bleu,

122
00:05:06,244 --> 00:05:08,506
c'est ce que j'ai appelé le maître de jeu,

123
00:05:08,606 --> 00:05:09,808
l'objet qu'on appellera maître de jeu,

124
00:05:09,908 --> 00:05:10,864
qui est le contrôleur.

125
00:05:10,964 --> 00:05:13,396
C'est le point d'entrée du programme.

126
00:05:13,496 --> 00:05:14,949
C'est aussi ce contrôleur

127
00:05:15,049 --> 00:05:18,544
qui va distribuer un petit peu les tâches

128
00:05:18,644 --> 00:05:21,319
et qui fait le chef d'orchestre un petit peu

129
00:05:21,419 --> 00:05:23,066
de l'ensemble de l'organisation.

130
00:05:23,840 --> 00:05:26,914
Donc on part sur cette structure.

131
00:05:27,014 --> 00:05:29,173
On a déjà parlé de trois des objets

132
00:05:29,273 --> 00:05:30,859
présents ici dans mon graphique,

133
00:05:31,489 --> 00:05:32,819
le maître de jeu,

134
00:05:32,919 --> 00:05:34,270
le héros et le château.

135
00:05:34,909 --> 00:05:36,302
Je vous propose tout de suite

136
00:05:36,402 --> 00:05:39,597
de commencer à créer les objets

137
00:05:39,697 --> 00:05:41,475
et notamment donc leurs propriétés,

138
00:05:41,575 --> 00:05:43,768
on va voir le contenu de leurs constructeurs,

139
00:05:43,868 --> 00:05:46,665
et les questions à se poser pour savoir

140
00:05:46,765 --> 00:05:49,293
quels attributs, quelles propriétés

141
00:05:49,393 --> 00:05:50,455
ajouter à nos objets.

142
00:05:52,447 --> 00:05:54,846
Donc, on l'a vu, le maître de jeu

143
00:05:54,946 --> 00:05:59,350
va créer, et en même temps, il lui faut une vision

144
00:05:59,450 --> 00:06:01,144
sur le château et le héros.

145
00:06:01,244 --> 00:06:03,627
Donc nous pouvons commencer.

146
00:06:04,397 --> 00:06:06,700
Notre classe maître de jeu

147
00:06:12,530 --> 00:06:14,706
dont le constructeur va

148
00:06:15,633 --> 00:06:17,080
alors pour l'instant, nous ne savons pas

149
00:06:17,180 --> 00:06:19,755
si ce constructeur aura des paramètres

150
00:06:19,855 --> 00:06:20,511
nous verrons bien,

151
00:06:21,807 --> 00:06:23,736
en tout cas, il doit créer

152
00:06:24,629 --> 00:06:25,504
un château

153
00:06:27,899 --> 00:06:31,937
donc avec l'appel à un constructeur de château,

154
00:06:32,037 --> 00:06:34,177
et le château évidemment

155
00:06:35,004 --> 00:06:36,850
va interagir avec son maître de jeu

156
00:06:37,228 --> 00:06:39,733
donc il faut que le château

157
00:06:39,833 --> 00:06:41,466
ait une connaissance de son maître de jeu,

158
00:06:41,566 --> 00:06:42,451
qu'il y ait un lien

159
00:06:42,551 --> 00:06:44,547
donc au moment de la création du château,

160
00:06:45,759 --> 00:06:47,666
probablement que le château aura

161
00:06:47,766 --> 00:06:49,957
un attribut maître de jeu

162
00:06:50,057 --> 00:06:52,678
qui sera le maître de jeu qui est en train de créer son château.

163
00:06:52,778 --> 00:06:54,216
C'est pour cela qu'ici

164
00:06:54,316 --> 00:06:56,938
on lui passe comme argument self

165
00:06:57,038 --> 00:06:58,807
qui est donc le maître de jeu lui-même.

166
00:07:00,354 --> 00:07:02,267
On crée aussi donc un héros

167
00:07:06,824 --> 00:07:07,819
donc le héros,

168
00:07:08,784 --> 00:07:11,672
il a évidemment la connaissance du maître de jeu

169
00:07:11,772 --> 00:07:14,126
mais il doit aussi avoir la connaissance du château,

170
00:07:14,226 --> 00:07:16,157
c'est représenté ici par le petit lien,

171
00:07:16,257 --> 00:07:18,622
du château dans lequel il se déplace.

172
00:07:20,415 --> 00:07:24,438
Ici, on va passer, au constructeur du héros,

173
00:07:24,538 --> 00:07:26,844
le château que l'on vient de créer

174
00:07:27,044 --> 00:07:29,447
et le maître de jeu lui-même.

175
00:07:34,507 --> 00:07:38,818
Ensuite, concernant l'interface,

176
00:07:39,468 --> 00:07:41,709
l'interface ici est réalisée en turtle,

177
00:07:41,809 --> 00:07:43,438
évidemment, on aurait pu faire le même jeu

178
00:07:43,538 --> 00:07:45,667
avec un autre type d'interface,

179
00:07:45,767 --> 00:07:47,112
que ce soit pygame, 

180
00:07:47,312 --> 00:07:49,866
processing ou autre, peu importe.

181
00:07:50,548 --> 00:07:53,303
Donc on a un gestionnaire d'interface

182
00:07:53,403 --> 00:07:54,331
qui est ici turtle

183
00:07:54,431 --> 00:07:56,584
et qu'il va effectivement aussi falloir

184
00:07:57,891 --> 00:07:59,776
intégrer d'une façon ou d'une autre.

185
00:08:02,628 --> 00:08:08,645
Donc ici, l'objet qui sert à faire cette interface

186
00:08:08,745 --> 00:08:12,340
c'est l'objet turtle du module turtle.

187
00:08:13,391 --> 00:08:15,183
Donc ici, notre maître de jeu

188
00:08:15,283 --> 00:08:17,714
va devoir afficher les messages

189
00:08:17,814 --> 00:08:20,162
qui se trouvent en haut de l'écran ici,

190
00:08:20,262 --> 00:08:21,463
quand on ramasse un objet

191
00:08:21,563 --> 00:08:23,751
ou quand on est sur une porte,

192
00:08:24,464 --> 00:08:29,223
afficher le contenu de l'inventaire,

193
00:08:29,750 --> 00:08:33,058
donc l'ensemble des indices récupérés,

194
00:08:34,441 --> 00:08:37,353
et le maître de jeu va se charger également

195
00:08:37,453 --> 00:08:40,083
de l'interface, donc de la gestion des événements

196
00:08:40,183 --> 00:08:41,552
avec l'utilisateur.

197
00:08:49,097 --> 00:08:52,544
En gros, nous allons avoir une tortue pour afficher nos messages

198
00:08:52,963 --> 00:08:55,510
je vais l'appeler message et puis je vais rajouter

199
00:08:55,610 --> 00:08:56,905
le suffixe vue

200
00:08:57,105 --> 00:09:01,214
pour bien montrer qu'il s'agit d'une interface en fait

201
00:09:05,192 --> 00:09:06,714
qui donc sera une tortue.

202
00:09:08,066 --> 00:09:10,871
Alors, on pourrait utiliser une seule tortue

203
00:09:10,971 --> 00:09:13,184
qui se baladerait ici dans notre écran,

204
00:09:13,284 --> 00:09:16,037
qui viendrait ici pour afficher notre message

205
00:09:16,137 --> 00:09:17,538
d'indice ou de porte,

206
00:09:17,638 --> 00:09:20,351
puis qui reviendrait ici pour afficher les indices.

207
00:09:21,516 --> 00:09:23,023
C'est finalement plus simple

208
00:09:24,043 --> 00:09:25,039
d'avoir deux tortues

209
00:09:25,139 --> 00:09:26,566
qui ne bougent pratiquement pas,

210
00:09:26,666 --> 00:09:27,689
celle-là ne bougera pas du tout

211
00:09:27,789 --> 00:09:30,525
et elle affichera ses messages toujours au même endroit,

212
00:09:30,625 --> 00:09:32,610
et la tortue pour les indices

213
00:09:32,710 --> 00:09:35,779
va simplement se décaler un petit peu vers le bas

214
00:09:36,461 --> 00:09:37,879
une fois un indice affiché

215
00:09:37,979 --> 00:09:40,010
puisque les indices vont rester affichés

216
00:09:40,893 --> 00:09:43,206
tout le long du programme.

217
00:09:44,579 --> 00:09:47,687
Je vais l'appeler objets

218
00:09:48,895 --> 00:09:50,311
obj_vue

219
00:09:51,092 --> 00:09:52,664
et donc ce sera une autre tortue,

220
00:09:52,764 --> 00:09:56,450
il n'y a pas de problème à créer plusieurs tortues.

221
00:09:59,464 --> 00:10:01,226
Et puis nous avons

222
00:10:01,992 --> 00:10:05,909
donc là, il faut une petite connaissance du module turtle,

223
00:10:06,009 --> 00:10:08,843
les interactions du module turtle

224
00:10:08,943 --> 00:10:13,932
se passent via l'écran en fait du module.

225
00:10:14,032 --> 00:10:18,295
Cet écran, ici on le voit, il sera partagé par toutes les tortues

226
00:10:18,802 --> 00:10:21,202
et il y a un seul écran

227
00:10:21,714 --> 00:10:24,158
dans lequel tout se passe.

228
00:10:25,897 --> 00:10:27,526
Ici, je vais l'appeler contrôleur en fait

229
00:10:28,110 --> 00:10:37,178
et il s'agit d'un objet écran du module turtle.

230
00:10:37,865 --> 00:10:40,271
Et donc avec ces trois objets-là, nous avons

231
00:10:40,420 --> 00:10:43,104
complètement défini notre contrôleur,

232
00:10:43,204 --> 00:10:44,351
notre maître de jeu avec

233
00:10:44,551 --> 00:10:46,381
ses deux objets château et héros

234
00:10:46,481 --> 00:10:51,765
et l'ensemble des objets turtle

235
00:10:51,865 --> 00:10:54,417
pour réaliser les interactions avec l'utilisateur

236
00:10:54,517 --> 00:10:55,594
que ce soit les affichages

237
00:10:55,694 --> 00:10:57,135
ou la gestion des événements.

238
00:11:00,127 --> 00:11:04,880
Donc voilà pour ce qui est du maître de jeu,

239
00:11:04,980 --> 00:11:10,591
on peut regarder la création du château et du héros

240
00:11:11,808 --> 00:11:13,501
en commençant par le château.

241
00:11:16,029 --> 00:11:17,969
Le château, on l'a vu,

242
00:11:18,506 --> 00:11:21,793
il doit avoir un maître de jeu,

243
00:11:22,299 --> 00:11:26,748
il aura un paramètre maître de jeu

244
00:11:26,848 --> 00:11:27,796
dans son constructeur

245
00:11:35,730 --> 00:11:38,308
et, le château, on constate

246
00:11:38,408 --> 00:11:40,756
qu'il va également faire des affichages,

247
00:11:42,120 --> 00:11:44,072
on voit ici sur notre graphique

248
00:11:44,172 --> 00:11:45,818
qu'on a une partie vue

249
00:11:46,018 --> 00:11:49,638
qui va réaliser tout l'affichage du château,

250
00:11:49,838 --> 00:11:51,126
de chacune des cellules.

251
00:11:54,653 --> 00:11:58,590
Et donc, on peut pareillement définir

252
00:11:58,690 --> 00:12:01,049
l'attribut self.vue

253
00:12:01,796 --> 00:12:03,285
comme étant une tortue

254
00:12:03,816 --> 00:12:05,098
du module turtle

255
00:12:07,277 --> 00:12:10,267
et qui va permettre de faire les affichages.

256
00:12:11,879 --> 00:12:15,364
Pour l'instant, le château, c'est tout ce que nous avons.

257
00:12:15,464 --> 00:12:16,498
Alors évidemment,

258
00:12:16,598 --> 00:12:20,155
on voit bien que le château est constitué de plusieurs cellules,

259
00:12:21,268 --> 00:12:22,495
on peut préparer le terrain

260
00:12:22,595 --> 00:12:25,805
et se donner un attribut map par exemple

261
00:12:26,059 --> 00:12:27,604
ou grille, ou ce qu'on veut

262
00:12:28,634 --> 00:12:30,879
qui va probablement être une liste

263
00:12:32,991 --> 00:12:34,068
de cellules.

264
00:12:35,298 --> 00:12:39,811
Ici, j'ai toute une partie du graphique dont je n'ai pas parlé,

265
00:12:39,911 --> 00:12:41,408
c'est que le château

266
00:12:41,508 --> 00:12:43,154
sera constitué lui-même de cellules

267
00:12:43,254 --> 00:12:47,546
qui seront des objets qu'on va créer.

268
00:12:47,646 --> 00:12:48,543
Pour l'instant,

269
00:12:49,827 --> 00:12:53,480
simplement on sait qu'il va y avoir une map, une grille

270
00:12:53,580 --> 00:12:56,049
et on s'arrête là.

271
00:12:57,308 --> 00:12:59,042
Passons au héros.

272
00:13:03,434 --> 00:13:05,724
Donc le héros

273
00:13:07,470 --> 00:13:11,297
doit non seulement connaître le maître de jeu

274
00:13:11,397 --> 00:13:12,928
mais également, on l'a vu, le château.

275
00:13:13,410 --> 00:13:15,961
Donc nous avons ici deux paramètres

276
00:13:16,061 --> 00:13:17,772
pour le constructeur du héros,

277
00:13:19,947 --> 00:13:21,309
son maître de jeu

278
00:13:22,449 --> 00:13:24,610
et son château.

279
00:13:29,392 --> 00:13:30,178
Voilà.

280
00:13:30,563 --> 00:13:34,868
Ensuite, le héros, on le voit,il se déplace

281
00:13:34,968 --> 00:13:36,994
et donc probablement il va falloir quand même

282
00:13:37,094 --> 00:13:38,656
mémoriser sa position,

283
00:13:39,321 --> 00:13:40,455
puisqu'il va avoir des coordonnées

284
00:13:40,663 --> 00:13:41,973
x, y

285
00:13:47,625 --> 00:13:50,111
qui au départ, peuvent être mises à None

286
00:13:50,211 --> 00:13:52,659
puisqu'on ne sait pas où démarre le héros.

287
00:13:53,159 --> 00:13:54,817
Pour l'instant, on en est juste à la création,

288
00:13:54,917 --> 00:13:57,530
évidemment, il va probablement falloir

289
00:13:57,630 --> 00:13:59,332
faire une phase de setup

290
00:13:59,432 --> 00:14:01,038
de paramétrage.

291
00:14:01,694 --> 00:14:03,070
Dans cette phase de paramétrage,

292
00:14:03,170 --> 00:14:05,060
le maître de jeu va récupérer

293
00:14:05,160 --> 00:14:08,212
les informations concernant le château,

294
00:14:08,312 --> 00:14:10,519
c'est-à-dire tous les murs,

295
00:14:10,842 --> 00:14:12,846
où se trouvent les portes, les objets, et cætera.

296
00:14:14,141 --> 00:14:15,688
Et c'est dans cette phase-là

297
00:14:15,788 --> 00:14:19,136
que la position initiale du héros

298
00:14:19,236 --> 00:14:20,536
sera déterminée.

299
00:14:21,702 --> 00:14:23,817
Pour l'instant, nous avons donc

300
00:14:23,917 --> 00:14:25,151
le maître de jeu, le château,

301
00:14:25,251 --> 00:14:27,370
une position en x et en y

302
00:14:27,966 --> 00:14:29,034
que nous ne connaissons pas.

303
00:14:31,545 --> 00:14:34,686
Et le héros va aussi avoir besoin de s'afficher,

304
00:14:34,786 --> 00:14:36,160
comme en plus, il se déplace tout le temps

305
00:14:37,359 --> 00:14:39,175
ce n'est pas inintéressant

306
00:14:39,275 --> 00:14:41,195
de lui dédier une tortue.

307
00:14:50,109 --> 00:14:50,881
Voilà.

308
00:14:51,928 --> 00:14:53,858
Et là nous avons défini

309
00:14:55,198 --> 00:14:56,480
pratiquement tous nos objets,

310
00:14:56,580 --> 00:14:57,802
il reste les objets cellules

311
00:14:57,902 --> 00:14:58,823
sur lesquels nous reviendrons

312
00:14:58,923 --> 00:15:02,285
mais nous avons défini les principaux objets,

313
00:15:02,385 --> 00:15:04,232
le contrôleur, le château et le héros,

314
00:15:04,903 --> 00:15:08,297
ceux en tout cas qui embarquent aussi

315
00:15:08,764 --> 00:15:14,012
une partie qui permet l'interaction avec l'utilisateur

316
00:15:14,112 --> 00:15:15,275
que ce soit pour les affichages

317
00:15:15,375 --> 00:15:16,454
ou pour la gestion des événements.

318
00:15:17,226 --> 00:15:17,779
Voilà.

319
00:15:18,360 --> 00:15:20,002
On arrête là pour cette première vidéo.

320
00:15:21,538 --> 00:15:23,888
Dans une prochaine vidéo, nous verrons

321
00:15:23,988 --> 00:15:25,443
un peu plus avant

322
00:15:25,648 --> 00:15:27,214
le développement de ces objets

323
00:15:27,314 --> 00:15:30,043
notamment quelques méthodes

324
00:15:30,343 --> 00:15:33,111
et comment nous gérons les événements

325
00:15:33,211 --> 00:15:34,257
avec le module turtle.

