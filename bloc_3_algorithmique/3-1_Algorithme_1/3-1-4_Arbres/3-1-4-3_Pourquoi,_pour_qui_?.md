## Types abstrait de données : pourquoi, pour qui ? 3/10

  1. Introduction à la séquence
  2. Types abstraits en algorithmique
  3. **Pourquoi, pour qui ?**
  4. La pile
  5. La file
  6. Arbres - définition, propriétés, comptage part1 & part 2 
  7. Arbres binaires 
  8. Parcours d'arbre
  9. Feuilles étiquetées


[![Vidéo 3 B3-M1-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S4-V3.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S4-V3.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S4/B3-M1-S4-video3.srt" target="_blank">Sous-titre de la vidéo</a>

## Supports de présentation (diapos)

[Support de la présentation des vidéos 2 à 5](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S4/B3-M1-S4-part1.pdf)

