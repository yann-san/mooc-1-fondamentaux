## Introduction au concept d'algorithme : algorithme, vie courante, première approche

L'objectif de cette section est d'introduire le concept d'algorithme et de situer ce concept dans un cadre scientifique plus large en informatique, avec de manière plus précise quelques éléments en lien avec l'enseignement de l'informatique au lycée et le concours du Capes/Cafep NSI.

1. **Algorithme, vie courante, première approche 1 / 4**
2. La conception d'un algorithme 2 / 4
3. L'expression d'un algorithme 3 / 4
4. Algorithmique positionnement et références 4 / 4


Dans ces vidéos, nous allons  :

- étudier la représentation de l'algorithme dans les journaux grand public
- et regarder de près l'analogie entre algorithme et recette de cuisine.

[![Vidéo 1 part 1 B3-M1-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S1-Video1a.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S1-Video1a.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S1/B3-M1-S1-video1a.srt" target="_blank">Sous-titre de la vidéo</a>

[![Vidéo 1 part 2 B3-M1-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S1-Video1b.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S1-Video1b.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S1/B3-M1-S1-video1b.srt" target="_blank">Sous-titre de la vidéo</a>

## Supports de présentation (diapos)

[Supports de la présentation](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S1/B3-M1-S1-video1a.pdf)
