## Introduction au concept d'algorithme : algorithmique positionnement et références

1. Algorithme, vie courante, première approche 1 / 4
2. La conception d'un algorithme 2 / 4
3. L'expression d'un algorithme 3 / 4
4. **Algorithmique positionnement et références 4 / 4**

Dans la représentation de la science informatique - quelques références bibliographiques

[![Vidéo 4 B3-M1-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S1-Video4.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S1-Video4.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S1/B3-M1-S1-video4.srt" target="_blank">Sous-titre de la vidéo</a>

## Supports de présentation (diapos)

[Supports de la présentation](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S1/B3-M1-S1-video4.pdf)
