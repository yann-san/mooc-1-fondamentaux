## Arbres binaires de recherche. 1/4: motivation et méthodologie

1. **Motivation et méthodologie**
2. Principe,rechercher dans un ABR
3. Autres opérations part1
4. Autres opérations part2

Dans cette séquence, nous allons regarder comment on utilise une structure de type arbre binaire pour traiter une collection d'objets de façon plus efficace. Le concept d'**Arbre Binaire de Recherche (ABR)**, qui est au programme de terminal, consiste à maintenir une structure semi-triée, c'est à dire avec une relation d'ordre entre les éléments, mais pas complètement triée comme une liste ordonnée. On va utiliser cette structure d'arbre pour pouvoir effectuer des opérations d'insertion, de suppression, ... pour trier et pour rechercher un élément, etc.

Notre objectif est de montrer comment construire une structure de données qui soit relativement souple à l'usage, c'est à dire qui permette d'utiliser les éléments, modifier, mettre à jour, etc. et qui soit également efficace du point de vue de la recherche, des différentes opérations, et qu'on puisse parcourir de façon simple.

Enfin, on faira apparaître comment les ABR sont une généralisation de la liste vue précédemment.

[![Vidéo 1 B3-M1-S5 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S5-V1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S5-V1.mp4)
