## Autres opérations part2 4/4

1. Motivation et méthodologie
2. Principe,rechercher dans un ABR
3. Autres opérations part1
4. **Autres opérations part2**

[![Vidéo 4 B3-M1-S5 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S5-V4.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S5-V4.mp4)

## Supports de présentation (diapos)

[Support de la présentation des vidéos de la séquence ABR](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S5/B3-M1-S5.pdf)


