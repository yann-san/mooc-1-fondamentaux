## Algorithmes gloutons. 4/5 : Problème des choix d'activité

1. Problème d'optimisation
2. Allocation
3. Preuve d'algorithme
4. **Problème des choix d'activité**
5. Résumé

[![Vidéo 4 B3-M1-S6 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S6-V4.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S6-V4.mp4)

## Supports de présentation (diapos)

[Support des vidéos de la séquence Algorithmes gloutons](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S6/B3-M1-S6.pdf)

